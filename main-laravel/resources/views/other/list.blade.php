
@extends('layouts.backend')
  @section('scripts')
<script type="text/javascript">
   $_token = '{{ csrf_token() }}';
	function refresh() {
			var loading = $(".load-refresh");
			var edit = $(".edit-contact");
			var services = $("#rows-services");
			var about = $("#rows-about");
			var header = $("#rows-header");
			var contact = $("#rows-contact");
			$_token = "{{ csrf_token() }}";
			var url = "{{ url('/') }}";
			loading.removeClass('hide');
			
			$.post("{{ url('admin/other/refresh') }}", { _token: $_token }, function(response) {
					// clear services table
					services.html("");
					about.html("");
					contact.html("");
					header.html("");

					// populate about us table
					$.each(response.header.data, function(index, row) {

							header.append(
									'<tr data-id="' + row.id + '">' +
									'<td>' + row.title_eng + '</td>' +
									'<td>' + row.desc_eng + '</td>' +
									'<td>' +
										'<a href="' + url + '/admin/other/header/' + row.id + '/edit" class="btn btn-xs btn-primary btn-edit" title="Edit About Us" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
									'</td>' +
									'</tr>'
							);
					});

					// populate services table
					$.each(response.services.data, function(index, row) {

							var desc_eng;

							if (row.desc_eng.length < 200) {
								desc_eng = row.desc_eng;
							} else {
								desc_eng = row.desc_eng.substring(0, 200) + '...';
							}

							services.append(
									'<tr data-id="' + row.id + '">' +
									'<td><i class="fa fa-' + row.icon + ' fa-3x"></i></td>' +
									'<td>' + row.title_eng + '</td>' +
									'<td>' + desc_eng + '</td>' +
									'<td>' +
										'<a href="' + url + '/admin/other/service/' + row.id + '/edit" class="btn btn-xs btn-primary btn-edit" title="Edit Services" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
									'</td>' +
									'</tr>'
							);
					});

					// populate about us table
					$.each(response.about.data, function(index, row) {

							var desc_eng;

							if (row.desc_eng.length < 200) {
								desc_eng = row.desc_eng;
							} else {
								desc_eng = row.desc_eng.substring(0, 200) + '...';
							}

							about.append(
									'<tr data-id="' + row.id + '">' +
									'<td>' + desc_eng + '</td>' +
									'<td>' +
										'<a href="' + url + '/admin/other/about/' + row.id + '/edit" class="btn btn-xs btn-primary btn-edit" title="Edit About Us" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
									'</td>' +
									'</tr>'
							);
					});

					// populate contact information table
					$.each(response.contact.data, function(index, row) {

							contact.append(
									'<tr data-id="' + row.id + '">' +
									'<td>Address </td>' +
									'<td>' + row.address + '</td>' +
									'</tr>' +
							
									'<tr data-id="' + row.id + '">' +
									'<td>Phone Number</td>' +
									'<td>' + row.phone + '</td>' +
									'</tr>' +
									'<tr data-id="' + row.id + '">' +
									'<td>Email Address</td>' +
									'<td>' + row.email + '</td>' +
									'</tr>' +
									'<tr data-id="' + row.id + '">' +
									'<td>Clinic Hours (English)</td>' +
									'<td>' + row.clinic_eng + '</td>' +
									'</tr>' +
									'<tr data-id="' + row.id + '">' +
									'<td>Clinic Hours (Arabic)</td>' +
									'<td>' + row.clinic_arabic + '</td>' +
									'</tr>' +
									'<tr data-id="' + row.id + '">' +
									'<td>Facebook Link</td>' +
									'<td>' + row.facebook_link + '</td>' +
									'</tr>' +
									'<tr data-id="' + row.id + '">' +
									'<td>Twitter Link</td>' +
									'<td>' + row.twitter_link + '</td>' +
									'</tr>'
							);
					});

					loading.addClass('hide');
					edit.removeClass('hide');

			}, 'json');
	}
	
	$(document).ready(function() {

		refresh();

	});
    function search() {
      
      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
      table.html("");
      
      var body = "";
      console.log(response);

        $.each(response.rows.data, function(index, row) {

           body += '<tr data-id="' + row.id + '"' + ((row.status == 1) ? ((row.id == {{ Auth::user()->id }}) ? 'class=info' : '') : 'class="tr-disabled"') + '>' + 
              '<td class="hide">' + row.status + '</td>' +
              //'<td><img id="system_user_photo" src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
              '<td>' + row.name + '</td>' +
              '<td>' + row.email + '</td>' +
              //'<td class="hidden-xs">' + row.type_name + '</td>' +
              '<td class="rightalign">';
                  
              body += '</td>'+
            '</tr>';
        });
      
      table.html(body);
      $('#row-pages').html(response.pages);
      loading.addClass("hide");

      }, 'json');
    
    }

    $(".content-wrapper").on("click", ".btn-add", function() {
           $("#modal-form").find('form').trigger("reset");
           $("#row-name").removeAttr('disabled','disabled');
           $("#row-email").removeAttr('disabled','disabled');
           $("#row-password").removeAttr('disabled','disabled');
           $("#row-password_confirmation").removeAttr('disabled','disabled');
           $("#row-mobile").removeAttr('disabled','disabled');
           $("#row-usertype_id").removeAttr('disabled','disabled');
           $("#button-save").removeClass("hide");
           $(".modal-title").html('<i class="fa fa-plus"></i><strong> Add Advertisement</strong>');
           $("#modal-form").modal('show');
        
    });
     $(".content-wrapper").on("click", ".btn-edit", function() {

        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        
        $(".uploader-pane").addClass("hide");
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";

        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".change").addClass('hide');

        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/other/edit') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit User ['+response.name+']</strong>');

                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          
                         if(index == "user_type") {
                          $("#row-usertype_id-static").val(value.type_name);
                         }
                     
                          // field exists, therefore populate
                          
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                      });
        
                      $("#row-address").removeAttr('disabled','disabled');
                      $("#row-email").removeAttr('disabled','disabled');
                      $("#row-password").removeAttr('disabled','disabled');
                      $("#row-password_confirmation").removeAttr('disabled','disabled');
                      $("#row-mobile").removeAttr('disabled','disabled');
                      $("#row-usertype_id").removeAttr('disabled','disabled');
                      $("#button-save").removeClass("hide");
                      // show form
                      $("#modal-form").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
   $(".content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        
        $(".uploader-pane").addClass("hide");
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";

        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".change").addClass('hide');

        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/other/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit User ['+response.name+']</strong>');

                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          
                         if(index == "user_type") {
                          $("#row-usertype_id-static").val(value.type_name);
                         }
                     
                          // field exists, therefore populate
                          
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                      });
        
                      $("#row-category").attr('disabled','disabled');
                      $("#row-title").attr('disabled','disabled');
                      $("#row-body").attr('disabled','disabled');
                      $("#row-password_confirmation").attr('disabled','disabled');
                      $("#row-mobile").attr('disabled','disabled');
                      $("#row-usertype_id").attr('disabled','disabled');
                      $("#button-save").addClass("hide");
                      // show form
                      $("#modal-form").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
</script>
@stop
@section('content')
  <!-- include jquery -->
  <div class="container">
    @if (Session::has("notice"))
        <div class="alert alert-dismissable alert-{{ Session::get('notice.type') }}">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <i class="fa fa-fw fa-exclamation-triangle"></i> {{ Session::get('notice.msg') }} <a target="_blank" href="{{ Session::get('notice.url') }}">View Here</a>
        </div>
    @endif
</div>
		<div class="container">

			<h3>HEADER</h3>
			<div class="table-responsive padding-none">
				<table id="header-lists" class="table">
					<thead>
						<tr>
							<th>TITLE</th>
							<th>DESCRIPTION</th>
							<th><i class="fa pull-right fa-refresh fa-spin load-refresh"></i></th>
						</tr>
					</thead>
					<tbody id="rows-header">
					</tbody>
				</table>
			</div>

			<h3>ABOUT WINSTAR</h3>
			<div class="table-responsive padding-none">
				<table id="about-lists" class="table">
					<thead>
						<tr>
							<th>BODY</th>
							<th><i class="fa pull-right fa-refresh fa-spin load-refresh"></i></th>
						</tr>
					</thead>
					<tbody id="rows-about">
					</tbody>
				</table>
			</div>

			<hr>
			<h3>WINSTAR SERVICES</h3>
			<div class="table-responsive padding-none">
				<table id="service-lists" class="table">
					<thead>
						<tr>
							<th>ICON</th>
							<th>SERVICE TITLE</th>
							<th>DESCRIPTION</th>
							<th><i class="fa pull-right fa-refresh fa-spin load-refresh"></i></th>
						</tr>
					</thead>
					<tbody id="rows-services">
					</tbody>
				</table>
			</div>

			<hr>
			<h3>CONTACT INFORMATION <i class="fa fa-refresh fa-spin load-refresh"></i><button type="button" class="btn btn-xs btn-primary btn-edit edit-contact hide" data-toggle="modal" data-target="#btn-edit"><i class="fa fa-pencil"></i></button>
			</h3>
			<div class="table-responsive padding-none">
				<table id="contact-lists" class="table">
					    <tbody id="contact">
            @foreach($contact as $row)
              <tr data-id="{{$row->id}}">
               <!--  <td>{{$row->name}}</td> -->
                <td>{{$row->address}}</td>
                <td>{{$row->phone}}</td>
                <td>{{$row->clinic}}</td>
                 <td>
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view" data-toggle="modal" data-target="#"><i class="fa fa-eye"></i></button>
                  <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit" data-toggle="modal" data-target="#"><i class="fa fa-pencil"></i></button>
                </td> 
              </tr>
            @endforeach
					</tbody>
				</table>
			</div>

		</div>

@stop
@include('other.form')