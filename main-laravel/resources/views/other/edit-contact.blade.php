<!-- @extends('layouts.backend')

@section('scripts')

@stop

@section('content')
 
<div class="container">
	@if ($errors->any())
	<div class="alert alert-dismissable alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
		@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
    </div>
	@endif
	{!! Form::model($contact, ['action' => ['OtherController@updateContact', $contact->id], 'role' => 'form', 'method' => 'PATCH', 'class' => 'form-horizontal', 'files' => true]) !!}
	<div class="well">
		<a class="btn btn-goback" href="{{ url('admin/other') }}"><i class="fa fa-arrow-left"></i> Go Back</a>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-address', 'Address', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('address', null, ['class' => 'form-control', 'id' => 'row-address']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-phone', 'Phone Number', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('phone', null, ['class' => 'form-control', 'id' => 'row-phone']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-email', 'Email Address', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('email', null, ['class' => 'form-control', 'id' => 'row-email']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-clinic', 'Clinic Hours ', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('clinic', null, ['class' => 'form-control', 'id' => 'row-clinic']) !!}
		</div>
		</div>
	
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-facebook_link', 'Facebook Link', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('facebook_link', null, ['class' => 'form-control', 'id' => 'row-facebook_link']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-twitter_link', 'Twitter Link', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('twitter_link', null, ['class' => 'form-control', 'id' => 'row-twitter_link']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-offset-3 col-sm-9">
			<button type="submit" class="btn btn-submit"><i class="fa fa-save"></i> Update Contact Info</button>
		</div>
		</div>
	</div>
	{!! Form::close() !!}
</div>

@stop -->

<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">

	@if ($errors->any())
	<div class="alert alert-dismissable alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
		@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
    </div>
	@endif
	{!! Form::model($contact, ['action' => ['OtherController@updateContact', $contact->id], 'role' => 'form', 'method' => 'PATCH', 'class' => 'form-horizontal', 'files' => true]) !!}
	<div class="well">
		<a class="btn btn-goback" href="{{ url('admin/other') }}"><i class="fa fa-arrow-left"></i> Go Back</a>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-address', 'Address', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('address', null, ['class' => 'form-control', 'id' => 'row-address']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-phone', 'Phone Number', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('phone', null, ['class' => 'form-control', 'id' => 'row-phone']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-email', 'Email Address', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('email', null, ['class' => 'form-control', 'id' => 'row-email']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-clinic', 'Clinic Hours ', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('clinic', null, ['class' => 'form-control', 'id' => 'row-clinic']) !!}
		</div>
		</div>
	
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-facebook_link', 'Facebook Link', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('facebook_link', null, ['class' => 'form-control', 'id' => 'row-facebook_link']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-twitter_link', 'Twitter Link', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('twitter_link', null, ['class' => 'form-control', 'id' => 'row-twitter_link']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-offset-3 col-sm-9">
			<button type="submit" class="btn btn-submit"><i class="fa fa-save"></i> Update Contact Info</button>
		</div>
		</div>
	</div>
	{!! Form::close() !!}
</div>