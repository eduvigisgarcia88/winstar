<!-- @section('scripts')
<script type="text/javascript">
  
    /* $('#row-photo').on('change', function() {
    var tmppath = URL.createObjectURL(event.target.files[0]);
      $("img#preview").attr('src',tmppath);
    }); */
$("input.file").fileinput({
        maxFileCount: 1,
        maxFileSize: 1024,
        allowedFileTypes: ['image'],
    allowedFileExtensions: ['jpg', 'bmp', 'png', 'jpeg'],
    });
</script>
@stop -->
<div class="modal fade" id="btn-edit" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
<!-- Modal -->
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-warning">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><i class="fa fa-plus"></i> Edit Contact</h4>
        </div>
          <div class="modal-body">
         
@if ($errors->any())
  <div class="alert alert-dismissable alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
    </div>
  @endif
{!! Form::model($contact, ['action' => ['OtherController@updateContact', '1'], 'role' => 'form', 'method' => 'PATCH', 'class' => 'form-horizontal', 'files' => true]) !!}
           
   <div class="form-group">
    <div class="col-md-3 text-right">
      {!! Form::label('row-address', 'Address', ['class' => 'control-label font-color']) !!}
    </div>
    <div class="col-md-9">
      {!! Form::text('address', null, ['class' => 'form-control', 'id' => 'row-address']) !!}
    </div>
    </div>
    <div class="form-group">
    <div class="col-md-3 text-right">
      {!! Form::label('row-phone', 'Phone Number', ['class' => 'control-label font-color']) !!}
    </div>
    <div class="col-md-9">
      {!! Form::text('phone', null, ['class' => 'form-control', 'id' => 'row-phone']) !!}
    </div>
    </div>
    <div class="form-group">
    <div class="col-md-3 text-right">
      {!! Form::label('row-email', 'Email Address', ['class' => 'control-label font-color']) !!}
    </div>
    <div class="col-md-9">
      {!! Form::text('email', null, ['class' => 'form-control', 'id' => 'row-email']) !!}
    </div>
    </div>
    <div class="form-group">
    <div class="col-md-3 text-right">
      {!! Form::label('row-clinic', 'Clinic Hours ', ['class' => 'control-label font-color']) !!}
    </div>
    <div class="col-md-9">
      {!! Form::text('clinic', null, ['class' => 'form-control', 'id' => 'row-clinic']) !!}
    </div>
    </div>
  
    <div class="form-group">
    <div class="col-md-3 text-right">
      {!! Form::label('row-facebook_link', 'Facebook Link', ['class' => 'control-label font-color']) !!}
    </div>
    <div class="col-md-9">
      {!! Form::text('facebook_link', null, ['class' => 'form-control', 'id' => 'row-facebook_link']) !!}
    </div>
    </div>
    <div class="form-group">
    <div class="col-md-3 text-right">
      {!! Form::label('row-twitter_link', 'Twitter Link', ['class' => 'control-label font-color']) !!}
    </div>
    <div class="col-md-9">
      {!! Form::text('twitter_link', null, ['class' => 'form-control', 'id' => 'row-twitter_link']) !!}
    </div>
    </div>
    <div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
      <button type="submit" class="btn btn-submit"><i class="fa fa-save"></i> Update Contact Info</button>
    </div>
    </div>
  
  {!! Form::close() !!}
          </div>
        <div class="modal-footer">
          <input type="hidden" name="id" id="row-id">
          <button type="submit" class="btn btn-default btn-success"> <i class="fa fa-floppy-o"></i> Save</button>
          <button type="button" class="btn btn-default btn-danger" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Close</button>
        </div>
           {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>


    