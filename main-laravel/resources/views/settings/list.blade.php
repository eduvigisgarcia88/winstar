@extends('layouts.backend')

@section('scripts')
<script type="text/javascript">

$("#row-inquiries").select2({
  tags: true,
  tokenSeparators: [',', ' ']
});

$("#row-applications").select2({
  tags: true,
  tokenSeparators: [',', ' ']
});
    // submit form
    $("#form-save").find("#modal-save_form").submit(function(e) {
        var form = $("#form-save").find("form");
        var loading = $("#load-form");

        $("#alert-success").addClass("hide");
        // stop form from submitting
        e.preventDefault();

        loading.removeClass('hide');
        // console.log(form.serialize());
        // push form data
        $.ajax({
            type: "post",
            url: '{{ url("admin/settings/save") }}',
            //data: form.serialize(),
            data: new FormData($("#modal-save_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,

            success: function(response) {

                if (response.unauthorized) {
                window.location.href = '{{ url("login")}}';
                }
                
                if(response.error) {
                    var errors = '<ul>';

                    $.each(response.error, function(index, value) {
                        errors += '<li>' + value + '</li>';
                    });

                    errors += '</ul>';

                    status('Please correct the following:', errors, 'alert-danger', '#form-notice');
                    $("#alert-success").addClass("hide");
                } else {

                    // status(response.title, response.body, 'alert-success');
                    $("#alert-success").removeClass("hide");
                }
                loading.addClass('hide');
            }
        });
    });

</script>
@stop

@section('content')
    
    
<div class="page-header">
  <h1>Winstar</h1>
  <ol class="breadcrumb">
    <li><a href="#">Admin</a></li>
    <li><a href="#">Settings</a></li>
  </ol>
</div>

    <div id="form-save" class="col-xs-12">
    <div id="load-form" class="loading-pane hide">
      <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
    </div>
    {!! Form::open(array('url' => 'admin/settings/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
    <div class="borderZero panel panel-default">
      <div class="panel-body">
        <div id="form-notice"></div>
        <div class="form-group">
        <div class="col-sm-2">
            {!! Form::label('row-username', 'Username', ['class' => 'font-color']) !!}
        </div>
        <div class="col-sm-10">
            <input type="text" class="form-control borderZero" name="username" id="row-username" value="{{ $settings->username }}">
        </div>
        </div>
        <div class="form-group">
        <div class="col-sm-2">
            {!! Form::label('row-email', 'Email', ['class' => 'font-color']) !!}
        </div>
        <div class="col-sm-10">
            <input type="text" class="form-control borderZero" name="email" id="row-email" value="{{ $settings->email }}">
        </div>
        </div>
        <div class="form-group">
        <div class="col-sm-2">
            {!! Form::label('row-email', 'Password', ['class' => 'font-color']) !!}
        </div>
        <div class="col-sm-10">
          <input type="password" name="password" class="borderZero form-control borderzero" id="row-password" maxlength="30" placeholder="Leave blank for unchanged. Min 6 chars">
        </div>
        </div>
        <div class="form-group">
        <div class="col-sm-2">
            {!! Form::label('row-password_confirmation', 'Confirm Password', ['class' => 'font-color']) !!}
        </div>
        <div class="col-sm-10">
          <input type="password" name="password_confirmation" class="borderZero form-control borderzero" id="row-password_confirmation" maxlength="30" placeholder="Repeat password">
        </div>
        </div>
        <div class="form-group">
        <div class="col-sm-2">
            {!! Form::label('row-email_inq', 'Main Recipient (Inquiries)', ['class' => 'font-color']) !!}
        </div>
        <div class="col-sm-10">
          <input type="text" name="email_inq" class="borderZero form-control borderzero" id="row-email_inq" value="{{ $settings->email_inq }}">
        </div>
        </div>
        <div class="form-group">
        <div class="col-sm-2">
            {!! Form::label('row-inquiries', 'Sub Recipient (Inquiries)', ['class' => 'font-color']) !!}
        </div>
        <div class="col-sm-10">
          <input type="text" name="inquiries" class="borderZero form-control borderzero" id="row-inquiries" value="{{ $settings->inquiries }}">
        </div>
        </div>
        <div class="form-group">
        <div class="col-sm-2">
            {!! Form::label('row-email_app', 'Main Recipient (Applications)', ['class' => 'font-color']) !!}
        </div>
        <div class="col-sm-10">
          <input type="text" name="email_app" class="borderZero form-control borderzero" id="row-email_app" value="{{ $settings->email_app }}">
        </div>
        </div>
        <div class="form-group">
        <div class="col-sm-2">
            {!! Form::label('row-applications', 'Sub Recipient (Applications)', ['class' => 'font-color']) !!}
        </div>
        <div class="col-sm-10">
          <input type="text" name="applications" class="borderZero form-control borderzero" id="row-applications" value="{{ $settings->applications }}">
        </div>
        </div>
      </div>
      <div class="panel-footer panelFooter">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <a class="btn btn-goback btn-back borderZero" href="{{ url('admin') }}">Back</a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                <input type="hidden" name="id" value="{{ $settings->id }}">
                <button type="submit" class="btn btn-submit btn-primary borderZero"><i class="fa fa-save"></i> Save Changes</button>
            </div>
        </div>
      </div>
    </div>
{!! Form::close() !!}
</div>
<div id="alert-success" class="col-lg-3 col-lg-offset-9 borderZero alert alert-dismissable alert-success hide">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <i class="fa fa-fw fa-exclamation-triangle"></i> Changes has been saved.
</div>

@stop