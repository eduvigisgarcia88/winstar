@extends('layouts.backend')

@section('scripts')
<script>
   $_token = '{{ csrf_token() }}';
function refresh() {
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $("#row-filter_status").val('');
      $(".th-sort").find('i').removeAttr('class');

      var loading = $(".loading-pane");
      var table = $("#rows-banner");
      var url = "{{ url('/') }}";
      loading.removeClass('hide');

      $.post("{{ url('admin/banner/refresh') }}", { _token: $_token }, function(response) {
        
        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }

        // clear table
        table.html("");
        // populate table
        $.each(response.rows, function(index, row) {
          console.log(row.status);
            table.append(
                '<tr data-id="' + row.id + '">' +
                '<td style="'+(row.status == 2 ? 'opacity: 0.5;':'')+'" height="100"><img height="100" width="200" src="'+ row.url + '?' + new Date().getTime() + '"></td>' +
                '<td style="'+(row.status == 2 ? 'opacity: 0.5;':'')+'" height="100">' + row.title + '</td>' +
                '<td height="100" class="text-right">' + 
                  '<a target="_blank" href="' + row.url + '" class="btn btn-xs borderZero btn-info btn-view" title="View Banner Image" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-eye"></i></a>' +
                  '&nbsp;<a class="btn btn-xs borderZero btn-primary btn-edit" title="Edit News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
                  (row.status == 1 ? '&nbsp;<a class="btn btn-xs borderZero btn-warning btn-disable" title="Disable Banner" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-ban"></i></a>':'&nbsp;<a class="btn btn-xs borderZero btn-success btn-enable" title="Enable Banner" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-check-square"></i></a>')+
                  
                '</td>' +
                '</tr>'
            );


        });

      loading.addClass("hide");

      }, 'json');
  }
     $("#page-content-wrapper").on("click", ".btn-add", function() {
      $("#banner-header").removeAttr('class').attr('class', 'modal-header modal-warning');
      $(".banner-title").html('<i class="fa fa-plus"></i> Add Banner');
      $(".photo-upload").removeClass("hide");
      $(".photo-preview").addClass("hide");
      $(".btn-cancel").addClass("hide");
      $("#modal-banner").find('form').trigger("reset");
      $("#banner-id").val("");
      $("#modal-banner").modal("show");
    });

    $("#page-content-wrapper").on("click", ".btn-delete", function() {
      $("#row-id").val("");
      $("#row-id").val($(this).parent().parent().data('id'));
      $("#modal-form").modal("show");
    });
    $("#page-content-wrapper").on("click", ".btn-disable", function() {
      $("#row-id").val("");
      $("#row-id").val($(this).parent().parent().data('id'));
      $("#modal-disable").modal("show");
    });
     $("#page-content-wrapper").on("click", ".btn-enable", function() {
      $("#row-id").val("");
      $("#row-id").val($(this).parent().parent().data('id'));
      $("#modal-enable").modal("show");
    });
     $("#page-content-wrapper").on("click", ".btn-edit", function() {
      var id = $(this).parent().parent().data('id');
      var btn = $(this);
      var url = "{{ url('/') }}";
      $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
      $("#modal-client").find('form').trigger("reset");
      $("#client-id").val("");
      $(".photo-upload").addClass("hide");
      $(".photo-preview").removeClass("hide");
      $(".btn-cancel").addClass("hide");

      $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");
            
      $.post("{{ url('admin/banner/manipulate') }}", { id: id, _token: $_token }, function(response) {

          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }

          if(!response.error) {
          // set form title
              $(".client-title").html('<i class="fa fa-pencil"></i> Editing <strong>' + response.title + '</strong>');

              // output form data
              $.each(response, function(index, value) {
                  var field = $("#banner-" + index);

                  if(field.length > 0) {
                  
                        field.val(value);
                    }
              });
              // show form
              $("#modal-banner").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }

          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');
    });
</script>
@stop

@section('content')
<div class="page-header">
  <h1>Winstar</h1>
  <ol class="breadcrumb">
    <li><a href="#">Admin</a></li>
    <li><a href="#">Banner Management</a></li>
  </ol>
</div>

<div class="panel panel-default">
  <div class="panel-heading panelHeader">
    <div class="row">
      <div class="col-xs-5 col-sm-6 col-lg-3">
        <h5>Banners</h5>
      </div>
      <div class="col-lg-6 col-offset-lg-3 col-xs-7 col-sm-6 text-right pull-right">

        @if($count_banners >= 3)
        @else
        <button type="button" class="btn btn-info btn-md btn-attr borderZero btn-add"><i class="fa fa-plus"></i></button> 
        @endif
        <button type="button" class="btn btn-success btn-md btn-attr borderZero" onclick="refresh()"><i class="fa fa-refresh"></i></button> 
       
      </div>
    </div>
  </div>
  <div class="panel-body">
    <div class="table-responsive">
        <div id="load-contact" class="loading-pane hide col-xs-12">
          <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
        </div>
        <table id="contact-lists" class="table table-striped table-pagination">
          <thead class="myTableThead">
            <tr>
              <th class="myTableHeader">Photo</th>
              <th class="myTableHeader">Catch Phrase</th>
              <th class="myTableHeader text-right">Tools</th>
            </tr>
          </thead>
          <tbody id="rows-banner">
            @foreach($rows as $row)
            <tr data-id = "{{$row->id}}">
              <td height="100" style="{{$row->status == 2 ? 'opacity: 0.5;':'' }}"><img height="100" width="200" src="{{$row->url}}"></td>
              <td height="100" style="{{$row->status == 2 ? 'opacity: 0.5;':'' }}">
                 <?php echo "$row->title" ?>
              </td>
              <td height="100" class="text-right">
                  <a target="_blank" href="{{$row->url}}" class="btn btn-xs borderZero btn-info btn-view" title="View Banner Image" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-eye"></i></a>
                  <a class="btn btn-xs borderZero btn-primary btn-edit" title="Edit Banner" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>
                  @if($row->status == 1)
                    <a class="btn btn-xs borderZero btn-warning btn-disable" title="Disable Banner" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-ban"></i></a>
                  @else
                    <a class="btn btn-xs borderZero btn-success btn-enable" title="Enable Banner" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-check-square"></i></a>
                  @endif
                 
              </td>
            </tr>
           @endforeach
          </tbody>
        </table>
        <br class="visible-xs">
      
      </div>
      
  </div>
</div>
    @if(Session::has("notice"))
      <div id="alert" class="col-lg-3 col-lg-offset-9 borderZero alert alert-dismissable alert-{{ Session::get('notice.type') }}">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <i class="fa fa-fw fa-exclamation-triangle"></i> {{ Session::get('notice.msg') }}
      </div>
    @endif
@stop

@include('banner.form')