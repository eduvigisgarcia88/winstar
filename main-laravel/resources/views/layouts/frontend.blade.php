<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	

    <!-- Bootstrap -->
    @if(Session::has('locale'))
        @if (Session::get('locale') == 'sa')
            <title>{{ $title_arabic }}</title>
            {!! HTML::style('/css/bootstrap.min.css') !!}
            {!! HTML::style('/css/bootstrap-rtl.min.css') !!}
        @else
            <title>{{ $title }}</title>
            {!! HTML::style('/css/bootstrap.min.css') !!}
        @endif
    @else
        <title>{{ $title }}</title>
        {!! HTML::style('/css/bootstrap.min.css') !!}
    @endif

    <!-- Font Awesome -->
    {!! HTML::style('/plugins/font-awesome/css/font-awesome.min.css') !!}

    <!-- CSS Plugins -->
    {!! HTML::style('/css/plugins/toastr.min.css') !!}
    {!! HTML::style('/plugins/components/datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Main Stylesheet -->
    {!! HTML::style('/css/style.css') !!}

</head>

<body>

@yield('content')

@if(Session::has('locale'))

    @if (Session::get('locale') == 'sa')

        @include('layouts.sa.frontend')

    @else

        @include('layouts.en.frontend')

    @endif

@else

    @include('layouts.en.frontend')

@endif

    <!-- jQuery -->
    {!! HTML::script('/plugins/components/jquery/jquery.min.js') !!}

    <!-- Bootstrap -->
    {!! HTML::script('/js/bootstrap.min.js') !!}


    <!-- Main Scripts -->
    {!! HTML::script('/js/script.js') !!}
    {!! HTML::script('/js/main.js') !!}

    <!-- JS Plugins -->
    {!! HTML::script('/js/plugins/toastr.min.js') !!}
    {!! HTML::script('/js/plugins/tweenmax.min.js') !!}
    {!! HTML::script('/js/plugins/scrolltoplugin.min.js') !!}
    {!! HTML::script('/plugins/components/moment/min/moment.min.js') !!}
    {!! HTML::script('/plugins/components/moment/min/locales.js') !!}
    {!! HTML::script('/plugins/components/datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
    <!--{!! HTML::script('/plugins/components/datetimepicker/bootstrap-datetimepicker-built.js') !!} -->

    @yield('scripts')

    <script type="text/javascript">
        
    $(document).ready(function() {
        //datepicker
            $(function () {
                $('#datetimepicker1').datetimepicker({
                    locale: 'en'
                });
            });

        $(".btn-booknow").on('click', function() {
            $("#modal-form").find('form').trigger('reset');
            $("#modal-form").modal('show');
        });

    });

    </script>

</body>

</html>
