<!DOCTYPE html>
<html lang="en-US">
  <head>
		<meta charset="utf-8">
  </head>
  <body style="font-family: Helvetica, Arial, sans-serif;">
  	<h2>Winstar Loans and Credits</h2>
  	<div style="margin-top: 20px;">
	  	@yield('content')
		</div>
		<div style="margin-top: 20px; border-top: 1px solid #eee; padding-top: 10px; font-size: 0.9em;">
			<div>&copy; {{ date('Y') }} Winstar Loans and Credits</div>
		</div>
  </body>
</html>