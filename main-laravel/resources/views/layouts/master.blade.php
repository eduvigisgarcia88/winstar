<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	

 <!-- For editor -->
  <!-- <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" />
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.css" /> -->


    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{{ $title }}</title>

    <!-- Bootstrap -->
     {!! HTML::style('/css/bootstrap.min.css') !!}
     {!! HTML::style('/font-awesome/css/font-awesome.min.css') !!}
     {!! HTML::style('/css/styles.css') !!}
     {!! HTML::style('/css/common.css') !!}
     {!! HTML::style('/css/backend.css') !!}
     {!! HTML::style('/plugins/css//toastr.min.css') !!}

     <!-- Editor -->
     {!! HTML::style('/plugins/css/summernote.css') !!}




    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
    <div class="container-fluid">
        <div class="row">
          <nav class="navbar navbar-default">
            <div class="container-fluid">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{url('admin/dashboard')}}"><img alt="Brand" src="{{ url()}}/img/yakolaklogo.png" height="70" width="100" class="img-responsive"></a>
              </div>
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">User Management</a>
                    <ul class="dropdown-menu">
                      <li{{ Route::current()->getPath() == 'admin/users' ? ' class=active' : '' }}><a href="{{ url('admin/users') }}" ><span id="hide" data-toggle="tooltip"  data-placement="right">Front-End Users</span></a></li>
                      <li><a href="#">Back-End Users</a></li>
                    </ul>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Ad Management</a>
                    <ul class="dropdown-menu">
                      <li{{ Route::current()->getPath() == 'admin/ads' ? ' class=active' : '' }}><a href="{{url('admin/ads')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Manage Ads</span></a></li>
                    </ul>
                  <!--   <ul class="dropdown-menu">
                      <li><a href="#"></a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">One more separated link</a></li>
                    </ul> -->
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Category Management</a>
                    <ul class="dropdown-menu">
                      <li{{ Route::current()->getPath() == 'admin/category' ? ' class=active' : '' }}><a href="{{url('admin/category')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Manage Categories</span></a></li>
                    </ul>
                   <!--  <ul class="dropdown-menu">
                      <li><a href="#"></a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">One more separated link</a></li>
                    </ul> -->
                  </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
<input type="hidden" value="PVCCn3FqAlNCV4ulpJu205eapLO2QqnAXm234aAOBZY=">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img class="img-circle" src="{{ url()}}/img/avatar.jpg" width="20" height="20"> Hi {{ Auth::user()->name }}! <span class="badge">5</span> <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#"></a></li>
                        <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                      <!--   <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li> -->
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ url('admin/logout')}}"><i class="fa fa-sign-out"></i> Logout</a></li>
                      </ul>
                    </li>
                  </ul>
              </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
          </nav>
        </div>

      <div id="content">
        @yield('content')
      </div>
    </div>
      <!-- jQuery -->
    {!! HTML::script('/js/jquery.js') !!}
    {!! HTML::script('/js/form.js') !!}
    {!! HTML::script('/plugins/js//toastr.min.js') !!}
    
    <!-- Bootstrap -->
    {!! HTML::script('/js/bootstrap.min.js') !!}
     @yield('scripts')
    <!-- Editor -->
    {!! HTML::script('/plugins/js/summernote.js') !!}
  </body>
</html>