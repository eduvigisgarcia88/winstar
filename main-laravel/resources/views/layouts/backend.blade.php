<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="_token" content="{{ csrf_token() }}">
    <title>{{ $title }}</title>

    <!-- include summernote -->
    <!-- Editor -->
    {!! HTML::style('/plugins/editor/summernote.css') !!}

    <!-- Bootstrap -->
    {!! HTML::style('/css/bootstrap.min.css') !!}

    <!-- Font Awesome -->
    {!! HTML::style('/plugins/font-awesome/css/font-awesome.min.css') !!}

    <!-- CSS Plugins -->
    {!! HTML::style('/css/plugins/toastr.min.css') !!}
    {!! HTML::style('/css/plugins/fileinput.css') !!}
    {!! HTML::style('/plugins/components/datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}
    {!! HTML::style('/css/plugins/select2-bootstrap.css') !!}
    {!! HTML::style('/css/plugins/select2.css') !!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Main Stylesheet -->
    {!! HTML::style('/css/backend-style.css') !!}
    {!! HTML::style('/css/sidenav.css') !!}
    {!! HTML::style('/css/backendContent.css') !!}
</head>

<body>
<div id="wrapper">
<nav id="navbar-backend" class="navbar navbar-inverse navbar-fixed-top visible-xs" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle borderZero" data-toggle="collapse" data-target=".navbar-ex-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Logo</a>
    </div>
    <div class="collapse navbar-collapse navbar-right navbar-ex-collapse">
    <ul class="nav navbar-nav">
    <li{{ Route::current()->getPath() == 'admin' ? ' class=active' : '' }}><a href="{{ url('admin') }}"></i>Dashboard</a></li>
    @if (Auth::user()->usertype_id == 1)
    <li{{ Route::current()->getPath() == 'admin/news' | Route::current()->getPath() == 'admin/news/create' ? ' class=active' : '' | Route::current()->getPath() == 'admin/news/{id}/edit' ? ' class=active' : '' }}><a href="{{ url('admin/news') }}"></i>News</a></li>
    @endif
    <li{{ Route::current()->getPath() == 'admin/inquiry' ? ' class=active' : '' }}><a href="{{ url('admin/inquiry') }}">Inquiries</a></li>
    <li{{ Route::current()->getPath() == 'admin/application' ? ' class=active' : '' }}><a href="{{ url('admin/application') }}">Applications</a></li>
    <li{{ Route::current()->getPath() == 'admin/services' ? ' class=active' : '' }}><a href="{{ url('admin/services') }}">Services</a></li>
    <li{{ Route::current()->getPath() == 'admin/clients' ? ' class=active' : '' }}><a href="{{ url('admin/clients') }}">Clients</a></li>
    <li{{ Route::current()->getPath() == 'admin/contact' ? ' class=active' : '' }}><a href="{{ url('admin/contact') }}">Contact Information</a></li>
    <li{{ Route::current()->getPath() == 'admin/loans' ? ' class=active' : '' }}><a href="{{ url('admin/loans') }}">Loan Amortization</a></li>
    @if (Auth::user()->usertype_id == 1)
    <li{{ Route::current()->getPath() == 'admin/custom' ? ' class=active' : '' | Route::current()->getPath() == 'admin/custom/header/{id}/edit' ? ' class=active' : '' | Route::current()->getPath() == 'admin/other/about/{id}/edit' ? ' class=active' : '' | Route::current()->getPath() == 'admin/other/service/{id}/edit' ? ' class=active' : '' | Route::current()->getPath() == 'admin/other/contact/{id}/edit' ? ' class=active' : '' }}><a href="{{ url('admin/custom') }}">Custom Pages</a></li>
    <li{{ Route::current()->getPath() == 'admin/custom/banner' ? ' class=active' : '' }}><a href="{{ url('admin/banner') }}"></i>Custom Banner</a></li>
    @endif
    <li{{ Route::current()->getPath() == 'admin/settings' ? ' class=active' : '' }}><a href="{{ url('admin/settings') }}">Settings</a></li>
    <li{{ Route::current()->getPath() == 'logout' ? ' class=active' : '' }}><a href="{{ url('logout') }}">Logout<i class="fa fa-sign-out sidebarIcon pull-right"></i></a></li>
      </ul>
    </div>
  </div>
</nav>

    <!-- Sidebar -->
    <div id="sidebar-wrapper" class="">
        <ul class="sidebar-nav">
            <li class="backendLogo sidebar-brand">
                <a target="_blank" href="{{ url('/') }}">
                    <img id="main-logo" class="animated fadeIn img-responsive opaque-logo en-logo" style="padding-top: 10px; padding-left: 15px; width: 150px;" src="{{ url('images/logo') . '/winstar-logo.png'}}">
                </a>
            </li>
            <li{{ Route::current()->getPath() == 'admin' ? ' class=active' : '' }}><a href="{{ url('admin') }}"></i>Dashboard</a></li>
    @if (Auth::user()->usertype_id == 1)
    <li{{ Route::current()->getPath() == 'admin/news' | Route::current()->getPath() == 'admin/news/create' ? ' class=active' : '' | Route::current()->getPath() == 'admin/news/{id}/edit' ? ' class=active' : '' }}><a href="{{ url('admin/news') }}"></i>News</a></li>
    @endif

    <li{{ Route::current()->getPath() == 'admin/inquiry' ? ' class=active' : '' }}><a href="{{ url('admin/inquiry') }}">Inquiries</a></li>
    <li{{ Route::current()->getPath() == 'admin/application' ? ' class=active' : '' }}><a href="{{ url('admin/application') }}">Applications</a></li>
    <li{{ Route::current()->getPath() == 'admin/services' ? ' class=active' : '' }}><a href="{{ url('admin/services') }}">Services</a></li>
    <li{{ Route::current()->getPath() == 'admin/clients' ? ' class=active' : '' }}><a href="{{ url('admin/clients') }}">Clients</a></li>
    <li{{ Route::current()->getPath() == 'admin/contact' ? ' class=active' : '' }}><a href="{{ url('admin/contact') }}">Contact Information</a></li>
    <li{{ Route::current()->getPath() == 'admin/loans' ? ' class=active' : '' }}><a href="{{ url('admin/loans') }}">Loan Amortization</a></li>
    
    @if (Auth::user()->usertype_id == 1)
    <li{{ Route::current()->getPath() == 'admin/custom' ? ' class=active' : '' | Route::current()->getPath() == 'admin/custom/header/{id}/edit' ? ' class=active' : '' | Route::current()->getPath() == 'admin/other/about/{id}/edit' ? ' class=active' : '' | Route::current()->getPath() == 'admin/other/service/{id}/edit' ? ' class=active' : '' | Route::current()->getPath() == 'admin/other/contact/{id}/edit' ? ' class=active' : '' }}><a href="{{ url('admin/custom') }}">Custom Pages</a></li>
    <li{{ Route::current()->getPath() == 'admin/banner' ? ' class=active' : '' }}><a href="{{ url('admin/banner') }}"></i>Banner</a></li>
    @endif
    <li role="presentation" class="divider"></li>

    <li{{ Route::current()->getPath() == 'admin/settings' ? ' class=active' : '' }}><a href="{{ url('admin/settings') }}">Settings</a></li>
    <li{{ Route::current()->getPath() == 'logout' ? ' class=active' : '' }}><a href="{{ url('logout') }}">Logout<i class="fa fa-sign-out sidebarIcon pull-right"></i></a></li>

        </ul>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                    @yield('content')
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->



    <!-- jQuery -->
    {!! HTML::script('/plugins/components/jquery/jquery.min.js') !!}
    {!! HTML::script('/js/jquery.form.js') !!}

    <!-- Bootstrap -->
    {!! HTML::script('/js/bootstrap.min.js') !!}
    {!! HTML::script('/js/plugins/underscore.js') !!}
    <!-- Main Scripts -->
    {!! HTML::script('/js/script.js') !!}
    {!! HTML::script('/js/main.js') !!}

    <!-- JS Plugins -->
    {!! HTML::script('/js/plugins/toastr.min.js') !!}
    {!! HTML::script('/js/plugins/tweenmax.min.js') !!}
    {!! HTML::script('/js/plugins/iconpicker.js') !!}
    {!! HTML::script('/js/plugins/jquery.ui.pos.js') !!}
    {!! HTML::script('/js/plugins/scrolltoplugin.min.js') !!}
    {!! HTML::script('/js/plugins/readmore.min.js') !!}
    {!! HTML::script('/js/plugins/fileinput.js') !!}
    {!! HTML::script('/js/plugins/select2.min.js') !!}
    {!! HTML::script('/plugins/components/moment/min/moment.min.js') !!}
    {!! HTML::script('/plugins/components/datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}

    <!-- Editor -->
    {!! HTML::script('/plugins/editor/summernote.js') !!}

    @yield('scripts')

<script type="text/javascript">
    

      // delete news
      $(".btn-settings").on("click", function() {
        $("#settings-id").val("");
        $("#modal-settings").find('form').trigger("reset");
        $("#modal-settings").modal("show");
      });


</script>

</body>

</html>