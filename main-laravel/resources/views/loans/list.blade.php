@extends('layouts.backend')

@section('scripts')
<script type="text/javascript">

  $_token = '{{ csrf_token() }}';
  $loading_expand = false;
  $expand_id = 0;

	// refresh the list
	function refresh() {
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $("#row-filter_status").val('');
      $(".th-sort").find('i').removeAttr('class');

	  $loading_expand = false;
	  $expand_id = 0;
      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();

			var loading = $(".loading-pane");
			var table = $("#rows");
			var url = "{{ url('/') }}";
			loading.removeClass('hide');

      $.post("{{ url('admin/loans/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, per: $per, _token: $_token }, function(response) {
				
        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }
        // clear table
				table.html("");
				// populate table
        var download_url = "{{ url('admin/loans/download') }}";
        var download_token = '{!! csrf_field() !!}';

				$.each(response.rows.data, function(index, row) {
	
						table.append(
								'<tr data-id="' + row.id + '">' +
								'<td><a class="btn btn-xs borderZero btn-default btn-expand" title="View Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-plus"></i></a>&nbsp;' + row.account_no + '</td>' +
								'<td>' + row.name + '</td>' +
								'<td>' + moment(row.created_at).format('LLL') + '</td>' +
                (row.get_loan_amortization.length > 0 ?
                '<td class="text-right">' + 
                 '<form role="form" method="post" action="' + download_url + '">' + download_token +
                  '<a class="btn btn-info btn-xs btn-add-amortization borderZero" style="margin-bottom: 5px;" data-id="' + row.id + '"><i class="fa fa-plus"></i> Add Amortization</a>' +
                  '&nbsp;<a data-id="' + row.id + '" class="btn btn-xs borderZero btn-primary btn-edit" title="Edit Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
                  '&nbsp;<a data-id="' + row.id + '" class="btn btn-xs borderZero btn-danger btn-delete" title="Delete Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-trash"></i></a>' +
                  '&nbsp;<button type="submit" class="btn btn-xs borderZero btn-success btn-download" data-toggle="tooltip" title="Download" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-download"></i></button>' +
                  '<input type="hidden" name="id" value="' + row.id + '">' +
                '</td>' : 
                '<td class="text-right">' + 
                  '<a class="btn btn-info btn-xs btn-add-amortization borderZero" style="margin-bottom: 5px;" data-id="' + row.id + '"><i class="fa fa-plus"></i> Add Amortization</a>' +
                  '&nbsp;<a data-id="' + row.id + '" class="btn btn-xs borderZero btn-primary btn-edit" title="Edit Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
                  '&nbsp;<a data-id="' + row.id + '" class="btn btn-xs borderZero btn-danger btn-delete" title="Delete Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-trash"></i></a>' +
                  '&nbsp;<a disabled class="btn btn-xs borderZero btn-success btn-no-download" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-download"></i></a>' +
                '</td>' ) +
								'</tr>'
						);
				});

			// update links
			$("#row-pages").html(response.pages);

			$(".sort-refresh").find('i').addClass('fa fa-sort-up');
      loading.addClass("hide");

      }, 'json');
	}
	
    function search() {
      var loading = $(".loading-pane");
      var table = $("#rows");

	  $loading_expand = false;
	  $expand_id = 0;
      $("#row-page").val(1);
      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
    	$search = $("#row-search").val();
			var url = "{{ url('/') }}";

      loading.removeClass("hide");

      $.post("{{ url('admin/loans/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, per: $per, _token: $_token }, function(response) {
				
        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }

        // clear table
        table.html("");
        // populate table
        var download_url = "{{ url('admin/loans/download') }}";
        var download_token = '{!! csrf_field() !!}';

        $.each(response.rows.data, function(index, row) {
  
            table.append(
                '<tr data-id="' + row.id + '">' +
                '<td><a class="btn btn-xs borderZero btn-default btn-expand" title="View Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-plus"></i></a>&nbsp;' + row.account_no + '</td>' +
                '<td>' + row.name + '</td>' +
                '<td>' + moment(row.created_at).format('LLL') + '</td>' +
                (row.get_loan_amortization.length > 0 ?
                '<td class="text-right">' + 
                 '<form role="form" method="post" action="' + download_url + '">' + download_token +
                  '<a class="btn btn-info btn-xs btn-add-amortization borderZero" style="margin-bottom: 5px;" data-id="' + row.id + '"><i class="fa fa-plus"></i> Add Amortization</a>' +
                  '&nbsp;<a data-id="' + row.id + '" class="btn btn-xs borderZero btn-primary btn-edit" title="Edit Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
                  '&nbsp;<a data-id="' + row.id + '" class="btn btn-xs borderZero btn-danger btn-delete" title="Delete Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-trash"></i></a>' +
                  '&nbsp;<button type="submit" class="btn btn-xs borderZero btn-success btn-download" data-toggle="tooltip" title="Download" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-download"></i></button>' +
                  '<input type="hidden" name="id" value="' + row.id + '">' +
                '</td>' : 
                '<td class="text-right">' + 
                  '<a class="btn btn-info btn-xs btn-add-amortization borderZero" style="margin-bottom: 5px;" data-id="' + row.id + '"><i class="fa fa-plus"></i> Add Amortization</a>' +
                  '&nbsp;<a data-id="' + row.id + '" class="btn btn-xs borderZero btn-primary btn-edit" title="Edit Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
                  '&nbsp;<a data-id="' + row.id + '" class="btn btn-xs borderZero btn-danger btn-delete" title="Delete Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-trash"></i></a>' +
                  '&nbsp;<a disabled class="btn btn-xs borderZero btn-success btn-no-download" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-download"></i></a>' +
                '</td>' ) +
                '</tr>'
            );
        });

			// update links
			$("#row-pages").html(response.pages);

      loading.addClass("hide");

      }, 'json');
    } 

	$(document).ready(function() {
	// refresh();

    $(function () {
        $('#date_acc-picker').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
    });

		// delete news
		$("#page-content-wrapper").on("click", ".btn-add", function() {
			$("#loan-id").val("");
      $(".loan-title").html('<i class="fa fa-plus"></i> Add Loan');
      $("#modal-loan").find('form').trigger("reset");
			$("#modal-loan").modal("show");
		});

		// delete news
		$("#page-content-wrapper").on("click", ".btn-add-amortization", function() {
			$("#amortization-id").val("");
      $(".amortization-modal-title").html('<i class="fa fa-plus"></i> Add Amortization');
      $("#modal-amortization").find('form').trigger("reset");
			$("#amortization-loan_id").val($(this).data('id'));
			$("#modal-amortization").modal("show");
		});

		// delete news
		$("#page-content-wrapper").on("click", ".btn-delete", function() {
			$("#row-id").val("");
			$("#row-id").val($(this).data('id'));
			$("#modal-form").modal("show");
		});

		// delete news
		$("#page-content-wrapper").on("click", ".btn-view-amortization", function() {
      $("#view-amortization-preview").html("");
			var id = $(this).data('id');
			var loan_id = $(this).data('loan_id');
      var title = $(this).parent().parent().find('td:nth-child(1)').html();

      var btn = $(this);
      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");

			$('#preview-notice').html("");

      $.post("{{ url('admin/loans/view-amortize') }}", { id: id, loan_id: loan_id, _token: $_token }, function(response) {
      	
        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }
        if (!response.error) {
      		$(".view-amortization-title").html('<i class="fa fa-eye"></i> Viewing <strong>' + title + '</strong>');
      		$("#view-amortization-preview").html(response);
					$("#modal-view-amortization").modal("show");
      	} else {
      		status(response.title, response.error, 'alert-danger');
				}

          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');
		});

		// delete news
		$("#page-content-wrapper").on("click", ".btn-preview", function() {
			var id = $("#amortization-id").val();
			var loan_id = $("#amortization-loan_id").val();
			var loan_amount = $("#amortization-loan_amount").val();
			var interest_rate = $("#amortization-interest_rate").val();
			var term_of_loan = $("#amortization-term_of_loan").val();
			var title = $("#amortization-title").val();
			var date_acc = $("#amortization-date_acc").val();
      
			$('#preview-notice').html("");

	      $.post("{{ url('admin/loans/preview') }}", { id: id, loan_id: loan_id, loan_amount: loan_amount, interest_rate: interest_rate, term_of_loan: term_of_loan, title: title, date_acc: date_acc, _token: $_token }, function(response) {
	      	
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }

          if (!response.error) {
	      		$("#amortization-preview").html(response);
	      	} else {
						var errors = '<ul>';

						$.each(response.error, function(index, value) {
							errors +=  '<li>' + value + '</li>';
						});

						errors += '</ul>';
						status('Please correct the following:', errors, 'alert-danger', '#amortization-notice');
					}
	      }, 'json');

			});

		// delete news
		$("#page-content-wrapper").on("click", ".btn-delete-amortization", function() {
			$("#delete-amortization-id").val("");
			$("#delete-amortization-id").val($(this).data('id'));
			$("#modal-delete-amortization").modal("show");
		});

    // edit client
    $("#page-content-wrapper").on("click", ".btn-edit", function() {
      var id = $(this).data('id');
      var btn = $(this);
      var url = "{{ url('/') }}";
      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      $("#modal-loan").find('form').trigger("reset");
      $("#loan-id").val("");

      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");

      $.post("{{ url('admin/loans/edit') }}", { id: id, _token: $_token }, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }

          if(!response.error) {
          // set form title
              $(".loan-title").html('<i class="fa fa-pencil"></i> Editing <strong>' + response.account_no + '</strong>');

              // output form data
              $.each(response, function(index, value) {
                  var field = $("#loan-" + index);

                  if(field.length > 0) {
                        field.val(value);
                  }
              });
              // show form
              $("#modal-loan").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }

          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');
    });

    // edit client
    $("#page-content-wrapper").on("click", ".btn-edit-amortization", function() {
      var id = $(this).parent().parent().data('id');
      var btn = $(this);
      var title = $(this).parent().parent().find('td:nth-child(1)').html();
      var url = "{{ url('/') }}";
      $("#amortization-header").removeAttr('class').attr('class', 'modal-header modal-default');
      $("#modal-amortization").find('form').trigger("reset");
      $("#amortization-id").val("");

      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");
            
      $.post("{{ url('admin/loans/edit-amortize') }}", { id: id, _token: $_token }, function(response) {
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }
          if(!response.error) {
          // set form title
              $(".amortization-modal-title").html('<i class="fa fa-pencil"></i> Editing <strong>' + title + '</strong>');

              // output form data
              $.each(response, function(index, value) {
                  var field = $("#amortization-" + index);

                  if(field.length > 0) {
                  	if(field.parent().hasClass('date')) {
                        field.val(moment(value).format('MM/DD/YYYY'));
                  	} else {
                        field.val(value);
                  	}
                  }
              });
              // show form
              $("#modal-amortization").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }

          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');
    });



    $("#page-content-wrapper").on("click", ".btn-expand", function() {
      // $(this).find('i').toggleClass('fa-plus fa-minus');
       $(".btn-expand").html('<i class="fa fa-minus"></i>');
       $(".btn-expand").html('<i class="fa fa-plus"></i>');


      $type_id = 0;

      if ($loading_expand){
        return;
      }
      $tr = $(this).parent().parent();
      $id = $tr.data('id');
      $_token = '{{ csrf_token() }}';

      $selected = '.row-expand-' + $expand_id;
        $($selected).slideUp(function () {
          $($selected).parent().parent().remove();
        });

      if ($id == $expand_id) {
        $expand_id = 0;
        return
      }

      $expand_id = $id;
      $button = $(this);
      $button.html('<i class="fa fa-spinner fa-spin"></i>');
      $loading_expand = true;


      $.post("{{ url('admin/loans/view') }}", { id: $id, _token: $_token }, function(response) {

        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }
        if (!response.error) {
          $tr.after('<tr><td colspan="4" style="padding:10px"><div class="row-expand-' + $expand_id + '" style="display:none;">' + response + '</div></td></tr>');

          $('.row-expand-' + $expand_id).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
          $loading_expand = false;
        } else {
          status(false, response.error, 'alert-danger');
        }

      });

    });


	});

    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {

      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

	  $loading_expand = false;
	  $expand_id = 0;
        $('.loading-pane').removeClass('hide');
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $status = $("#row-filter_status").val();
        $per = $("#row-per").val();
				var table = $("#rows");
				var url = "{{ url('/') }}";

        var loading = $(".loading-pane");
        loading.removeClass("hide");

        $.post("{{ url('admin/loans/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, per: $per, _token: $_token }, function(response) {
				
        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }
        // clear table
        table.html("");
        // populate table
        var download_url = "{{ url('admin/loans/download') }}";
        var download_token = '{!! csrf_field() !!}';

        $.each(response.rows.data, function(index, row) {
  
            table.append(
                '<tr data-id="' + row.id + '">' +
                '<td><a class="btn btn-xs borderZero btn-default btn-expand" title="View Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-plus"></i></a>&nbsp;' + row.account_no + '</td>' +
                '<td>' + row.name + '</td>' +
                '<td>' + moment(row.created_at).format('LLL') + '</td>' +
                (row.get_loan_amortization.length > 0 ?
                '<td class="text-right">' + 
                 '<form role="form" method="post" action="' + download_url + '">' + download_token +
                  '<a class="btn btn-info btn-xs btn-add-amortization borderZero" style="margin-bottom: 5px;" data-id="' + row.id + '"><i class="fa fa-plus"></i> Add Amortization</a>' +
                  '&nbsp;<a data-id="' + row.id + '" class="btn btn-xs borderZero btn-primary btn-edit" title="Edit Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
                  '&nbsp;<a data-id="' + row.id + '" class="btn btn-xs borderZero btn-danger btn-delete" title="Delete Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-trash"></i></a>' +
                  '&nbsp;<button type="submit" class="btn btn-xs borderZero btn-success btn-download" data-toggle="tooltip" title="Download" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-download"></i></button>' +
                  '<input type="hidden" name="id" value="' + row.id + '">' +
                '</td>' : 
                '<td class="text-right">' + 
                  '<a class="btn btn-info btn-xs btn-add-amortization borderZero" style="margin-bottom: 5px;" data-id="' + row.id + '"><i class="fa fa-plus"></i> Add Amortization</a>' +
                  '&nbsp;<a data-id="' + row.id + '" class="btn btn-xs borderZero btn-primary btn-edit" title="Edit Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
                  '&nbsp;<a data-id="' + row.id + '" class="btn btn-xs borderZero btn-danger btn-delete" title="Delete Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-trash"></i></a>' +
                  '&nbsp;<a disabled class="btn btn-xs borderZero btn-success btn-no-download" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-download"></i></a>' +
                '</td>' ) +
                '</tr>'
            );
        });

				// update links
				$("#row-pages").html(response.pages);

        loading.addClass("hide");

        }, 'json');
      
    });

    $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $per = $("#row-per").val();

	  $loading_expand = false;
	  $expand_id = 0;
        var loading = $(".loading-pane");
        var table = $("#rows");
				var url = "{{ url('/') }}";

        loading.removeClass("hide");

        $.post("{{ url('admin/loans/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, per: $per, _token: $_token }, function(response) {
    				
        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }
            // clear table
            table.html("");
            // populate table
            var download_url = "{{ url('admin/loans/download') }}";
            var download_token = '{!! csrf_field() !!}';

            $.each(response.rows.data, function(index, row) {
      
              table.append(
                  '<tr data-id="' + row.id + '">' +
                  '<td><a class="btn btn-xs borderZero btn-default btn-expand" title="View Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-plus"></i></a>&nbsp;' + row.account_no + '</td>' +
                  '<td>' + row.name + '</td>' +
                  '<td>' + moment(row.created_at).format('LLL') + '</td>' +
                  (row.get_loan_amortization.length > 0 ?
                  '<td class="text-right">' + 
                   '<form role="form" method="post" action="' + download_url + '">' + download_token +
                    '<a class="btn btn-info btn-xs btn-add-amortization borderZero" style="margin-bottom: 5px;" data-id="' + row.id + '"><i class="fa fa-plus"></i> Add Amortization</a>' +
                    '&nbsp;<a data-id="' + row.id + '" class="btn btn-xs borderZero btn-primary btn-edit" title="Edit Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
                    '&nbsp;<a data-id="' + row.id + '" class="btn btn-xs borderZero btn-danger btn-delete" title="Delete Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-trash"></i></a>' +
                    '&nbsp;<button type="submit" class="btn btn-xs borderZero btn-success btn-download" data-toggle="tooltip" title="Download" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-download"></i></button>' +
                    '<input type="hidden" name="id" value="' + row.id + '">' +
                  '</td>' : 
                  '<td class="text-right">' + 
                    '<a class="btn btn-info btn-xs btn-add-amortization borderZero" style="margin-bottom: 5px;" data-id="' + row.id + '"><i class="fa fa-plus"></i> Add Amortization</a>' +
                    '&nbsp;<a data-id="' + row.id + '" class="btn btn-xs borderZero btn-primary btn-edit" title="Edit Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
                    '&nbsp;<a data-id="' + row.id + '" class="btn btn-xs borderZero btn-danger btn-delete" title="Delete Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-trash"></i></a>' +
                    '&nbsp;<a disabled class="btn btn-xs borderZero btn-success btn-no-download" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-download"></i></a>' +
                  '</td>' ) +
                  '</tr>'
              );
            });

				// update links
				$("#row-pages").html(response.pages);

        loading.addClass("hide");

        }, 'json');
      }
    });
</script>
@stop

@section('content')
<div class="page-header">
  <h1>Winstar</h1>
  <ol class="breadcrumb">
    <li><a href="#">Admin</a></li>
    <li><a href="#">Loan</a></li>
  </ol>
</div>

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading panelHeader">
  	<div class="row">
  		<div class="col-xs-4 col-sm-6 col-lg-3">
  			<input type="text" class="form-control borderZero" placeholder="Search for..." onkeyup="search()" id="row-search">
  		</div>
  		<div class="col-lg-6 col-offset-lg-3 col-xs-8 col-sm-6 text-right pull-right">
  			<a class="btn btn-warning btn-md btn-attr btn-add borderZero"><i class="fa fa-plus"></i> Add Loan</a>
  			<button type="button" class="btn btn-success btn-md btn-attr borderZero" onclick="refresh()"><i class="fa fa-refresh"></i></button> 
  		</div>
  	</div>
  </div>
  <div class="panel-body">
  	<div class="table-responsive">
        <div class="loading-pane hide">
          <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
        </div>
				<table id="news-lists" class="table table-striped table-pagination">
					<thead class="myTableThead">
						<tr>
              <th class="th-sort myTableHeader" data-sort="account_no"><i></i> Account No.</th>
							<th class="th-sort myTableHeader" data-sort="name"><i></i> Name</th>
							<th class="th-sort myTableHeader sort-refresh" data-sort="created_at"><i class="fa fa-sort-up"></i> Date Created</th>
							<th class="text-right myTableHeader"><i id="load-list" class="fa fa-refresh fa-spin pull-right hide"></i></th>
						</tr>
					</thead>
					<tbody id="rows">
						@foreach($rows as $row)
						<tr data-id="{{ $row->id }}">
							<td><a class="btn btn-xs borderZero btn-default btn-expand" title="View Loan" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-plus"></i></a>
								 {{ $row->account_no }}</td>
							<td>{{ $row->name }}</td>
							<td>{{ date_format(new DateTime($row->created_at), 'F d, Y g:i A') }}</td>
							<td class="text-right">
                @if (count($row->getLoanAmortization) > 0)
  								<form role="form" method="post" action="{{ url('admin/loans/download') }}">
  								{!! csrf_field() !!}
    								<a class="btn btn-info btn-xs btn-add-amortization borderZero" style="margin-bottom: 5px;" data-id="{{ $row->id }}"><i class="fa fa-plus"></i> Add Amortization</a>
    								<a data-id="{{ $row->id }}" class="btn btn-xs borderZero btn-primary btn-edit" data-toggle="tooltip" title="Edit" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>
    								<a data-id="{{ $row->id }}" class="btn btn-xs borderZero btn-danger btn-delete" data-toggle="tooltip" title="Delete" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-trash"></i></a>
    								<button type="submit" class="btn btn-xs borderZero btn-success btn-download" data-toggle="tooltip" title="Download" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-download"></i></button>
    								<input type="hidden" name="id" value="{{ $row->id }}">
  								</form>
                @else
                  <a class="btn btn-info btn-xs btn-add-amortization borderZero" style="margin-bottom: 5px;" data-id="{{ $row->id }}"><i class="fa fa-plus"></i> Add Amortization</a>
                  <a data-id="{{ $row->id }}" class="btn btn-xs borderZero btn-primary btn-edit" data-toggle="tooltip" title="Edit" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>
                  <a data-id="{{ $row->id }}" class="btn btn-xs borderZero btn-danger btn-delete" data-toggle="tooltip" title="Delete" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-trash"></i></a>
                  <a disabled class="btn btn-xs borderZero btn-success btn-no-download" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-download"></i></button>
                  <input type="hidden" name="id" value="{{ $row->id }}">
                @endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
  </div>
  <div class="panel-footer panelFooter">
  	<div class="row">
  		 <div id="row-pages" class="col-lg-6">{!! $pages !!}</div>
  		<div class="col-lg-6">
        <select class="form-control borderZero pull-right" onchange="search()" id="row-per" style="margin: 0; width:70px; border: 0px;">
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="50">100</option>
        </select>
  		</div>
  	</div>
  </div>
</div>
	<div class="content-wrapper">
        <div class="text-center">
          <input type="hidden" id="row-page" value="1">
          <input type="hidden" id="row-sort" value="">
          <input type="hidden" id="row-order" value="">
        </div>       
        </div>
      </div>

@include('loans.form')

@stop