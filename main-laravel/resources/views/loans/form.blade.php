<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content ajax-submit borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'admin/loans/delete', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}
			<div class="modal-header modal-delete">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-trash"></i> Delete Loan</h4>
			</div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Do you want to delete this loan?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="row-id" value="">
				<button type="submit" class="btn btn-submit btn-danger borderZero"><i class="fa fa-trash"></i> Yes</button>
				<button type="button" class="btn btn-info borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="modal fade" id="modal-delete-amortization" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content ajax-submit borderZero">
		    <div id="load-delete-amortization" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'admin/loans/delete-amortize', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}
			<div class="modal-header modal-delete">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="delete-amortization-title"><i class="fa fa-trash"></i> Delete Amortization</h4>
			</div>
			<div class="modal-body">
				<div id="delete-amortization-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Do you want to delete this amortization?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="delete-amortization-id" value="">
				<button type="submit" class="btn btn-submit btn-danger borderZero"><i class="fa fa-trash"></i> Yes</button>
				<button type="button" class="btn btn-info borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="modal fade" id="modal-loan" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content borderZero">
		    <div id="load-loan" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'admin/loans/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_loan', 'files' => true)) !!}
			<div class="modal-header modal-warning">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="loan-title"><i class="fa fa-plus"></i> Add Loan</h4>
			</div>
			<div class="modal-body">
				<div id="loan-notice"></div>
                <div class="form-group">
                    <label for="loan-account_no" class="col-sm-2 col-xs-5 font-color">Account No.</label>
                <div class="col-sm-10 col-xs-7">
                  <input type="text" name="account_no" class="borderZero form-control borderzero" id="loan-account_no" maxlength="30">
                </div>
                </div>
                <div class="form-group">
                    <label for="loan-name" class="col-sm-2 col-xs-5 font-color">Name</label>
                <div class="col-sm-10 col-xs-7">
                  <input type="text" name="name" class="borderZero form-control borderzero" id="loan-name" maxlength="30">
                </div>
                </div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="loan-id" value="">
				<button type="submit" class="btn btn-submit btn-success borderZero"><i class="fa fa-save"></i> Save Changes</button>
				<button type="button" class="btn btn-danger borderZero" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="modal fade" id="modal-amortization" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content borderZero">
		    <div id="load-amortization" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'admin/loans/amortize', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_amortization', 'files' => true)) !!}
			<div class="modal-header modal-warning">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title amortization-modal-title"><i class="fa fa-plus"></i> Add Amortization</h4>
			</div>
			<div class="modal-body">
				<div id="amortization-notice"></div>
                <div class="form-group">
                    <label for="amortization-title" class="col-sm-2 col-xs-5 font-color">Title</label>
                <div class="col-sm-10 col-xs-7">
                  <input type="text" name="title" class="borderZero form-control borderzero" id="amortization-title" maxlength="30">
                </div>
                </div>
                <div class="form-group">
                    <label for="amortization-loan_amount" class="col-sm-2 col-xs-5 font-color">Loan Amount</label>
                <div class="col-sm-10 col-xs-7">
                  <input type="text" name="loan_amount" class="borderZero form-control borderzero" id="amortization-loan_amount" maxlength="30">
                </div>
                </div>
                <div class="form-group">
                    <label for="amortization-interest_rate" class="col-sm-2 col-xs-5 font-color">Interest Rate</label>
                <div class="col-sm-10 col-xs-7">
                  <input type="text" name="interest_rate" class="borderZero form-control borderzero" id="amortization-interest_rate" maxlength="30">
                </div>
                </div>
                <div class="form-group">
                    <label for="amortization-term_of_loan" class="col-sm-2 col-xs-5 font-color">Term of Loan</label>
                <div class="col-sm-10 col-xs-7">
                  <input type="text" name="term_of_loan" class="borderZero form-control borderzero" id="amortization-term_of_loan" maxlength="30">
                </div>
                </div>
                <div class="form-group">
                    <label for="loan-date_acc" class="col-sm-2 col-xs-5 font-color">First Accrual Date</label>
                <div class="col-sm-10 col-xs-7">      
                  <div class="input-group date" id="date_acc-picker">
                    {!! Form::text('date_acc', null, ['class' => 'form-control borderZero', 'id' => 'amortization-date_acc']) !!}
                    <span class="borderZero input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                  </div>
                </div>
              </div>
              <div id="preview-notice"></div>
              <div id="amortization-preview">

              </div>
				</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="amortization-id" value="">
				<input type="hidden" name="loan_id" id="amortization-loan_id" value="">
				<button type="button" class="btn btn-submit btn-info btn-preview borderZero"><i class="fa fa-eye"></i> Preview</button>
				<button type="submit" class="btn btn-submit btn-success borderZero"><i class="fa fa-save"></i> Save Changes</button>
				<button type="button" class="btn btn-danger borderZero" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>


<div class="modal fade" id="modal-view-amortization" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content ajax-submit borderZero">
		    <div id="load-view-amortization" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			<form>
			<div class="modal-header modal-warning">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="view-amortization-title"><i class="fa fa-eye"></i> View</h4>
			</div>
			<div class="modal-body">
				<div id="view-amortization-preview"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger borderZero" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
			</div>
			</form>
		</div>
	</div>
</div>