<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>WINSTAR</title>

    <!-- Bootstrap -->
    {!! HTML::style('/css/bootstrap.min.css') !!}

    <!-- Font Awesome -->
    {!! HTML::style('/plugins/font-awesome/css/font-awesome.min.css') !!}

    <!-- CSS Plugins -->
    {!! HTML::style('/css/plugins/notification.css') !!}
    {!! HTML::style('/css/plugins/toastr.min.css') !!}
    {!! HTML::style('/css/plugins/fileinput.min.css') !!}
    {!! HTML::style('/css/plugins/jquery.dataTables.min.css') !!}
    {!! HTML::style('/css/plugins/select2-bootstrap.css') !!}
    {!! HTML::style('/css/plugins/select2.css') !!}
    {!! HTML::style('/plugins/components/datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}

    <!-- Main CSS -->
    {!! HTML::style('/css/style.css') !!}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

<!-- Navigation Bar -->
<nav id="navbar-main" class="navbar navbar-default navbar-fixed-top opaque" style="background-color: #1d1d1d !important;">
    <div class="container">
<div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
      <a href="{{ url('/') }}" rel="home">
            <img id="main-logo" class="animated fadeIn img-responsive opaque-logo en-logo" src="{{ url('images/logo') . '/winstar-logo.png'}}">
            </a>
        </div>
        <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
            <li><a class="smoothscroll" href="{{ url('/') }}">HOME</a></li>
            <li><a class="smoothscroll" href="{{ url('/#about-us') }}">ABOUT US</a></li>
            <li><a class="smoothscroll" href="{{ url('/#online-application') }}">ONLINE APPLICATION</a></li>
            <li><a class="smoothscroll" href="{{ url('/#services') }}">SERVICES</a></li>
            <li><a class="smoothscroll" href="{{ url('news') }}">NEWS</a></li>
            <li><a class="smoothscroll active" href="{{ url('/#contact-info') }}">CONTACT US</a></li>
            </ul>
        </div>
    </div>
</nav>


<!-- Jumbotron Section-->

 <!-- About Us Section -->
<section id="contact-info" class="active">
      <div id="top-nav" class="hidden-xs">
        <div class="container">
            <p id="sub-title">WINSTAR ANGELES CITY LOANS AND CREDIT</p>
            <p class="hidden-xs" id="sub-title-right">{{ $contact->phone }}</p>
        </div>
    </div>
    <div class="container">
    <div class="col-lg-12">
      <br>
      <br>
      <br>
      <br>
  <?php 
      echo($contact->body)
    ?>

   </div>
<!--        <div id="jumbotron-footer">
    </div> -->
  </div>
</section>

    <!-- Application Form -->
    <div class="modal fade" id="modal-application" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content borderZero">
                <div id="load-application" class="loading-pane hide">
                    <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                </div>
                {!! Form::open(array('url' => 'apply', 'role' => 'form', 'class' => 'form-horizontal')) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="application-title">APPLICATION FORM</h4>
                </div>
                <div id="application-us" class="modal-body">
                  <div id="application-notice"></div>
                  <div class="form-group">
                    <label class="col-sm-2">Name</label>
                    <div class="col-sm-10">
                      <div class="input-application">{!! Form::text('name', null, ['class' => 'form-control borderZero', 'id' => 'application-name']) !!}</div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2">Email</label>
                    <div class="col-sm-10">          
                        <div class="input-application">{!! Form::text('email', null, ['class' => 'form-control borderZero', 'id' => 'application-email']) !!}</div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2">Phone Number</label>
                    <div class="col-sm-10">          
                        <div class="input-application">{!! Form::text('phone', null, ['class' => 'form-control borderZero', 'id' => 'application-phone']) !!}</div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2">Type of Loan</label>
                    <div class="col-sm-10">
                      <div class="row">
                      <div class="col-lg-4">
                          <label><input name="commercial_loan" type="checkbox" id="application-commercial_loan"> Commercial Loan</label>
                      </div>
                      <div class="col-lg-4">
                          <label><input name="consumer_loan" type="checkbox" id="application-consumer_loan" > Consumer Loan</label>
                      </div>
                      <div class="col-lg-4">
                          <label><input name="personal_loan" type="checkbox" id="application-personal_loan"> Personal Loan</label>
                      </div>
                      <div class="col-lg-4">
                          <label><input name="salary_loan" type="checkbox" id="application-salary_loan"> Salary Loan</label>
                      </div>
                      <div class="col-lg-4">
                          <label><input name="car_loan" type="checkbox" id="application-car_loan"> Car Loan</label>
                      </div>
                      <div class="col-lg-4">
                          <label><input name="others_loan" type="checkbox" id="application-others"> Others</label><br>
                          <input type="text" name="others" class="form-control borderZero">
                      </div>
                      </div>        
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2">Date of Birth</label>
                    <div class="col-sm-10">          
                        <div class="input-group date" id="dob-picker">
                        {!! Form::text('dob', null, ['class' => 'form-control borderZero', 'id' => 'row-dob']) !!}
                        <span class="borderZero input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                      </div>
                  </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-2">Address</label>
                      <div class="col-sm-10">          
                          <div class="input-application">{!! Form::text('address', null, ['class' => 'form-control borderZero', 'id' => 'application-address']) !!}</div>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2">Preferred Schedule</label>
                    <div class="col-sm-10">          
                      <div class="input-group date" id="pref_sched-picker">
                        {!! Form::text('pref_sched', null, ['class' => 'form-control borderZero', 'id' => 'row-dob']) !!}
                        <span class="borderZero input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-submit btn-success borderZero"><i class="fa fa-fw fa-laptop"></i> Apply</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <!-- Inquiry Form -->
    <div class="modal fade" id="modal-inquire" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-inquire" class="loading-pane hide">
                    <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                </div>
                {!! Form::open(array('url' => 'inquire', 'role' => 'form', 'class' => 'form-horizontal')) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="inquire-title">INQUIRY FORM</h4>
                </div>
                <div id="inquire-us" class="modal-body">
                   <div id="inquire-notice"></div>
                <div class="form-group">
                    <label class="col-sm-2">Name</label>
                    <div class="col-sm-10">
                      <div class="input-inquire">{!! Form::text('name', null, ['class' => 'form-control borderZero', 'id' => 'inquire-name', 'required']) !!}</div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2">Email</label>
                    <div class="col-sm-10">          
                        <div class="input-inquire">{!! Form::text('email', null, ['class' => 'form-control borderZero', 'id' => 'inquire-email', 'required']) !!}</div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2">Phone Number</label>
                    <div class="col-sm-10">          
                        <div class="input-inquire">{!! Form::text('phone', null, ['class' => 'form-control borderZero', 'id' => 'inquire-phone', 'required']) !!}</div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2">Message</label>
                    <div class="col-sm-10">          
                        <div class="input-inquire">{!! Form::textarea('body', null, ['class' => 'form-control borderZero', 'id' => 'inquire-body', 'rows' => '5']) !!}</div>
                    </div>
                  </div>   
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-submit btn-success borderZero"><i class="fa fa-fw fa-laptop"></i> INQUIRE</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

</section>




<!-- Online Application Section -->
<section id="online-application" class="active">
    <div class="container">
        <h2>NEED TO APPLY NOW? CHECK OUT OUR ONLINE APPLICATION</h2>
        <p>YOU CAN Now APPLY TO us online with the our our hassle free system.</p>
    <div class="col-md-12 padding-none">
        <a class="btn btn-application btn-lg">APPLY NOW <i class="fa fa-fw fa-laptop"></i></a>
        <a class="btn btn-inquire btn-lg">INQUIRE <i class="fa fa-fw fa-laptop"></i></a>
    </div>
</section>



<!-- Clients Section -->
<section id="clients" class="active">
    <div class="container">
      <h2>Winstar Loans & Credit Corp. CLIENTS</h2>
      <marquee>
      @foreach ($clients as $row)
        <a target="_blank" title="{{ $row->title }}" href="{{ $row->url }}"><img style="width: 180px; padding-right: 20px;" src="{{ url('uploads/clients') . '/' . $row->photo}}"></a>
      @endforeach
      </marquee>
    </div>
</section>

<!-- Contact Information Section -->
<!-- <section id="contact-info" class="active">
    <div class="container">
    <center><h2>OUR MAIN OFFICE LOCATION CONTACT INFORMATION</h2></center>
    <div class="col-md-6">
      <h2>MAP GUIDE</h2>
        <iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d962.7449065886997!2d120.5910605628066!3d15.159463549953323!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x3396f273d389910f%3A0xee24cf0cae1778c6!2sOceana+Gas+Station%2C+MacArthur+Hwy%2C+Angeles%2C+Pampanga!3m2!1d15.159361299999999!2d120.59161309999999!5e0!3m2!1sen!2sph!4v1452077756199" width="100%" height="155" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="col-md-6">
      <h2>CONTACT DETAILS</h2>
      @if ($contact)
        <p><strong>Address:</strong> {{ $contact->address }}</p>
        <p><strong>Phone No:</strong> {{ $contact->phone }}</p>
        <p><strong>Email:</strong> {{ $contact->email }}</p>
        <p><strong>Office Hours:</strong> {{ $contact->office_hours }}</p>
        <h2>SOCIAL MEDIA</h2> <a target="_blank" href="{{ $contact->facebook_link }}"><i class="fa fa-facebook-square fa-2x"></i></a> &nbsp;<a target="_blank" href="{{ $contact->twitter_link }}"><i class="fa fa-twitter fa-2x"></i></a> 
      @else
        <p><strong>Address:</strong> N/A</p>
        <p><strong>Phone No:</strong> N/A</p>
        <p><strong>Email:</strong> N/A</p>
        <p><strong>Office Hours:</strong> N/A</p>
        <h2>SOCIAL MEDIA</h2> <i class="fa fa-facebook-square fa-2x"></i> &nbsp;<i class="fa fa-twitter fa-2x"></i>
      @endif
    </div>
    </div>
</section> -->

</div>

<!-- Footer -->
<footer>
  <div class="container">
    <p class="footer-copyright">© Copyright 2016  Winstar Loans & Credit Corp.</p>
    <p class="xerolabs">Designed and Developed by Xerolabs Web Solutions</p>
  </div>
</footer>
    <!-- jQuery -->
    {!! HTML::script('/js/jquery.js') !!}

    <!-- Bootstrap -->
    {!! HTML::script('/js/bootstrap.min.js') !!}

    <!-- Main Scripts -->
    {!! HTML::script('/js/script.js') !!}
    
    <!-- JS Plugins -->
    {!! HTML::script('/js/plugins/toastr.min.js') !!}
    {!! HTML::script('/js/plugins/raphael-min.js') !!}
    {!! HTML::script('/js/plugins/morris.js') !!}
    {!! HTML::script('/js/plugins/modernizr.js') !!}
    {!! HTML::script('/js/plugins/jquery.dataTables.js') !!}
    {!! HTML::script('/js/plugins/fileinput.min.js') !!}
    {!! HTML::script('/js/plugins/select2.min.js') !!}
    {!! HTML::script('/plugins/components/moment/min/moment.min.js') !!}
    {!! HTML::script('/plugins/components/datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
    {!! HTML::script('/js/plugins/toastr.min.js') !!}
    {!! HTML::script('/js/plugins/tweenmax.min.js') !!}
    {!! HTML::script('/js/plugins/scrolltoplugin.min.js') !!}

    {!! HTML::script('/js/main.js') !!}

    <script type="text/javascript">
        
    $_token = '{{ csrf_token() }}';

    $(function () {
        $('#dob-picker').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
    });

    $(function () {
        $('#pref_sched-picker').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY h:MM A'
        });
    });

    $(".btn-application").click(function() {
        $('#modal-application').find('form').trigger('reset');
        $('#modal-application').modal('show');
    });


    // reset form notice
    $("#modal-application").on('show.bs.modal', function(e) {
        $("#application-notice").html("");
    });

    // submit form
    $("#modal-application").find("form").submit(function(e) {
   
        var form = $("#modal-application").find("form");
        var loading = $("#load-application");
        
        // stop form from submitting
        e.preventDefault();

        loading.removeClass('hide');
        console.log(form.serialize());
        
        // push form data
        $.ajax({
            type: "post",
            url: form.attr('action'),
            data: form.serialize(),
            dataType: "json",
            success: function(response) {
                if(response.error) {
                    var errors = '<ul>';

                    $.each(response.error, function(index, value) {
                        errors += '<li>' + value + '</li>';
                    });

                    errors += '</ul>';

                    status('Please correct the following:', errors, 'alert-danger', '#application-notice');
                } else {
                    $("#modal-application").modal('hide');
                    status(response.title, response.body, 'alert-success');
                }

                loading.addClass('hide');
            }
        });
    });

    $(".btn-inquire").click(function() {
        $('#modal-inquire').find('form').trigger('reset');
        $('#modal-inquire').modal('show');
    });

    // reset form notice
    $("#modal-inquire").on('show.bs.modal', function(e) {
        $("#inquire-notice").html("");
    });

    // submit form
    $("#modal-inquire").find("form").submit(function(e) {
   
        var form = $("#modal-inquire").find("form");
        var loading = $("#load-inquire");
        
        // stop form from submitting
        e.preventDefault();

        loading.removeClass('hide');
        console.log(form.serialize());
        
        // push form data
        $.ajax({
            type: "post",
            url: form.attr('action'),
            data: form.serialize(),
            dataType: "json",
            success: function(response) {
                if(response.error) {
                    var errors = '<ul>';

                    $.each(response.error, function(index, value) {
                        errors += '<li>' + value + '</li>';
                    });

                    errors += '</ul>';

                    status('Please correct the following:', errors, 'alert-danger', '#inquire-notice');
                } else {
                    $("#modal-inquire").modal('hide');
                    status(response.title, response.body, 'alert-success');
                }

                loading.addClass('hide');
            }
        });
    });

    </script>

  </body>
</html>