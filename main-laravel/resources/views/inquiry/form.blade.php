<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content ajax-submit borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'admin/inquiry/delete', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}
			<div class="modal-header modal-delete">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-trash"></i> Delete Inquiry</h4>
			</div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Do you want to delete this inquiry?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="row-id" value="">
				<button type="submit" class="btn btn-submit btn-danger borderZero"><i class="fa fa-trash"></i> Yes</button>
				<button type="button" class="btn btn-info borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>


<div class="modal fade" id="modal-replied" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-replied" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'admin/inquiry/replied', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_replied', 'files' => true)) !!}
			<div class="modal-header modal-warning">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-reply"></i> Inquiry Status</h4>
			</div>
			<div class="modal-body">
				<div id="replied-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Update status to <strong>Replied</strong>?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="replied-id" value="">
				<button type="submit" class="btn btn-submit btn-danger borderZero"><i class="fa fa-check"></i> Yes</button>
				<button type="button" class="btn btn-info borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>



<div class="modal fade" id="modal-view" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content borderZero">
		    <div id="load-view" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			<form>
			<div class="modal-header modal-warning">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="view-title">View</h4>
			</div>
			<div class="modal-body">
				<div id="view-notice"></div>
        <div class="form-group">
         	<label for="view-tracking_no" class="col-md-3 font-color">Tracking No.</label>
        <div class="col-md-9">
          <p id="view-tracking_no"></p>
        </div>
        </div>
        <div class="form-group">
         	<label for="view-name" class="col-md-3 font-color">Name</label>
        <div class="col-md-9">
          <p id="view-name"></p>
        </div>
        </div>
        <div class="form-group">
         	<label for="view-email" class="col-md-3 font-color">Email</label>
        <div class="col-md-9">
          <p id="view-email"></p>
        </div>
        </div>
        <div class="form-group">
         	<label for="view-phone" class="col-md-3 font-color">Phone</label>
        <div class="col-md-9">
          <p id="view-phone"></p>
        </div>
        </div>
        <div class="form-group">
         	<label for="view-body" class="col-md-3 font-color">Message</label>
        <div class="col-md-9">
          <p id="view-body"></p>
        </div>
        </div>
        <div class="clearfix"></div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="view-id" value="">
				<button type="button" class="btn btn-info borderZero" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
			</div>
			</form>
		</div>
	</div>
</div>
