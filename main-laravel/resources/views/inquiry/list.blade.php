@extends('layouts.backend')

@section('scripts')
<script type="text/javascript">

  $_token = '{{ csrf_token() }}';

	// refresh the list
	function refresh() {
      $("#row-page").val(1);
      $("#row-order").val('desc');
      $("#row-sort").val('updated_at');
      $("#row-search").val('');
      $(".th-sort").find('i').removeAttr('class');

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();

			var loading = $(".loading-pane");
			var table = $("#rows");
			var url = "{{ url('/') }}";
			loading.removeClass('hide');

      $.post("{{ url('admin/inquiry/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
				
        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }
				// clear table
				table.html("");
				// populate table
				$.each(response.rows.data, function(index, row) {

						table.append(
								'<tr data-id="' + row.id + '">' +
								'<td>' + row.name + '</td>' +
								'<td>' + row.email + '</td>' +
								'<td>' + row.phone + '</td>' +
								'<td>' + moment(row.updated_at).format('LLL') + '</td>' +
								'<td>' + row.status + '</td>' +
								'<td class="text-right">' +
								(row.status == "Replied" ? 
									'<a type="but ton" title="View" class="borderZero btn-xs btn btn-info btn-view"><i class="fa fa-eye"></i></a>' +
									'&nbsp;<a type="button" title="Cancel" class="borderZero btn-xs btn btn-danger btn-delete"><i class="fa fa-trash"></i></a>' : 
									'<a type="but ton" title="View" class="borderZero btn-xs btn btn-info btn-view"><i class="fa fa-eye"></i> </a>' +
									'&nbsp;<a type="button" title="Replied" class="borderZero btn-xs btn btn-warning btn-replied"><i class="fa fa-reply"></i></a>' +
									'&nbsp;<a type="button" title="Cancel" class="borderZero btn-xs btn btn-danger btn-delete"><i class="fa fa-trash"></i></a>') 
								+ '</td>' +
								'</tr>'
						);
				});

			// update links
			$("#row-pages").html(response.pages);

			$(".sort-refresh").find('i').addClass('fa fa-sort-up');
      loading.addClass("hide");

      }, 'json');
	}
	
    function search() {
      var loading = $(".loading-pane");
      var table = $("#rows");

      $("#row-page").val(1);
      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
			var url = "{{ url('/') }}";

      loading.removeClass("hide");

      $.post("{{ url('admin/inquiry/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
				
        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }
				// clear table
				table.html("");
				// populate table
				$.each(response.rows.data, function(index, row) {

					table.append(
								'<tr data-id="' + row.id + '">' +
								'<td>' + row.name + '</td>' +
								'<td>' + row.email + '</td>' +
								'<td>' + row.phone + '</td>' +
								'<td>' + moment(row.updated_at).format('LLL') + '</td>' +
								'<td>' + row.status + '</td>' +
								'<td class="text-right">' +
								(row.status == "Replied" ? 
									'<a type="but ton" title="View" class="borderZero btn-xs btn btn-info btn-view"><i class="fa fa-eye"></i></a>' +
									'&nbsp;<a type="button" title="Cancel" class="borderZero btn-xs btn btn-danger btn-delete"><i class="fa fa-trash"></i></a>' : 
									'<a type="but ton" title="View" class="borderZero btn-xs btn btn-info btn-view"><i class="fa fa-eye"></i> </a>' +
									'&nbsp;<a type="button" title="Replied" class="borderZero btn-xs btn btn-warning btn-replied"><i class="fa fa-reply"></i></a>' +
									'&nbsp;<a type="button" title="Cancel" class="borderZero btn-xs btn btn-danger btn-delete"><i class="fa fa-trash"></i></a>') 
								+ '</td>' +
								'</tr>'
						);
				});

			// update links
			$("#row-pages").html(response.pages);

			$(".sort-refresh").find('i').addClass('fa fa-sort-up');
      loading.addClass("hide");

      }, 'json');
    } 

	$(document).ready(function() {

		$("#page-content-wrapper").on("click", ".btn-replied", function() {
			$("#modal-replied").find('form').trigger('reset');
			$("#replied-id").val($(this).parent().parent().data('id'));
			$("#modal-replied").modal('show');
		});

		$("#page-content-wrapper").on("click", ".btn-view", function() {
			// $("#modal-view").find('form').trigger('reset');
			// $("#view-id").val($(this).parent().parent().data('id'));
			// $("#modal-view").modal('show');

			var id = $(this).parent().parent().data('id');
      var btn = $(this);
      var url = "{{ url('/') }}";
      $("#view-header").removeAttr('class').attr('class', 'modal-header modal-default');
      $("#modal-view").find('form').trigger("reset");
      $("#view-id").val("");

      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");
            
      $.post("{{ url('admin/inquiry/view') }}", { id: id, _token: $_token }, function(response) {
      	
        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }
          if(!response.error) {
          // set form title
              $(".view-title").html('<i class="fa fa-eye"></i> Viewing <strong>' + response.name + '</strong>');
              console.log(response);
              // output form data
              $.each(response, function(index, value) {
                  var field = $("#view-" + index);

                  if(field.length > 0) {
                    field.html(value);
                  }
              });
              // show form
              $("#modal-view").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }

          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');

		});

		// delete news
		$("#page-content-wrapper").on("click", ".btn-delete", function() {
			$("#row-id").val("");
			$("#row-id").val($(this).parent().parent().data('id'));
			$("#modal-form").modal("show");
		});

		$(".tab-pending").click(function() {

 			$("#row-page").val(1);
      $("#row-order").val('desc');
      $("#row-sort").val('updated_at');
      $("#row-search").val('');
      $("#row-filter_status").val('Pending');
      $(".th-sort").find('i').removeAttr('class');

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();

			var loading = $(".loading-pane");
			var table = $("#rows");
			var url = "{{ url('/') }}";
			loading.removeClass('hide');

      $.post("{{ url('admin/inquiry/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
				
        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }
				// clear table
				table.html("");
				// populate table
				$.each(response.rows.data, function(index, row) {

						table.append(
								'<tr data-id="' + row.id + '">' +
								'<td>' + row.name + '</td>' +
								'<td>' + row.email + '</td>' +
								'<td>' + row.phone + '</td>' +
								'<td>' + moment(row.updated_at).format('LLL') + '</td>' +
								'<td>' + row.status + '</td>' +
								'<td class="text-right">' +
								(row.status == "Replied" ? 
									'<a type="but ton" title="View" class="borderZero btn-xs btn btn-info btn-view"><i class="fa fa-eye"></i></a>' +
									'&nbsp;<a type="button" title="Cancel" class="borderZero btn-xs btn btn-danger btn-delete"><i class="fa fa-trash"></i></a>' : 
									'<a type="but ton" title="View" class="borderZero btn-xs btn btn-info btn-view"><i class="fa fa-eye"></i> </a>' +
									'&nbsp;<a type="button" title="Replied" class="borderZero btn-xs btn btn-warning btn-replied"><i class="fa fa-reply"></i></a>' +
									'&nbsp;<a type="button" title="Cancel" class="borderZero btn-xs btn btn-danger btn-delete"><i class="fa fa-trash"></i></a>') 
								+ '</td>' +
								'</tr>'
						);
				});

			// update links
			$("#row-pages").html(response.pages);

			$(".sort-refresh").find('i').addClass('fa fa-sort-up');
      loading.addClass("hide");

      }, 'json');
		});

		$(".tab-replied").click(function() {

 			$("#row-page").val(1);
      $("#row-order").val('desc');
      $("#row-sort").val('updated_at');
      $("#row-search").val('');
      $("#row-filter_status").val('Replied');
      $(".th-sort").find('i').removeAttr('class');

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();

			var loading = $(".loading-pane");
			var table = $("#rows");
			var url = "{{ url('/') }}";
			loading.removeClass('hide');

      $.post("{{ url('admin/inquiry/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
				
        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }
				// clear table
				table.html("");
				// populate table
				$.each(response.rows.data, function(index, row) {

						table.append(
								'<tr data-id="' + row.id + '">' +
								'<td>' + row.name + '</td>' +
								'<td>' + row.email + '</td>' +
								'<td>' + row.phone + '</td>' +
								'<td>' + moment(row.updated_at).format('LLL') + '</td>' +
								'<td>' + row.status + '</td>' +
								'<td class="text-right">' +
								(row.status == "Replied" ? 
									'<a type="but ton" title="View" class="borderZero btn-xs btn btn-info btn-view"><i class="fa fa-eye"></i></a>' +
									'&nbsp;<a type="button" title="Cancel" class="borderZero btn-xs btn btn-danger btn-delete"><i class="fa fa-trash"></i></a>' : 
									'<a type="but ton" title="View" class="borderZero btn-xs btn btn-info btn-view"><i class="fa fa-eye"></i> </a>' +
									'&nbsp;<a type="button" title="Replied" class="borderZero btn-xs btn btn-warning btn-replied"><i class="fa fa-reply"></i></a>' +
									'&nbsp;<a type="button" title="Cancel" class="borderZero btn-xs btn btn-danger btn-delete"><i class="fa fa-trash"></i></a>') 
								+ '</td>' +
								'</tr>'
						);
				});

			// update links
			$("#row-pages").html(response.pages);

			$(".sort-refresh").find('i').addClass('fa fa-sort-up');
      loading.addClass("hide");

      }, 'json');
		});

	});

    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {

      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

        $('.loading-pane').removeClass('hide');
	      $per = $("#row-per").val();
	      $page = $("#row-page").val();
	      $order = $("#row-order").val();
	      $sort = $("#row-sort").val();
	      $search = $("#row-search").val();
	      $status = $("#row-filter_status").val();
        $per = $("#row-per").val();
				var table = $("#rows");
				var url = "{{ url('/') }}";

        var loading = $(".loading-pane");
        loading.removeClass("hide");

	      $.post("{{ url('admin/inquiry/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
					
        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }
					// clear table
					table.html("");
					// populate table
					$.each(response.rows.data, function(index, row) {

						table.append(
								'<tr data-id="' + row.id + '">' +
								'<td>' + row.name + '</td>' +
								'<td>' + row.email + '</td>' +
								'<td>' + row.phone + '</td>' +
								'<td>' + moment(row.updated_at).format('LLL') + '</td>' +
								'<td>' + row.status + '</td>' +
								'<td class="text-right">' +
								(row.status == "Replied" ? 
									'<a type="but ton" title="View" class="borderZero btn-xs btn btn-info btn-view"><i class="fa fa-eye"></i></a>' +
									'&nbsp;<a type="button" title="Cancel" class="borderZero btn-xs btn btn-danger btn-delete"><i class="fa fa-trash"></i></a>' : 
									'<a type="but ton" title="View" class="borderZero btn-xs btn btn-info btn-view"><i class="fa fa-eye"></i> </a>' +
									'&nbsp;<a type="button" title="Replied" class="borderZero btn-xs btn btn-warning btn-replied"><i class="fa fa-reply"></i></a>' +
									'&nbsp;<a type="button" title="Cancel" class="borderZero btn-xs btn btn-danger btn-delete"><i class="fa fa-trash"></i></a>') 
								+ '</td>' +
								'</tr>'
						);
					});

				// update links
				$("#row-pages").html(response.pages);

	      loading.addClass("hide");

	      }, 'json');
      
    });

    $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $("#row-page").val($(this).html());
	      $per = $("#row-per").val();
	      $page = $("#row-page").val();
	      $order = $("#row-order").val();
	      $sort = $("#row-sort").val();
	      $search = $("#row-search").val();
	      $status = $("#row-filter_status").val();

        var loading = $(".loading-pane");
        var table = $("#rows");
				var url = "{{ url('/') }}";

        loading.removeClass("hide");

	      $.post("{{ url('admin/inquiry/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
					
        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }
					// clear table
					table.html("");
					// populate table
					$.each(response.rows.data, function(index, row) {

							table.append(
								'<tr data-id="' + row.id + '">' +
								'<td>' + row.name + '</td>' +
								'<td>' + row.email + '</td>' +
								'<td>' + row.phone + '</td>' +
								'<td>' + moment(row.updated_at).format('LLL') + '</td>' +
								'<td>' + row.status + '</td>' +
								'<td class="text-right">' +
								(row.status == "Replied" ? 
									'<a type="but ton" title="View" class="borderZero btn-xs btn btn-info btn-view"><i class="fa fa-eye"></i></a>' +
									'&nbsp;<a type="button" title="Cancel" class="borderZero btn-xs btn btn-danger btn-delete"><i class="fa fa-trash"></i></a>' : 
									'<a type="but ton" title="View" class="borderZero btn-xs btn btn-info btn-view"><i class="fa fa-eye"></i> </a>' +
									'&nbsp;<a type="button" title="Replied" class="borderZero btn-xs btn btn-warning btn-replied"><i class="fa fa-reply"></i></a>' +
									'&nbsp;<a type="button" title="Cancel" class="borderZero btn-xs btn btn-danger btn-delete"><i class="fa fa-trash"></i></a>') 
								+ '</td>' +
								'</tr>'
						);
					});

				// update links
				$("#row-pages").html(response.pages);

	      loading.addClass("hide");

	      }, 'json');
      }
    });

</script>
@stop

@section('content')
<div class="page-header">
  <h1>Winstar</h1>
  <ol class="breadcrumb">
    <li><a href="#">Admin</a></li>
    <li><a href="#">Inquiries</a></li>
  </ol>
</div>
<div id=""class="container-fluid col-xs-12">
    <div class="loading-pane hide">
      <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
    </div>
  <!-- Nav tabs -->
  <ul id="myTabs" class="myTabBg nav nav-tabs nav-justified" role="tablist">
    <li role="presentation" class="myTabsHeader active"><a class="tabLink tab-pending" aria-controls="home" role="tab" data-toggle="tab">Pending</a></li>
    <li role="presentation" class="myTabsHeader"><a class="tabLink tab-replied" aria-controls="profile" role="tab" data-toggle="tab">Replied</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="myTabs tab-content">
    <div role="tabpanel" class="tab-pane active" id="pending">
  	<input type="text" class="form-control borderZero" placeholder="Search for..." onkeyup="search()" id="row-search">
  	<br>
    <div class="table-responsive">
			<table id="lists" class="table table-striped table-pagination">
				<thead class="myTableThead">
					<tr>
						<th class="th-sort myTableHeader" data-sort="name"><i></i> Name</th>
						<th class="th-sort myTableHeader" data-sort="email"><i></i> Email</th>
						<th class="th-sort myTableHeader" data-sort="phone"><i></i> Phone</th>
						<th class="th-sort myTableHeader sort-refresh" data-sort="updated_at"><i class="fa fa-sort-up"></i> Date Updated</th>
						<th class="myTableHeader">Status</th>
						<th class="text-right myTableHeader"><button type="button" class="btn btn-success btn-sm btn-attr borderZero" onclick="refresh()"><span class="fa fa-refresh"></span></button></th>
					</tr>
				</thead>
				<tbody id="rows">
					@foreach ($rows as $row)
					<tr data-id="{{ $row->id }}">
						<td>{{ $row->name }}</td>
						<td>{{ $row->email }}</td>
						<td>{{ $row->phone }}</td>
						<td>{{ date("F j, Y g:i A", strtotime($row->updated_at)) }}</td>
						<td>{{ $row->status}}</td>
						<td class="text-right">
							<a type="button" title="View" class="borderZero btn-xs btn btn-info btn-view"><i class="fa fa-eye"></i> </a>
							<a type="button" title="Replied" class="borderZero btn-xs btn btn-warning btn-replied"><i class="fa fa-reply"></i> </a>
							<a type="button" title="Cancel" class="borderZero btn-xs btn btn-danger btn-delete"><i class="fa fa-trash"></i> </a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
  		</div>
	</div>
	<div class="panel-footer panelFooter">
	  	<div class="row">
	  		 <div id="row-pages" class="col-lg-6">{!! $pages !!}</div>
	  		<div class="col-lg-6">
	        <select class="form-control borderZero pull-right" onchange="search()" id="row-per" style="margin: 0; width:70px; border: 0px;">
	          <option value="10">10</option>
	          <option value="25">25</option>
	          <option value="50">50</option>
	          <option value="50">100</option>
	        </select>
	        <input type="hidden" id="row-filter_status" value="Pending">
            <input type="hidden" id="row-page" value="1">
            <input type="hidden" id="row-sort" value="">
            <input type="hidden" id="row-order" value="">
	  		</div>
	  	</div>
	  </div>
</div>

@include('inquiry.form')
</div>

@stop