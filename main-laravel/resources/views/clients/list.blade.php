@extends('layouts.backend')

@section('scripts')
<script type="text/javascript">

  $_token = '{{ csrf_token() }}';

	// refresh the list
	function refresh() {
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $("#row-filter_status").val('');
      $(".th-sort").find('i').removeAttr('class');

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();

			var loading = $(".loading-pane");
			var table = $("#rows");
			var url = "{{ url('/') }}";
			loading.removeClass('hide');

      $.post("{{ url('admin/clients/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, per: $per, _token: $_token }, function(response) {
				
        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }

        // clear table
				table.html("");
				// populate table
				$.each(response.rows.data, function(index, row) {

						table.append(
								'<tr data-id="' + row.id + '">' +
                '<td><img src="' + url + '/uploads/thumbnails/clients/' + row.photo + '?' + new Date().getTime() + '"></td>' +
								'<td>' + row.title + '</td>' +
								'<td>' + moment(row.created_at).format('LLL') + '</td>' +
								'<td class="text-right">' + 
									'<a target="_blank" href="' + row.url + '" class="btn btn-xs borderZero btn-info btn-view" title="View News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-eye"></i></a>' +
									'&nbsp;<a class="btn btn-xs borderZero btn-primary btn-edit" title="Edit News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
									'&nbsp;<a data-id="' + row.id + '" class="btn btn-xs borderZero btn-danger btn-delete" title="Delete News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-trash"></i></a>' +
								'</td>' +
								'</tr>'
						);
				});

			// update links
			$("#row-pages").html(response.pages);

			$(".sort-refresh").find('i').addClass('fa fa-sort-up');
      loading.addClass("hide");

      }, 'json');
	}
	
    function search() {
      var loading = $(".loading-pane");
      var table = $("#rows");

      $("#row-page").val(1);
      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
    	$search = $("#row-search").val();
			var url = "{{ url('/') }}";

      loading.removeClass("hide");

      $.post("{{ url('admin/clients/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, per: $per, _token: $_token }, function(response) {
        
        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }

        // clear table
        table.html("");
        // populate table
        $.each(response.rows.data, function(index, row) {

            table.append(
                '<tr data-id="' + row.id + '">' +
                '<td><img src="' + url + '/uploads/thumbnails/clients/' + row.photo + '?' + new Date().getTime() + '"></td>' +
                '<td>' + row.title + '</td>' +
                '<td>' + moment(row.created_at).format('LLL') + '</td>' +
                '<td class="text-right">' + 
                  '<a target="_blank" href="' + row.url + '" class="btn btn-xs borderZero btn-info btn-view" title="View News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-eye"></i></a>' +
                  '&nbsp;<a class="btn btn-xs borderZero btn-primary btn-edit" title="Edit News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
                  '&nbsp;<a data-id="' + row.id + '" class="btn btn-xs borderZero btn-danger btn-delete" title="Delete News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-trash"></i></a>' +
                '</td>' +
                '</tr>'
            );
        });

      // update links
      $("#row-pages").html(response.pages);

      loading.addClass("hide");

      }, 'json');
    } 

	$(document).ready(function() {
		// refresh();

    $('.summernote').summernote({
      height: 300,
      tabsize: 1
    });

    // add client
    $("#page-content-wrapper").on("click", ".btn-add", function() {
      $("#client-header").removeAttr('class').attr('class', 'modal-header modal-warning');
      $(".client-title").html('<i class="fa fa-plus"></i> Add Client');
      $(".photo-upload").removeClass("hide");
      $(".photo-preview").addClass("hide");
      $(".btn-cancel").addClass("hide");
      $("#modal-client").find('form').trigger("reset");
      $("#client-id").val("");
      $("#modal-client").modal("show");
    });

    $(".btn-change").click(function() {
      $(".photo-upload").removeClass("hide");
      $(".photo-preview").addClass("hide");
      $(".btn-cancel").removeClass("hide");
    });

    $(".btn-cancel").click(function() {
      $(".photo-upload").addClass("hide");
      $(".photo-preview").removeClass("hide");
      $(".btn-cancel").addClass("hide");
    });

    // edit client
    $("#page-content-wrapper").on("click", ".btn-edit", function() {
      var id = $(this).parent().parent().data('id');
      var btn = $(this);
      var url = "{{ url('/') }}";
      $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
      $("#modal-client").find('form').trigger("reset");
      $("#client-id").val("");
      $(".photo-upload").addClass("hide");
      $(".photo-preview").removeClass("hide");
      $(".btn-cancel").addClass("hide");

      $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");
            
      $.post("{{ url('admin/clients/edit') }}", { id: id, _token: $_token }, function(response) {

          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }

          if(!response.error) {
          // set form title
              $(".client-title").html('<i class="fa fa-pencil"></i> Editing <strong>' + response.title + '</strong>');

              // output form data
              $.each(response, function(index, value) {
                  var field = $("#client-" + index);

                  if(field.length > 0) {
                    if (field.parent().hasClass('photo-file')) {
                        field.attr('src', url + '/uploads/clients/' + value + '?' + new Date().getTime());
                    } else {
                        field.val(value);
                    }
                  }
              });
              // show form
              $("#modal-client").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }

          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');
    });

		// delete news
		$("#page-content-wrapper").on("click", ".btn-delete", function() {
			$("#row-id").val("");
			$("#row-id").val($(this).data('id'));
			$("#modal-form").modal("show");
		});

	});

    $("#page-content-wrapper").on('click', '.table-pagination .th-sort', function (event) {

      if ($(this).find('i').hasClass('fa-sort-up')) {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('asc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-down');
      } else {
        $('.table-pagination th i').removeAttr('class');
        $order = $("#row-order").val('desc');
        $sort = $("#row-sort").val($(this).data('sort'));
        $(this).find('i').addClass('fa fa-sort-up');
      }

        $('.loading-pane').removeClass('hide');
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $status = $("#row-filter_status").val();
        $per = $("#row-per").val();
				var table = $("#rows");
				var url = "{{ url('/') }}";

        var loading = $(".loading-pane");
        loading.removeClass("hide");

        $.post("{{ url('admin/clients/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, per: $per, _token: $_token }, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }

          // clear table
          table.html("");
          // populate table
          $.each(response.rows.data, function(index, row) {

              table.append(
                  '<tr data-id="' + row.id + '">' +
                  '<td><img src="' + url + '/uploads/thumbnails/clients/' + row.photo + '?' + new Date().getTime() + '"></td>' +
                  '<td>' + row.title + '</td>' +
                  '<td>' + moment(row.created_at).format('LLL') + '</td>' +
                  '<td class="text-right">' + 
                    '<a target="_blank" href="' + row.url + '" class="btn btn-xs borderZero btn-info btn-view" title="View News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-eye"></i></a>' +
                    '&nbsp;<a class="btn btn-xs borderZero btn-primary btn-edit" title="Edit News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
                    '&nbsp;<a data-id="' + row.id + '" class="btn btn-xs borderZero btn-danger btn-delete" title="Delete News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-trash"></i></a>' +
                  '</td>' +
                  '</tr>'
              );
          });

        // update links
        $("#row-pages").html(response.pages);

        loading.addClass("hide");

        }, 'json');
      
    });

    $("#page-content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

        $("html, body").animate({ scrollTop: 0 }, "fast");
        $("#row-page").val($(this).html());
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");
				var url = "{{ url('/') }}";

        loading.removeClass("hide");

        $.post("{{ url('admin/clients/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, per: $per, _token: $_token }, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }

          // clear table
          table.html("");
          // populate table
          $.each(response.rows.data, function(index, row) {

              table.append(
                  '<tr data-id="' + row.id + '">' +
                  '<td><img src="' + url + '/uploads/thumbnails/clients/' + row.photo + '?' + new Date().getTime() + '"></td>' +
                  '<td>' + row.title + '</td>' +
                  '<td>' + moment(row.created_at).format('LLL') + '</td>' +
                  '<td class="text-right">' + 
                    '<a target="_blank" href="' + row.url + '" class="btn btn-xs borderZero btn-info btn-view" title="View News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-eye"></i></a>' +
                    '&nbsp;<a class="btn btn-xs borderZero btn-primary btn-edit" title="Edit News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
                    '&nbsp;<a data-id="' + row.id + '" class="btn btn-xs borderZero btn-danger btn-delete" title="Delete News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-trash"></i></a>' +
                  '</td>' +
                  '</tr>'
              );
          });

        // update links
        $("#row-pages").html(response.pages);

        loading.addClass("hide");

        }, 'json');
      }
    });
</script>
@stop

@section('content')
<div class="page-header">
  <h1>Winstar</h1>
  <ol class="breadcrumb">
    <li><a href="#">Admin</a></li>
    <li><a href="#">Clients</a></li>
  </ol>
</div>

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading panelHeader">
  	<div class="row">
  		<div class="col-xs-6 col-sm-6 col-lg-3">
  			<input type="text" class="form-control borderZero" placeholder="Search for..." onkeyup="search()" id="row-search">
  		</div>
  		<div class="col-lg-6 col-offset-lg-3 col-xs-6 col-sm-6 text-right pull-right">
  			<a class="btn btn-warning btn-md btn-attr btn-add borderZero"><i class="fa fa-plus"></i> Add</a>
  			<button type="button" class="btn btn-success btn-md btn-attr borderZero" onclick="refresh()"><i class="fa fa-refresh"></i></button> 
  		</div>
  	</div>
  </div>
  <div class="panel-body">
  	<div class="table-responsive">
        <div class="loading-pane hide">
          <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
        </div>
				<table id="services-lists" class="table table-striped table-pagination">
					<thead class="myTableThead">
						<tr>
              <th class="myTableHeader">Photo</th>
							<th class="th-sort myTableHeader" data-sort="title"><i></i> Title</th>
							<th class="th-sort myTableHeader sort-refresh" data-sort="created_at"><i class="fa fa-sort-up"></i> Date Created</th>
							<th class="text-right myTableHeader"><i id="load-list" class="fa fa-refresh fa-spin pull-right hide"></i></th>
						</tr>
					</thead>
					<tbody id="rows">
          @foreach ($rows as $row)
            <tr data-id="{{ $row->id }}">
              <td><img src="{{ url('/') . '/uploads/thumbnails/clients/' . $row->photo }}"></td>
              <td>{{ $row->title }}</td>
              <td>{{ date_format(new DateTime($row->created_at), 'F d, Y g:i A') }}</td>
              <td class="text-right">
                <a target="_blank" href="{{ $row->url }}" class="btn btn-xs borderZero btn-info btn-view" title="View News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-eye"></i></a>
                <a class="btn btn-xs borderZero btn-primary btn-edit" title="Edit News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>
                <a data-id="{{ $row->id }}"class="btn btn-xs borderZero btn-danger btn-delete" title="Delete News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-trash"></i></a>
              </td>
            </tr>
          @endforeach
					</tbody>
				</table>
			</div>
  </div>
  <div class="panel-footer panelFooter">
  	<div class="row">
  		 <div id="row-pages" class="col-lg-6">{!! $pages !!}</div>
  		<div class="col-lg-6">
        <select class="form-control borderZero pull-right" onchange="search()" id="row-per" style="margin: 0; width:70px; border: 0px;">
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="50">100</option>
        </select>
  		</div>
  	</div>
  </div>
</div>
		@if(Session::has("notice"))
  		<div id="alert" class="col-lg-3 col-lg-offset-9 borderZero alert alert-dismissable alert-{{ Session::get('notice.type') }}">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <i class="fa fa-fw fa-exclamation-triangle"></i> {{ Session::get('notice.msg') }}
  		</div>
  	@endif
				<div class="content-wrapper">
        <div class="text-center">
          <input type="hidden" id="row-page" value="1">
          <input type="hidden" id="row-sort" value="">
          <input type="hidden" id="row-order" value="">
        </div>       
        </div>
      </div>

@include('clients.form')

@stop