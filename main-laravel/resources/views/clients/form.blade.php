<div class="modal fade" id="modal-client" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content borderZero">
		    <div id="load-client" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'admin/clients/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_client', 'files' => true)) !!}
			<div id="client-header" class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="client-title">Add Client</h4>
			</div>
			<div class="modal-body">
				<div id="client-notice"></div>
                <div class="form-group photo-preview hide">
					<label for="client-photo" class="col-sm-2 col-xs-5 font-color">Photo</label>
					<div class="col-sm-10 col-xs-7 photo-file">
						<img id="client-photo">
						<a class="btn btn-primary btn-sm btn-change borderZero pull-right">Change Photo</a>
					</div>
                </div>
                <div class="form-group photo-upload">
					<label for="client-photo" class="col-sm-2 col-xs-5 font-color">Photo</label>
					<div class="col-sm-10 col-xs-7">
						<input name="photo" id='client-photo_upload' type='file' class='file borderZero file_photo' data-show-upload='false' placeholder='Upload a photo...'>
						<a class="btn btn-primary btn-sm btn-cancel hide borderZero pull-right" style="margin-top: 5px;">Cancel</a>
					</div>
                </div>
                <div class="form-group">
                    <label for="client-title" class="col-sm-2 col-xs-5 font-color">Title</label>
                <div class="col-sm-10 col-xs-7">
                  <input type="text" name="title" class="borderZero form-control borderzero" id="client-title" maxlength="30">
                </div>
                </div>
                <div class="form-group">
                    <label for="client-url" class="col-sm-2 col-xs-5 font-color">URL</label>
                <div class="col-sm-10 col-xs-7">
                  <input type="text" name="url" class="borderZero form-control borderzero" id="client-url">
                </div>
                </div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="client-id" value="">
				<button type="submit" class="btn btn-submit btn-success borderZero"><i class="fa fa-save"></i> Save Changes</button>
				<button type="button" class="btn btn-danger borderZero" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>


<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content ajax-submit borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'admin/clients/delete', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}
			<div class="modal-header modal-delete">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Client</h4>
			</div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Do you want to delete?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="row-id" value="">
				<button type="submit" class="btn btn-submit btn-danger borderZero"><i class="fa fa-trash"></i> Yes</button>
				<button type="button" class="btn btn-info borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

