<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content ajax-submit borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'admin/application/delete', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}
			<div class="modal-header modal-delete">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-trash"></i> Delete Inquiry</h4>
			</div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Do you want to delete this application?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="row-id" value="">
				<button type="submit" class="btn btn-submit btn-danger borderZero"><i class="fa fa-trash"></i> Yes</button>
				<button type="button" class="btn btn-info borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>


<div class="modal fade" id="modal-scheduled" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-scheduled" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'admin/application/schedule', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_scheduled', 'files' => true)) !!}
			<div class="modal-header modal-warning">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-reply"></i> Schedule</h4>
			</div>
			<div class="modal-body">
				<div id="scheduled-notice"></div>
                  <div class="form-group">
                    <label class="col-sm-4">Set Schedule</label>
                    <div class="col-sm-8">          
                      <div class="input-group date" id="date_sched-picker">
                        {!! Form::text('date_sched', null, ['class' => 'form-control borderZero', 'id' => 'row-dob']) !!}
                        <span class="borderZero input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                      </div>
                    </div>
                  </div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="scheduled-id" value="">
				<button type="submit" class="btn btn-submit btn-danger borderZero"><i class="fa fa-check"></i> Yes</button>
				<button type="button" class="btn btn-info borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>


<div class="modal fade" id="modal-cancelled" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-cancelled" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'admin/application/cancel', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_cancelled', 'files' => true)) !!}
			<div class="modal-header modal-warning">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-reply"></i> Cancel Application</h4>
			</div>
			<div class="modal-body">
				<div id="cancelled-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Set application status to <strong>Cancelled</strong>?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="cancelled-id" value="">
				<button type="submit" class="btn btn-submit btn-danger borderZero"><i class="fa fa-check"></i> Yes</button>
				<button type="button" class="btn btn-info borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="modal fade" id="modal-finished" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-finished" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'admin/application/finish', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_finished', 'files' => true)) !!}
			<div class="modal-header modal-warning">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-reply"></i> Cancel Application</h4>
			</div>
			<div class="modal-body">
				<div id="finished-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Set application status to <strong>Finished</strong>?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="finished-id" value="">
				<button type="submit" class="btn btn-submit btn-danger borderZero"><i class="fa fa-check"></i> Yes</button>
				<button type="button" class="btn btn-info borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>


<div class="modal fade" id="modal-view" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content borderZero">
		    <div id="load-view" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			<form>
			<div class="modal-header modal-warning">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="view-title">View</h4>
			</div>
			<div class="modal-body">
				<div id="view-notice"></div>
        <div class="form-group">
         	<label for="view-tracking_no" class="col-md-3 font-color">Tracking No.</label>
        <div class="col-md-9">
          <p id="view-tracking_no"></p>
        </div>
        </div>
        <div class="form-group">
         	<label for="view-name" class="col-md-3 font-color">Name</label>
        <div class="col-md-9">
          <p id="view-name"></p>
        </div>
        </div>
        <div class="form-group">
         	<label for="view-email" class="col-md-3 font-color">Email</label>
        <div class="col-md-9">
          <p id="view-email"></p>
        </div>
        </div>
        <div class="form-group">
         	<label for="view-phone" class="col-md-3 font-color">Phone</label>
        <div class="col-md-9">
          <p id="view-phone"></p>
        </div>
        </div>
        <div class="form-group">
         	<label for="view-loan" class="col-md-3 font-color">Type Of Loan</label>
        <div class="col-md-9">
          <p id="view-loan"></p>
        </div>
        </div>
        <div class="form-group">
         	<label for="view-dob" class="col-md-3 font-color">Date of Birth</label>
        <div class="col-md-9">
          <p id="view-dob"></p>
        </div>
        </div>
        <div class="form-group">
         	<label for="view-address" class="col-md-3 font-color">Address</label>
        <div class="col-md-9">
          <p id="view-address"></p>
        </div>
        </div>
        <div class="form-group">
         	<label for="view-pref_sched" class="col-md-3 font-color">Preferred Schedule</label>
        <div class="col-md-9">
          <p id="view-pref_sched"></p>
        </div>
        </div>
        <div class="form-group form-date_sched hide">
         	<label for="view-date_sched" class="col-md-3 font-color">Date Scheduled</label>
        <div class="col-md-9">
          <p id="view-date_sched"></p>
        </div>
        </div>
        <div class="form-group form-date_sched_cancel hide">
         	<label for="view-date_sched" class="col-md-3 font-color">Date Scheduled</label>
        <div class="col-md-9">
          <p id="view-date_sched_cancel"></p>
        </div>
        </div>
        <div class="form-group">
         	<label for="view-status" class="col-md-3 font-color">Status</label>
        <div class="col-md-9">
          <p id="view-status"></p>
        </div>
        </div>
        <div class="clearfix"></div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="view-id" value="">
				<button type="button" class="btn btn-info borderZero" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
			</div>
			</form>
		</div>
	</div>
</div>
