@extends('layouts.backend')

@section('scripts')

@stop

@section('content')
    <div class="page-header">
      <h1>Winstar</h1>
      <ol class="breadcrumb">
        <li><a href="#">Admin</a></li>
        <li><a href="#">Home</a></li>
      </ol>
    </div>

    <div id="dashboardHome" class="">
    @if (Auth::user()->usertype_id == 1)
    <div class="col-md-6">
    @else
    <div class="col-md-offset-2 col-md-8">
    @endif
    	<div class="borderZero panel panel-default">
    		<div class="panelHead panel-heading panel-dashboard">
    			<i class="fa fa-laptop"></i> Inquiries
    		</div>
    		<div class="panel-body padding-none">
                <div class="table-responsive">
                  <table id="dash" class="table border-none">
                    <th class="th-border">Tracking No</th>
                    <th class="th-border">Name</th>
                    <th class="th-border">Email</th>
                    <th class="text-right th-border">Status</th>
                        @foreach($inquiries as $row)
                     <tr>
                        <td>{{ $row->tracking_no }}</td>
            		  	<td>{{ $row->name }}</td>
                        <td>{{ $row->email }}</td>
                        <td>
                            @if ($row->status == "Pending")
                            <small class="pull-right">{{ $row->status}}</small>
                            @elseif ($row->status == "Scheduled")
                            <small class="pull-right">{{ $row->status}}</small>
                            @elseif ($row->status == "Finished")
                            <small class="pull-right">{{ $row->status}}</small>
                            @else    
                            <small class="pull-right">{{ $row->status}}</small>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                  </table>
                </div>
                <hr class="hr-margin">
        		<center><a href="{{ url('admin/inquiry') }}" class="btn-view_more">View All</a></center>
    		</div>
    	</div>
    </div>

    @if (Auth::user()->usertype_id == 1)
    <div class="col-md-6">
    	<div class="borderZero panel panel-default">
    		<div class="panel-heading panel-dashboard">
    			<i class="fa fa-user-md"></i> Applications
    		</div>
    		<div class="panel-body padding-none">
                <div class="table-responsive">
                    <table id="dash" class="table border-none">
                        <th class="th-border">Name</th>
                        <th class="th-border">Date</th>
                        <th class="text-right th-border">Status</th>
                		  @foreach($applications as $row)
                          <tr>
                		  	<td>{{ $row->name }}</td>
                            <td>{{ date_format(date_create(substr($row->created_at, 0,10)),'m/d/Y') }}</td>
                            <td class="text-right">
                             @if ($row->status == "Pending")
                                <small class="pull-right">{{ $row->status}}</small>
                                @elseif ($row->status == "Scheduled")
                                <small class="pull-right">{{ $row->status}}</small>
                                @elseif ($row->status == "Finished")
                                <small class="pull-right">{{ $row->status}}</small>
                                @else    
                                <small class="pull-right">{{ $row->status}}</small>
                                @endif
                            </td>
                            </tr>
                		  @endforeach
                    </table>
                </div>
                  <hr class="hr-margin">
        		<center><a href="{{ url('admin/application') }}" class="btn btn-view_more">View All </a></center>
    		</div>
    	</div>
    </div>
    <div class="col-md-6">
    	<div class="borderZero panel panel-default">
    		<div class="panel-heading panel-dashboard">
    			<i class="fa fa-star-o"></i> News
    		</div>
    		<div class="panel-body padding-none">
             <div class="table-responsive">
                <table id="dash" class="table border-none">
                    <th class="th-border">Title</th>
                    <th class="text-right th-border">Date</th>
    		  @foreach($news as $row)
                <tr>
        		  	<td>{{ $row->title }}</td>	
                    <td class="text-right">{{ date_format(date_create(substr($row->created_at, 0,10)),'m/d/Y') }}</td>
                </tr>
    		  @endforeach
                </table>
                </div>
                <hr class="hr-margin">
                <center><a href="{{ url('admin/news') }}" class="btn btn-view_more">View All</a></center>
             </div>  
    		</div>
    </div>
    <div class="col-md-6">
    	<div class="borderZero panel panel-default">
    		<div class="panel-heading panel-dashboard">
    			<i class="fa fa-newspaper-o"></i> Services
    		</div>
            	<div class="panel-body padding-none">
                        <table id="dash" class="table border-none">
                            <th class="th-border">Name</th>
                            <th class="text-right th-border">Date</th>
                    		  @foreach($services as $row)
                                <tr>
                    		  	   <td>{{ $row->title }}</td>
                                   <td class="text-right">{{ date_format(date_create(substr($row->created_at, 0,10)),'m/d/Y') }}</td>
                                </tr>    
                    		  @endforeach
                        </table>
                        <hr class="hr-margin">
                        <center><a href="{{ url('admin/services') }}" class="btn btn-view_more">View All</a></center>
                    </div>
    		</div>
    	</div>
    </div>
    </div>
    @endif
@stop