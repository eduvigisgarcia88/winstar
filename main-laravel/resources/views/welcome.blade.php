<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>WINSTAR</title>

    <!-- Bootstrap -->
    {!! HTML::style('/css/bootstrap.min.css') !!}

    <!-- Font Awesome -->
    {!! HTML::style('/plugins/font-awesome/css/font-awesome.min.css') !!}

    <!-- CSS Plugins -->
    {!! HTML::style('/css/plugins/notification.css') !!}
    {!! HTML::style('/css/plugins/toastr.min.css') !!}
    {!! HTML::style('/css/plugins/fileinput.min.css') !!}
    {!! HTML::style('/css/plugins/jquery.dataTables.min.css') !!}
    {!! HTML::style('/css/plugins/select2-bootstrap.css') !!}
    {!! HTML::style('/css/plugins/select2.css') !!}
    {!! HTML::style('/plugins/components/datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}

    <!-- Main CSS -->
    {!! HTML::style('/css/style.css') !!}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

<!-- Navigation Bar -->
<nav id="navbar-main" class="navbar navbar-default navbar-fixed-top opaque" style="background-color: #1d1d1d !important;">
    <div class="container">
<div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
      <a href="{{ url('/') }}" rel="home">
            <img id="main-logo" class="animated fadeIn img-responsive opaque-logo en-logo" src="{{ url('images/logo') . '/winstar-logo.png'}}">
            </a>
        </div>
        <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
            <li><a class="smoothscroll active" href="#jumbotron">HOME</a></li>
            <li><a class="smoothscroll" href="#about-us">ABOUT US</a></li>
            <li><a class="smoothscroll" href="#online-application">ONLINE APPLICATION</a></li>
            <li><a class="smoothscroll" href="#services">SERVICES</a></li>
            <li><a class="smoothscroll" href="#news">NEWS</a></li>
            <li><a class="smoothscroll" href="#contact-us">CONTACT US</a></li>
            </ul>
        </div>
    </div>
</nav>


<!-- Jumbotron Section-->
<div id="parallax-bg"></div>
<section id="jumbotron" class="jumbotron animated fadeIn">
    <div id="top-nav" class="hidden-xs">
        <div class="container">
            <p id="sub-title">WINSTAR ANGELES CITY LOANS AND CREDIT</p>
            <p class="hidden-xs" id="sub-title-right">(045) 436-4083 / (045) 322-7427</p>
        </div>
    </div>
    <div class="container">
        <div id="jumbotron-header">
            <div class="padding-none">
                <h2>I choose to be <strong>ready</strong> for life after college</h2>
                <p></p>
                <div id="jumbotron-button">
                    <a class="btn btn-sm btn-jumbotron btn-booknow">WHY US</a>
                    <a href="#medical-team" class="btn btn-sm btn-jumbotron smoothscroll">APPLY NOW</a>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- About Us Section -->
<section id="about-us" class="active">
    <div class="container">
        <div class="col-md-4">
        <i class="fa fa-codepen fa-5x"></i>
        <h2>ABOUT US</h2>
          <p>
            The goal of this specialization of Dentistry, to diagnosis, and prevention, and inventory, directing and correct teeth that grow wrong, and affect the shape and structure of the mouth and teeth that appear evident through the face, and includes: Section
          </p>
        </div>
        <div class="col-md-4">
        <i class="fa fa-life-buoy fa-5x"></i>
        <h2>WHAT WE DO</h2>
          <p>
            The goal of this specialization of Dentistry, to diagnosis, and prevention, and inventory, directing and correct teeth that grow wrong, and affect the shape and structure of the mouth and teeth that appear evident through the face, and includes: Section
          </p>
        </div>
        <div class="col-md-4">
        <i class="fa fa-barcode fa-5x"></i>
        <h2>LOANS AND CREDITS</h2>
          <p>
            The goal of this specialization of Dentistry, to diagnosis, and prevention, and inventory, directing and correct teeth that grow wrong, and affect the shape and structure of the mouth and teeth that appear evident through the face, and includes: Section
          </p>
        </div>
    </div>
</section>

<!-- Online Application Section -->
<section id="online-application" class="active">
    <div class="container">
        <h2>NEED TO APPLY NOW? CHECK OUT OUR ONLINE APPLICATION</h2>
        <p>YOU CAN Now APPLY TO us online with the our our hassle free system.</p>
    <div class="col-md-12 padding-none">
        <a class="btn btn-booknow btn-lg">APPLY NOW <i class="fa fa-fw fa-laptop"></i></a>
        <a class="btn btn-booknow btn-lg">INQUIRE <i class="fa fa-fw fa-laptop"></i></a>
    </div>
</section>


<!-- Services Section -->
<section id="services" class="active">
    <div class="container">
        <div class="col-md-4">
        <i class="fa fa-university fa-5x"></i>
        <h2>COMMERCIAL LOAN</h2>
          <p>
            Non-collateral multi-purpose cash loan for employees and professionals
          </p>
        </div>
        <div class="col-md-4">
        <i class="fa fa-shield fa-5x"></i>
        <h2>PERSONAL LOAN</h2>
          <p>
            Non-collateral multi-purpose cash loan for employees and professionals
          </p>
        </div>
        <div class="col-md-4">
        <i class="fa fa-bus fa-5x"></i>
        <h2>CAR LOAN</h2>
          <p>
            Non-collateral multi-purpose cash loan for employees and professionals
          </p>
        </div>
        <div class="col-md-4">
        <i class="fa fa-shopping-cart fa-5x"></i>
        <h2>CONSUMER LOAN</h2>
          <p>
            Non-collateral multi-purpose cash loan for employees and professionals
          </p>
        </div>
        <div class="col-md-4">
        <i class="fa fa-briefcase fa-5x"></i>
        <h2>SALARY LOAN</h2>
          <p>
            Non-collateral multi-purpose cash loan for employees and professionals
          </p>
        </div>
        <div class="col-md-4">
        <i class="fa fa-area-chart fa-5x"></i>
        <h2>OTHERS</h2>
          <p>
            Non-collateral multi-purpose cash loan for employees and professionals
          </p>
        </div>
    </div>
</section>

<!-- Clients Section -->
<section id="clients" class="active">
    <div class="container">
      <h2>Winstar Loans & Credit Corp. CLIENTS</h2>
      <a href="#"><img id="client-samp" style="width: 180px;" src="{{ url('images') . '/client1.png'}}"></a>
      <a href="#"><img id="client-samp" style="width: 180px;" src="{{ url('images') . '/client2.png'}}"></a>
      <a href="#"><img id="client-samp" style="width: 180px;" src="{{ url('images') . '/client3.png'}}"></a>
      <a href="#"><img id="client-samp" style="width: 180px;" src="{{ url('images') . '/client4.png'}}"></a>
      <a href="#"><img id="client-samp" style="width: 180px;" src="{{ url('images') . '/client5.png'}}"></a>
      <a href="#"><img id="client-samp" style="width: 180px;" src="{{ url('images') . '/client6.png'}}"></a>
    </div>
</section>
<!-- CONTACT DETAILS
Address: Unit 202 2nd Floor Oceana Building, Balibago, 
    Angeles, Pampanga 2009 Philippines
Phone No :  (045) 436-4083 / (045) 322-7427
Email : info@winstar.co
Office Hours: 
SOCIAL MEDIA -->


<!-- Contact Information Section -->
<section id="contact-info" class="active">
    <div class="container">
    <center><h2>OUR MAIN OFFICE LOCATION CONTACT INFORMATION</h2></center>
    <div class="col-md-6">
      <h2>MAP GUIDE</h2>
        <iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d962.7449065886997!2d120.5910605628066!3d15.159463549953323!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x3396f273d389910f%3A0xee24cf0cae1778c6!2sOceana+Gas+Station%2C+MacArthur+Hwy%2C+Angeles%2C+Pampanga!3m2!1d15.159361299999999!2d120.59161309999999!5e0!3m2!1sen!2sph!4v1452077756199" width="100%" height="155" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="col-md-6">
      <h2>CONTACT DETAILS</h2>
      <p><strong>Address:</strong> Unit 202 Floor Oceana Building, Balibago Angeles, Pampanga 2009 Philippines</p>
      <p><strong>Phone No:</strong> (045) 436-4083 / (045) 322-7427</p>
      <p><strong>Email:</strong> info@winstar.co</p>
      <p><strong>Office Hours:</strong> Mon – Sat: 9:00 am – 6:00 pm</p>
      <h2>SOCIAL MEDIA</h2> <i class="fa fa-facebook-square fa-2x"></i> &nbsp;<i class="fa fa-twitter fa-2x"></i>
    </div>
    </div>
</section>

</div>

<!-- Footer -->
<footer>
  <div class="container">
    <p class="footer-copyright">© Copyright 2016  Winstar Loans & Credit Corp.</p>
    <p class="xerolabs">Designed and Developed by Xerolabs Web Solutions</p>
  </div>
</footer>
    <!-- jQuery -->
    {!! HTML::script('/js/jquery.js') !!}

    <!-- Bootstrap -->
    {!! HTML::script('/js/bootstrap.min.js') !!}

    <!-- Main Scripts -->
    {!! HTML::script('/js/script.js') !!}
    
    <!-- Sidebar Scripts -->
    {!! HTML::script('/js/sidebar.js') !!}

    <!-- JS Plugins -->
    {!! HTML::script('/js/plugins/toastr.min.js') !!}
    {!! HTML::script('/js/plugins/raphael-min.js') !!}
    {!! HTML::script('/js/plugins/morris.js') !!}
    {!! HTML::script('/js/plugins/graph.js') !!}
    {!! HTML::script('/js/plugins/modernizr.js') !!}
    {!! HTML::script('/js/plugins/jquery.dataTables.js') !!}
    {!! HTML::script('/js/plugins/fileinput.min.js') !!}
    {!! HTML::script('/js/plugins/select2.min.js') !!}
    {!! HTML::script('/plugins/components/moment/min/moment.min.js') !!}
    {!! HTML::script('/plugins/components/datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
    {!! HTML::script('/js/plugins/toastr.min.js') !!}
    {!! HTML::script('/js/plugins/tweenmax.min.js') !!}
    {!! HTML::script('/js/plugins/scrolltoplugin.min.js') !!}

    {!! HTML::script('/js/main.js') !!}

  </body>
</html>