<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- Latest compiled and minified CSS -->
    <title>{{ $title }}</title>
    <!-- Bootstrap -->
    {!! HTML::style('/css/bootstrap.min.css') !!}

    <!-- Font Awesome -->
    {!! HTML::style('/plugins/font-awesome/css/font-awesome.min.css') !!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    {!! HTML::style('/css/login-style.css') !!}
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="bg-login">
    <div class="container v-centered">
      <div class="row">
        <div class="centered">
            <div id="form-login" class="panel panel-default">
                {!! Form::open(array('url' => 'login', 'role' => 'form')) !!}
                @if(Session::has('notice'))
                <div class="alert alert-dismissable alert-{{ Session::get('notice.type') }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <i class="fa fa-fw fa-exclamation-triangle"></i>{{ Session::get('notice.msg') }}
                </div>
                @endif
                <div class="panel-logo">
                    <img src="{{ url('images/logo') . '/winstar-logo.png'}}" style="height: 75px; padding-bottom: 10px;">
                </div>
                <div class="panel-heading">
                    <div class="form-group">
                        <label class="control-label">Login</label>
                        <input type="text" class="form-control" name="username" id="username" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                    </div>
                    <div class="form-group auth-group">
                        <div class="checkbox col-xs-8">
                            <label><input type="checkbox" name="remember"> <span id="rememberme">&nbsp;Remember me</span></label>
                        </div>
                        <div class="col-xs-4 padding-none"><input class="hide" name="honeypot" value=""><button type="submit" class="btn btn-warning btn-responsive btn-login">Login</button></div>
                    </div>
                </div>
                <div class="panel-forgot">
                    <div class="panel-body padding-none">
                        <label><small>Forgot Password?</small></label><br>
                        <span><small>Contact admin or retrieve password on the link provider. <a class="a-forgot btn-reset">Forgot Password</a></small></span>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
      </div>
  </div>
    </div>

    <div class="modal fade" id="modal-reset" tabindex="-1" role="dialog" aria-labelledBy="reset-title" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div id="load-reset" class="loading-pane hide">
            <div><i class="fa fa-inverse fa-spinner fa-spin fa-5x centered"></i></div>
          </div>
          {!! Form::open(array('url' => 'forgot-password', 'role' => 'form', 'class' => 'form-horizontal')) !!}
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 id="reset-title" class="modal-title">Reset Password</h4>
            </div>
            <div class="modal-body">
              <div id="reset-notice"></div>
              <p>
                Please enter your e-mail address. Instructions and a link to reset your password will be sent to you.
              </p>
              <div class="form-group">
                <div class="col-sm-12">
                  <input type="email" name="email" class="form-control disabled" id="pw-email" maxlength="100" placeholder="E-mail Address">
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-fw fa-close"></i>Close</button>
              <button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-check"></i>Submit</button>
            </div>
        {!! Form::close() !!}
        </div>
      </div>
    </div>

    <!-- jQuery -->
    {!! HTML::script('/js/jquery.js') !!}

    <!-- Bootstrap -->
    {!! HTML::script('/js/bootstrap.min.js') !!}
    
    <script type="text/javascript">

        $('.btn-reset').click(function() {
            $("#pw-email").val("");
            $("#modal-reset").modal('show');
        });

    </script>
  </body>
</html>