@extends('layouts.email')

@section('content')

	<p>Application Details Received</p>
	<p>Tracking No: {{ $inquiry->tracking_no }}</p>
	<p>Name: {{ $inquiry->name }}</p>
	<p>Email: {{ $inquiry->email }}</p>
	<p>Phone Number: {{ $inquiry->phone }}</p>
	<p>Type of Loan:</p>
	@if($inquiry->commercial_loan)
	<p>Commercial Loan</p>
	@endif
	@if($inquiry->consumer_loan)
	<p>Consumer Loan</p>
	@endif
	@if($inquiry->personal_loan)
	<p>Personal Loan</p>
	@endif
	@if($inquiry->salary_loan)
	<p>Salary Loan</p>
	@endif
	@if($inquiry->car_loan)
	<p>Car Loan</p>
	@endif
	@if($inquiry->others)
	<p>{{ $inquiry->others }} (Others)</p>
	@endif
	<p>Date of Birth: {{ date("F j, Y", strtotime($inquiry->dob)) }}</p>
	<p>Address: {{ $inquiry->address }}</p>
	<p>Preferred Schedule: {{ date("F j, Y g:i A", strtotime($inquiry->pref_sched)) }}</p>

@stop