@extends('layouts.email')

@section('content')
    <p>Hello</p>
    <p style="margin: 20px 0;">You are receiving this e-mail because you requested a password reset for your Samaya Clinic account. If you did not request this change, simply disregard this message.</p>
    <div>
      <!--[if mso]>
      <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="{{ URL::to('reset-password', array($token)) }}" style="height:40px;width:400px;v-text-anchor:middle;padding:10px 15px;" stroke="f" fillcolor="#d9534f">
        <w:anchorlock/>
        <center style="color:#fff;font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:bold;">
          Reset my Password
        </center>
      </v:rect>
      <![endif]-->
      <![if !mso]>
      <table cellspacing="0" cellpadding="0">
      	<tr> 
		      <td align="center" height="40" bgcolor="#d9534f" style="color: #fff; display: block;">
		        <a href="{{ URL::to('reset-password', array($token)) }}" style="color: #fff; font-size:14px; font-weight: bold; font-family: Helvetica, Arial, sans-serif; text-decoration: none; line-height: 40px; width: 100%; display: inline-block">
		          Reset my Password
		        </a>
		      </td> 
	      </tr>
      </table> 
      <![endif]>
    </div>
    <p style="margin: 20px 0;">If the button doesn't work, copy and paste this link in your browser window:
    <p><a href="{{ URL::to('reset-password', array($token)) }}" target="_blank">{{ URL::to('reset-password', array($token)) }}</a></p>
    <p>This link will expire in {{ Config::get('auth.reminder.expire', 60) }} minutes for security purposes.</p>
    <p style="margin-top: 20px;">Thank you!</p>
@stop
