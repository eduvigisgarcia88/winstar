@extends('layouts.email')

@section('content')

	<p>Hi,</p>
	<p>{{ $inquiry->body }}.</p>
	<br>
	<p>Regards</p>
	<p>Winstar Loans and Credits</p>

@stop