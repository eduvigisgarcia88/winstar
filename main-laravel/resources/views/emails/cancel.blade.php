@extends('layouts.email')

@section('content')

	<p>Hi {{ $inquiry->name }},</p>
	<p>This is to inform you that you’re application has been cancelled. For more information kindly call us at {{ $contact->phone }} or email us at {{ $contact->email }}.</p>
	<br>
	<p>Regards</p>
	<p>Winstar Loans and Credits</p>

@stop