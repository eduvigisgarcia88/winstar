@extends('layouts.email')

@section('content')

	<p>Hi {{ $inquiry->name }},</p>
	<p>Thank you for choosing Winstar Loans and Credits.</p><br>
	<p>Regards</p>
	<p>Winstar Loans and Credits</p>
	<p>{{ $contact->address }}</p>
	<p>{{ $contact->phone }}</p>

@stop