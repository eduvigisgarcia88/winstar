@extends('layouts.email')

@section('content')

	<p>Hi {{ $inquiry->name }},</p>
	<p>You’re scheduled application will be on {{ date("F j, Y g:i A", strtotime($inquiry->date_sched)) }}. For more information kindly call us at {{ $contact->phone }} or email us at {{ $contact->email }}.</p>
	<p>Tracking No: {{ $inquiry->tracking_no }}</p>
	<br>
	<p>Regards</p>
	<p>Winstar Loan and Credits</p>

@stop