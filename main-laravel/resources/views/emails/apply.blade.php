@extends('layouts.email')

@section('content')

	<p>Hi {{ $inquiry->name }},</p>
	<p>This is to confirm that we received your preferred application schedule at {{ date("F j, Y g:i A", strtotime($inquiry->pref_sched)) }}.</p>
	<p>Here is your tracking no: {{ $inquiry->tracking_no }}</p>
	<br>
	<p>Regards,</p>
	<p>Winstar Loans and Credits</p>
	<p>{{ $contact->address }}</p>
	<p>{{ $contact->phone }}</p>

@stop