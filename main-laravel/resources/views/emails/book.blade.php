@extends('layouts.email')

@section('content')

	<p>Hi {{ $inquiry->name }},</p>
	<p>You’re scheduled appointment to our clinic will be on {{ date("F j, Y g:i A", strtotime($inquiry->date_sched)) }}. For more information kindly call us at {{ $contact->phone }} or email us at {{ $contact->email }}.</p>
	<br>
	<p>Regards</p>
	<p>Samaya Group Clinic</p>

@stop