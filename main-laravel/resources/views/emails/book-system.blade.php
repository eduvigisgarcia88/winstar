@extends('layouts.email')

@section('content')

	<p>Client Booking Scheduled</p>
	<p>Name: {{ $inquiry->name }}</p>
	<p>Email: {{ $inquiry->email }}</p>
	<p>Contact No.: {{ $inquiry->phone }}</p>
	<p>Preferred Date: {{ date("F j, Y g:i A", strtotime($inquiry->user_sched)) }}</p>
	<p>Date Scheduled: {{ date("F j, Y g:i A", strtotime($inquiry->date_sched)) }}</p>
	<p>Message:</p> 
	<p>{{ $inquiry->body }}</p>

@stop