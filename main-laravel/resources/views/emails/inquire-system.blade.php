@extends('layouts.email')

@section('content')

	<p>Inquiry Details Received</p>
	<p>Name: {{ $inquiry->name }}</p>
	<p>Email: {{ $inquiry->email }}</p>
	<p>Phone No.: {{ $inquiry->phone }}</p>
	<p>Message:</p> 
	<p>{{ $inquiry->body }}</p>

@stop