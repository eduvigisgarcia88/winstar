@extends('layouts.backend')
@section('styles')


@stop
@section('scripts')
  <script type="text/javascript">

    $_token = '{{ csrf_token() }}';

  

    $(document).ready(function() {
      $('.summernote').addClass('borderZero').summernote({
      toolbar: [
    // [groupName, [list of button]]
   ['style', ['bold', 'italic', 'underline', 'clear']],
    ['fontname', ['fontname', 'fontsize', 'color']],
    
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height', 'table']],
    ['link', ['link', 'picture', 'video']],
    ['fullscreen', ['fullscreen', 'codeview', 'help']],
  ]
      });
    });

	// submit form
    $("#form-save").find("form").submit(function(e) {
   
  		var form = $("#form-save").find("form");
  		var loading = $("#load-form");

  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			//data: form.serialize(),
			data: new FormData($("#modal-save_form")[0]),
			dataType: "json",	
			async: true,
	  		processData: false,
	  		contentType: false,

			success: function(response) {
				
		        if (response.unauthorized) {
		          window.location.href = '{{ url("login")}}';
		        }
				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#form-notice');
				} else {
					$("#load-success").removeClass('hide');
				    $.ajax({
				        type: 'POST',
				        url: '{{ url("admin/news/flash") }}',
				        data: { _token: $_token }
				    }).done(function () {
				        window.location.href = '{{ url("admin/news")}}';
				    });
				}
				loading.addClass('hide');
			}
  		});
    });

</script>
@stop

@section('content')
<div class="page-header">
  <h1>Dashboard</h1>
  <ol class="breadcrumb">
    <li><a href="#">Dashboard</a></li>
    <li><a href="#">News</a></li>
    <li><a href="#">Create News</a></li>
  </ol>
</div>
<div id="form-save" class="col-xs-12">
<div id="load-form" class="loading-pane hide">
  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
</div>
<div id="load-success" class="loading-pane hide">
  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
</div>
{!! Form::open(array('url' => 'admin/news/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
<div class="borderZero panel panel-default">
  <div class="panel-body">
  	<div id="form-notice"></div>
		<div class="form-group">
		<div class="col-sm-2">
			{!! Form::label('row-title', 'News Title', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-10">
			{!! Form::text('title', null, ['class' => 'form-control borderZero', 'id' => 'row-title']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-2">
			{!! Form::label('row-body', 'Content', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-10">
		<textarea name="body" class="form-control summernote borderZero" id="row-body"></textarea>
		</div>
		</div>
  	</div>
  <div class="panel-footer panelFooter">
  	<div class="row">
  		<div class="col-lg-6 col-xs-4">
  			<a class="btn btn-goback btn-back borderZero" href="{{ url('admin/news') }}">Back</a>
  		</div>
  		<div class="col-lg-6 col-xs-8 text-right">
  			<button type="submit" class="btn btn-submit btn-primary borderZero"><i class="fa fa-save"></i> Create News</button>
  		</div>
  	</div>
  </div>
</div>
{!! Form::close() !!}
</div>
@stop