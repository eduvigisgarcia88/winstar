@extends('layouts.backend')

@section('scripts')
<script type="text/javascript">

  $_token = '{{ csrf_token() }}';

  function refreshAbout() {

      var loading = $("#load-about");
      var table = $("#rows-about");
      var url = "{{ url('/') }}";
      loading.removeClass('hide');
    
      $.post("{{ url('admin/custom/refresh') }}", { _token: $_token }, function(response) {

          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }
        // clear table
        table.html("");
        // populate table
        $.each(response.about, function(index, row) {

            table.append(
                '<tr>' +
                '<td><i class="fa ' + row.icon + '"></i></td>' +
                '<td>' + row.title + '</td>' +
                '<td>' + moment(row.updated_at).format('LLL') + '</td>' +
                '<td class="text-right">' + 
                  '<a target="_blank" href="' + url + '/news/' + row.slug + '" class="btn btn-xs borderZero btn-info btn-view" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-eye"></i></a>' +
                  '&nbsp;<a href="' + url + '/admin/custom/about/' + row.id + '/edit" class="btn btn-xs borderZero btn-primary btn-edit" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
                '</td>' +
                '</tr>'
            );
        });

      loading.addClass("hide");

      }, 'json');

  }

  function refreshContact() {

      var loading = $("#load-contact");
      var table_contact = $("#rows-contact");
      var table_social = $("#rows-social");
      var url = "{{ url('/') }}";
      loading.removeClass('hide');
    
      $.post("{{ url('admin/custom/refresh') }}", { _token: $_token }, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }
        // clear table
        table_contact.html("");
        table_social.html("");
        // populate table

        table_contact.append(
          '<tr>' +
            '<td>Address: </td><td>' + response.contact.address + '</td>' +
          '</tr>' +
          '<tr>' +
            '<td>Phone No: </td><td>' + response.contact.phone + '</td>' +
          '</tr>' +
          '<tr>' +
            '<td>Email: </td><td>' + response.contact.email + '</td>' +
          '</tr>' +
          '<tr>' +
            '<td>Office Hours: </td><td>' + response.contact.office_hours + '</td>' +
          '</tr>'
        );

        table_social.append(
          '<tr>' +
            '<td>Facebook Link: </td><td>' + response.contact.facebook_link + '</td>' +
          '</tr>' +
          '<tr>' +
            '<td>Twitter Link: </td><td>' + response.contact.twitter_link + '</td>' +
          '</tr>'
        );

      loading.addClass("hide");

      }, 'json');

  }


</script>
@stop

@section('content')
<div class="page-header">
  <h1>Winstar</h1>
  <ol class="breadcrumb">
    <li><a href="#">Admin</a></li>
    <li><a href="#">Custom Pages</a></li>
  </ol>
</div>

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading panelHeader">
  	<div class="row">
  		<div class="col-xs-9 col-sm-6 col-lg-3">
        <h5>Company Information</h5>
  		</div>
  		<div class="col-lg-6 col-offset-lg-3 col-xs-3 col-sm-6 text-right pull-right">
  			<button type="button" class="btn btn-success btn-md btn-attr borderZero" onclick="refreshAbout()"><i class="fa fa-refresh"></i></button> 
  		</div>
  	</div>
  </div>
  <div class="row panel-body">
  	<div class="table-responsive col-xs-12">
        <div id="load-about" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
        </div>
        <table id="about-lists" class="table table-striped table-pagination">
          <thead class="myTableThead">
            <tr>
              <th class="myTableHeader">Icon</th>
              <th class="myTableHeader">Title</th>
              <th class="myTableHeader">Date Updated</th>
              <th class="text-right myTableHeader"><i id="load-list" class="fa fa-refresh fa-spin pull-right hide"></i></th>
            </tr>
          </thead>
          <tbody id="rows-about">
            @foreach($about as $row)
            <tr>
              <td><i class="fa {{ $row->icon }}"></i></td>
              <td>{{ $row->title }}</td>
              <td>{{ date_format(new DateTime($row->updated_at), 'F d, Y g:i A') }}</td>
              <td class="text-right">
                <a target="_blank" href="{{ url('/') . '/company/' . $row->slug }}" class="btn btn-xs borderZero btn-info btn-view" title="View News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-eye"></i></a>
                <a href="{{ url('admin/custom/about') . '/' . $row->id . '/edit'}}" class="btn btn-xs borderZero btn-primary btn-edit" title="Edit News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
			</div>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading panelHeader">
    <div class="row">
      <div class="col-xs-3 col-sm-6 col-lg-3">
        <h5>Contact Us</h5>
      </div>
      <div class="col-lg-6 col-offset-lg-3 col-xs-9 col-sm-6 text-right pull-right">
        <a target="_blank" href="{{ url('contact-us') }}" class="btn btn-info btn-md btn-attr borderZero"><i class="fa fa-eye"></i> View Page</a> 
        <a href="{{ url('admin/custom/contact/edit') }}" type="button" class="btn btn-primary btn-md btn-attr borderZero"><i class="fa fa-pencil"></i> Edit</a> 
      </div>
    </div>
  </div>
</div>
		@if(Session::has("notice-about"))
  		<div id="alert" class="col-lg-3 col-lg-offset-9 borderZero alert alert-dismissable alert-{{ Session::get('notice-about.type') }}">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <i class="fa fa-fw fa-exclamation-triangle"></i> {{ Session::get('notice-about.msg') }}
  		</div>
  	@endif
@stop