@extends('layouts.backend')

@section('scripts')
  <script type="text/javascript">
    $_token = '{{ csrf_token() }}';

    $(document).ready(function() {
      $('.summernote').summernote({
           toolbar: [
    // [groupName, [list of button]]
   ['style', ['bold', 'italic', 'underline', 'clear']],
    ['fontname', ['fontname', 'fontsize', 'color']],
    
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height', 'table']],
    ['link', ['link', 'picture', 'video']],
    ['fullscreen', ['fullscreen', 'codeview', 'help']],
  ]
      });

      $("#row-icon").val("{{ $contact->icon }}")
    });
	
	// submit form
    $("#form-save").find("#modal-save_form").submit(function(e) {
  		var form = $("#form-save").find("form");
  		var loading = $("#load-form");

  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: '{{ url("admin/custom/contact/save") }}',
			//data: form.serialize(),
			data: new FormData($("#modal-save_form")[0]),
			dataType: "json",	
			async: true,
	  		processData: false,
	  		contentType: false,

			success: function(response) {

		        if (response.unauthorized) {
		          window.location.href = '{{ url("login")}}';
		        }

				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#form-notice');
				} else {
					$("#load-success").removeClass('hide');
				    $.ajax({
				        type: 'POST',
				        url: '{{ url("admin/custom/flash/edit") }}',
				        data: { _token: $_token }
				    }).done(function () {
				        window.location.href = '{{ url("admin/custom")}}';
				    });
				}
				loading.addClass('hide');
			}
  		});
    });
</script>
@stop

@section('content')
<div class="page-header">
  <h1>Dashboard</h1>
  <ol class="breadcrumb">
    <li><a href="#">Dashboard</a></li>
    <li><a href="#">Custom Pages</a></li>
    <li><a href="#">Contact Us</a></li>
    <li><a href="#">Edit</a></li>
  </ol>
</div>
	<div id="form-save" class="col-xs-12">
	<div id="load-form" class="loading-pane hide">
	  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
	</div>
	<div id="load-success" class="loading-pane hide">
	  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
	</div>
	{!! Form::open(array('url' => 'admin/custom/contact/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
	<div class="borderZero panel panel-default">
	  <div class="panel-body">
	  	<div id="form-notice"></div>
		<div class="form-group">
		<div class="col-sm-2">
			{!! Form::label('row-body', 'Content', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-10">
			<textarea name="body" class="form-control summernote borderZero" id="row-body">{{ $contact->body }}</textarea>
		</div>
		</div>
	  </div>
	  <div class="panel-footer">
	  	<div class="row">
	  		<div class="col-lg-6  col-md-6 col-sm-6 col-xs-4">
	  			<a class="btn btn-goback btn-back borderZero" href="{{ url('admin/custom') }}">Back</a>
	  		</div>
	  		<div class="col-lg-6  col-md-6 col-sm-6 col-xs-8 text-right">
	  			<input type="hidden" name="id" value="{{ $contact->id }}">
	  			<button type="submit" class="btn btn-submit btn-primary borderZero"><i class="fa fa-save"></i> Update News</button>
	  		</div>
	  	</div>
	  </div>
	</div>
	{!! Form::close() !!}
</div>

@stop