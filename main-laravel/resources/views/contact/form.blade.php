<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
            <div id="load-form" class="loading-pane hide">
                <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
            </div>
			{!! Form::open(array('url' => 'admin/contact', 'role' => 'form', 'class' => 'form-horizontal', 'files' => true)) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Set Schedule</h4>
			</div>
			<div class="modal-body">
			  <div id="form-notice"></div>
                <div class="input-group date" id="datetimepicker1">
                    {!! Form::text('date_sched', null, ['class' => 'form-control', 'id' => 'row-date_sched', 'placeholder' => 'SET SCHEDULE', 'required']) !!}
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="row-id" value="">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-submit"><i class="fa fa-save"></i> Save changes</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>