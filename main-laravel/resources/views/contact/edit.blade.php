@extends('layouts.backend')

@section('scripts')
  <script type="text/javascript">
    $_token = '{{ csrf_token() }}';

    $(document).ready(function() {
      $('.summernote').summernote({
        height: 300,
        tabsize: 2
      });

      $("#row-icon").val("{{ $contact->icon }}")
    });
	
	// submit form
    $("#form-save").find("#modal-save_form").submit(function(e) {
  		var form = $("#form-save").find("form");
  		var loading = $("#load-form");

  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: '{{ url("admin/contact/save") }}',
			//data: form.serialize(),
			data: new FormData($("#modal-save_form")[0]),
			dataType: "json",	
			async: true,
	  		processData: false,
	  		contentType: false,

			success: function(response) {

		        if (response.unauthorized) {
		          window.location.href = '{{ url("login")}}';
		        }
        
				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#form-notice');
				} else {
					$("#load-success").removeClass('hide');
				    $.ajax({
				        type: 'POST',
				        url: '{{ url("admin/contact/flash/edit") }}',
				        data: { _token: $_token }
				    }).done(function () {
				        window.location.href = '{{ url("admin/contact")}}';
				    });
				}
				loading.addClass('hide');
			}
  		});
    });
</script>
@stop

@section('content')
<div class="page-header">
  <h1>Dashboard</h1>
  <ol class="breadcrumb">
    <li><a href="#">Dashboard</a></li>
    <li><a href="#">Contact Information</a></li>
    <li><a href="#">Edit</a></li>
  </ol>
</div>
	<div id="form-save" class="col-xs-12">
	<div id="load-form" class="loading-pane hide">
	  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
	</div>
	<div id="load-success" class="loading-pane hide">
	  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
	</div>
	{!! Form::open(array('url' => 'admin/contact/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
	<div class="borderZero panel panel-default">
	  <div class="panel-body">
	  	<div id="form-notice"></div>
		<div class="form-group">
		<div class="col-sm-2">
			{!! Form::label('row-address', 'Address', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-10">
			<input type="text" class="form-control borderZero" name="address" id="row-address" value="{{ $contact->address }}">
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-2">
			{!! Form::label('row-phone', 'Phone No.', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-10">
			<input type="text" class="form-control borderZero" name="phone" id="row-phone" value="{{ $contact->phone }}">
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-2">
			{!! Form::label('row-email', 'Email', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-10">
			<input type="text" class="form-control borderZero" name="email" id="row-email" value="{{ $contact->email }}">
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-2">
			{!! Form::label('row-office_hours', 'Office Hours', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-10">
			<input type="text" class="form-control borderZero" name="office_hours" id="row-office_hours" value="{{ $contact->office_hours }}">
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-2">
			{!! Form::label('row-facebook_link', 'Facebook Link', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-10">
			<input type="text" class="form-control borderZero" name="facebook_link" id="row-facebook_link" value="{{ $contact->facebook_link }}">
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-2">
			{!! Form::label('row-twitter_link', 'Twitter Link', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-10">
			<input type="text" class="form-control borderZero" name="twitter_link" id="row-twitter_link" value="{{ $contact->twitter_link }}">
		</div>
		</div>
	  </div>
	  <div class="panel-footer panelFooter">
	  	<div class="row">
	  		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-4">
	  			<a class="btn btn-goback btn-back borderZero" href="{{ url('admin/contact') }}">Back</a>
	  		</div>
	  		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-8 text-right">
	  			<input type="hidden" name="id" value="{{ $contact->id }}">
	  			<button type="submit" class="btn btn-submit btn-primary borderZero"><i class="fa fa-save"></i> Update Contact</button>
	  		</div>
	  	</div>
	  </div>
	</div>
	{!! Form::close() !!}
</div>

@stop