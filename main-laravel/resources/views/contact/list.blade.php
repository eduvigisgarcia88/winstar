@extends('layouts.backend')

@section('scripts')
<script type="text/javascript">

  $_token = '{{ csrf_token() }}';

  function refresh() {

      var loading = $("#load-contact");
      var table_contact = $("#rows-contact");
      var table_social = $("#rows-social");
      var url = "{{ url('/') }}";
      loading.removeClass('hide');
    
      $.post("{{ url('admin/contact/refresh') }}", { _token: $_token }, function(response) {

        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }
        
        // clear table
        table_contact.html("");
        table_social.html("");
        // populate table

        table_contact.append(
          '<tr>' +
            '<td>Address: </td><td>' + response.contact.address + '</td>' +
          '</tr>' +
          '<tr>' +
            '<td>Phone No: </td><td>' + response.contact.phone + '</td>' +
          '</tr>' +
          '<tr>' +
            '<td>Email: </td><td>' + response.contact.email + '</td>' +
          '</tr>' +
          '<tr>' +
            '<td>Office Hours: </td><td>' + response.contact.office_hours + '</td>' +
          '</tr>'
        );

        table_social.append(
          '<tr>' +
            '<td>Facebook Link: </td><td>' + response.contact.facebook_link + '</td>' +
          '</tr>' +
          '<tr>' +
            '<td>Twitter Link: </td><td>' + response.contact.twitter_link + '</td>' +
          '</tr>'
        );

      loading.addClass("hide");

      }, 'json');

  }


</script>
@stop

@section('content')
<div class="page-header">
  <h1>Winstar</h1>
  <ol class="breadcrumb">
    <li><a href="#">Admin</a></li>
    <li><a href="#">Contact Information</a></li>
  </ol>
</div>

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading panelHeader">
    <div class="row">
      <div class="col-xs-5 col-sm-6 col-lg-3">
        <h5>Contact</h5>
      </div>
      <div class="col-lg-6 col-offset-lg-3 col-xs-7 col-sm-6 text-right pull-right">
        <a href="{{ url('admin/contact/edit') }}" type="button" class="btn btn-primary btn-md btn-attr borderZero"><i class="fa fa-pencil"></i> Edit</a> 
        <button type="button" class="btn btn-success btn-md btn-attr borderZero" onclick="refresh()"><i class="fa fa-refresh"></i></button> 
      </div>
    </div>
  </div>
  <div class="panel-body">
    <div class="table-responsive">
        <div id="load-contact" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
        </div>
        <table id="contact-lists" class="table table-striped table-pagination">
          <thead class="myTableThead">
            <tr>
              <th class="myTableHeader">Contact Details</th>
              <th class="myTableHeader"></th>
            </tr>
          </thead>
          <tbody id="rows-contact">
            @if ($contact)
              <tr>
                <td>Address: </td><td>{{ $contact->address }}</td>
              </tr>
              <tr>
                <td>Phone No: </td><td>{{ $contact->phone }}</td>
              </tr>
              <tr>
                <td>Email: </td><td>{{ $contact->email }}</td>
              </tr>
              <tr>
                <td>Office Hours: </td><td>{{ $contact->office_hours }}</td>
              </tr>
            @endif
          </tbody>
        </table>
        <br class="visible-xs">
        <table id="social-lists" class="table table-striped table-pagination">
          <thead class="myTableThead">
            <tr>
              <th class="myTableHeader">Social Media</th>
              <th class="myTableHeader"></th>
            </tr>
          </thead>
          <tbody id="rows-social">
            @if ($contact)
              <tr>
                <td>Facebook Link: </td><td>{{ $contact->facebook_link }}</td>
              </tr>
              <tr>
                <td>Twitter Link: </td><td>{{ $contact->twitter_link }}</td>
              </tr>
            @endif
          </tbody>
        </table>
      </div>

  </div>
</div>
		@if(Session::has("notice"))
  		<div id="alert" class="col-lg-3 col-lg-offset-9 borderZero alert alert-dismissable alert-{{ Session::get('notice.type') }}">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <i class="fa fa-fw fa-exclamation-triangle"></i> {{ Session::get('notice.msg') }}
  		</div>
  	@endif
@stop