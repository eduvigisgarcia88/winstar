<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanAmortization extends Model
{
    protected $table = 'loan_amortizations';
}
