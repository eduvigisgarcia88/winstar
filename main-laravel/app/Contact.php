<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contact';

    protected $fillable = [
    	'address_eng',
    	'address_arabic',
    	'phone',
    	'email',
    	'clinic_eng',
    	'clinic_arabic',
    	'facebook_link',
    	'twitter_link',
    	'created_at',
    	'updated_at',
    ];
}
