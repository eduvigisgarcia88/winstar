<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';
    //

    protected $fillable = [
    	'photo',
	    'name_eng',
	    'name_arabic',
	    'specialty_eng',
	    'specialty_arabic',
	    'education_eng',
	    'education_arabic',
	    'cur_pos_eng',
	    'cur_pos_arabic',
	    'about_me_eng',
	    'about_me_arabic'
    ];

}
