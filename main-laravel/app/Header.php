<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Header extends Model
{
    protected $table = 'header';

    protected $fillable = [
    	'title_eng',
    	'desc_eng',
    	'title_arabic',
    	'desc_arabic',
    ];
}
