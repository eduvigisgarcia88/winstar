<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'loans';

    public function getLoanAmortization() {
		return $this->hasMany('App\LoanAmortization', 'loan_id')->where('status', 1);
	}

}
