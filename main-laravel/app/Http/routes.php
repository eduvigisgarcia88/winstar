<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/',	'HomeController@index');
Route::get('news', 'HomeController@viewNews');
Route::get('contact-us', 'HomeController@viewContactus');
Route::get('news/{slug}', 'HomeController@viewNewsreadmore');
Route::get('company/{slug}', 'HomeController@viewAbout');
Route::get('services/{slug}', 'HomeController@viewServices');

Route::get('login',	'AuthController@login');
Route::post('login', 'AuthController@doLogin');
Route::get('logout', 'AuthController@logout');
Route::post('inquire', 'InquiryController@save');
Route::post('apply', 'ApplicationController@save');

Route::group(['middleware' => 'App\Http\Middleware\BackendMiddleware'], function() {
	Route::get('admin', 'BackendController@index');
});

Route::get('admin/news', 'NewsController@index');
Route::get('admin/news/create', 'NewsController@create');
Route::get('admin/news/{id}/edit', 'NewsController@edit');

Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/news/refresh', 'NewsController@doList');
	Route::post('admin/news/delete', 'NewsController@delete');
	Route::post('admin/news/save', 'NewsController@save');
	Route::post('admin/news/view', 'NewsController@view');
});

Route::post('admin/news/flash', function () {
    Session::flash('notice', array(
                            'msg' => 'News successfully added.',
                            'type' => 'success'));
    return ['notice', array(
                            'msg' => 'News successfully added.',
                            'type' => 'success')];
});

Route::post('admin/news/flash/edit', function () {
    Session::flash('notice', array(
                            'msg' => 'News successfully updated.',
                            'type' => 'success'));
    return ['notice', array(
                            'msg' => 'News successfully updated.',
                            'type' => 'success')];
});

Route::get('admin/services', 'ServicesController@index');
Route::get('admin/services/create', 'ServicesController@create');
Route::get('admin/services/{id}/edit', 'ServicesController@edit');

Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/services/refresh', 'ServicesController@doList');
	Route::post('admin/services/delete', 'ServicesController@delete');
	Route::post('admin/services/save', 'ServicesController@save');
	Route::post('admin/services/view', 'ServicesController@view');
});

Route::post('admin/services/flash', function () {
    Session::flash('notice', array(
                            'msg' => 'Service successfully added.',
                            'type' => 'success'));
    return ['notice', array(
                            'msg' => 'Service successfully added.',
                            'type' => 'success')];
});

Route::post('admin/services/flash/edit', function () {
    Session::flash('notice', array(
                            'msg' => 'Service successfully updated.',
                            'type' => 'success'));
    return ['notice', array(
                            'msg' => 'Service successfully updated.',
                            'type' => 'success')];
});

Route::get('admin/clients', 'ClientController@index');
Route::get('admin/clients/create', 'ClientController@create');

Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/clients/refresh', 'ClientController@doList');
	Route::post('admin/clients/delete', 'ClientController@delete');
	Route::post('admin/clients/edit', 'ClientController@edit');
	Route::post('admin/clients/save', 'ClientController@save');
	Route::post('admin/clients/view', 'ClientController@view');
});

Route::post('admin/clients/flash', function () {
    Session::flash('notice', array(
                            'msg' => 'Client successfully added.',
                            'type' => 'success'));
    return ['notice', array(
                            'msg' => 'Client successfully added.',
                            'type' => 'success')];
});

Route::post('admin/clients/flash/edit', function () {
    Session::flash('notice', array(
                            'msg' => 'Client successfully updated.',
                            'type' => 'success'));
    return ['notice', array(
                            'msg' => 'Client successfully updated.',
                            'type' => 'success')];
});


// password reset
Route::post('forgot-password','AuthController@doForgotPassword');
Route::get('reset-password/{token}','AuthController@resetPassword');
Route::post('reset-password', 'AuthController@doResetPassword');



Route::get('admin/contact', 'ContactController@index');
Route::get('admin/contact/edit', 'ContactController@edit');

Route::group(array('before' => 'ajax'), function() {
    Route::post('admin/contact/refresh', 'ContactController@doList');
    Route::post('admin/contact/save', 'ContactController@save');
    Route::post('admin/contact/view', 'ContactController@view');
});

Route::post('admin/contact/flash/edit', function () {
    Session::flash('notice', array(
                            'msg' => 'Contact successfully updated.',
                            'type' => 'success'));
    return ['notice', array(
                            'msg' => 'Contact successfully updated.',
                            'type' => 'success')];
});

Route::get('admin/custom', 'CustomController@index');
Route::get('admin/custom/about/{id}/edit', 'CustomController@edit');
Route::get('admin/custom/contact/edit', 'CustomController@editContact');

Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/custom/refresh', 'CustomController@doList');
	Route::post('admin/custom/about/save', 'CustomController@saveAbout');
	Route::post('admin/custom/contact/save', 'CustomController@saveContact');
	Route::post('admin/custom/about/view', 'HomeController@viewAbout');
});

Route::post('admin/custom/flash/edit', function () {
    Session::flash('notice-about', array(
                            'msg' => 'Changes has been saved.',
                            'type' => 'success'));
    return ['notice-about', array(
                            'msg' => 'Changes has been saved.',
                            'type' => 'success')];
});


Route::get('admin/inquiry', 'InquiryController@index');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/inquiry/refresh', 'InquiryController@doList');
	Route::post('admin/inquiry/view', 'InquiryController@view');
	Route::post('admin/inquiry/delete', 'InquiryController@delete');
	Route::post('admin/inquiry/replied', 'InquiryController@replied');
});

Route::get('admin/application', 'ApplicationController@index');
Route::group(array('before' => 'ajax'), function() {
    Route::post('admin/application/refresh', 'ApplicationController@doList');
    Route::post('admin/application/view', 'ApplicationController@view');
    Route::post('admin/application/delete', 'ApplicationController@delete');
    Route::post('admin/application/schedule', 'ApplicationController@schedule');
    Route::post('admin/application/cancel', 'ApplicationController@cancel');
    Route::post('admin/application/finish', 'ApplicationController@finish');
});

Route::get('admin/loans', 'LoanController@index');
Route::group(array('before' => 'ajax'), function() {
    Route::post('admin/loans/refresh', 'LoanController@doList');
    Route::post('admin/loans/save', 'LoanController@save');
    Route::post('admin/loans/amortize', 'LoanController@saveAmortization');
    Route::post('admin/loans/edit-amortize', 'LoanController@editAmortization');
    Route::post('admin/loans/view', 'LoanController@view');
    Route::post('admin/loans/edit', 'LoanController@edit');
    Route::post('admin/loans/preview', 'LoanController@preview');
    Route::post('admin/loans/delete', 'LoanController@delete');
    Route::post('admin/loans/download', 'LoanController@download');
    Route::post('admin/loans/view-amortize', 'LoanController@viewAmortization');
    Route::post('admin/loans/delete-amortize', 'LoanController@deleteAmortization');
});

Route::get('admin/settings', 'SettingsController@index');
Route::group(array('before' => 'ajax'), function() {
    Route::post('admin/settings/refresh', 'SettingsController@doList');
    Route::post('admin/settings/save', 'SettingsController@save');
});

Route::get('admin/banner','BannerController@index');
Route::group(array('before' => 'ajax'), function() {
     Route::post('admin/banner/save', 'BannerController@save');
     Route::post('admin/banner/refresh', 'BannerController@doList');
     Route::post('admin/banner/manipulate', 'BannerController@edit');
     Route::post('admin/banner/delete', 'BannerController@delete');
     Route::post('admin/banner/disable', 'BannerController@disable');
     Route::post('admin/banner/enable', 'BannerController@enable');
});