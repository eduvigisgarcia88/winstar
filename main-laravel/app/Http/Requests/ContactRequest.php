<?php

namespace App\Http\Requests;

use Auth;
use App\Http\Requests\Request;

class ContactRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check()) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address_eng' => 'required',
            'address_arabic' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'clinic_eng' => 'required',
            'clinic_arabic' => 'required',
            'facebook_link' => 'required',
            'twitter_link' => 'required',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [ 
            'address_eng' => 'address (english)',
            'address_arabic' => 'address (arabic)',
            'phone' => 'phone number',
            'email' => 'email address',
            'clinic_eng' => 'clinic hours (english)',
            'clinic_arabic' => 'clinic hours (arabic)',
            'facebook_link' => 'facebook link',
            'twitter_link' => 'twitter link',
        ];
    }
}
