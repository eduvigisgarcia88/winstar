<?php

namespace App\Http\Requests;

use Auth;
use App\Http\Requests\Request;

class HeaderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_eng' => 'required',
            'desc_eng' => 'required',
            'title_arabic' => 'required',
            'desc_arabic' => 'required',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'title_eng' => 'title (english)', 
            'desc_eng' => 'description (english)', 
            'title_arabic' => 'title (arabic)', 
            'desc_arabic' => 'description (arabic)', 
        ];
    }
}
