<?php

namespace App\Http\Controllers;

use Auth;
use Crypt;
use Input;
use Request;
use Validator;
use Response;
use Hash;
use Image;
use Paginator;
use Session;
use Redirect;
use App\User;
use App\UserType;
use App\Services;
use App\Contact;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\NewsRequest;

class ServicesController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get row set
        $result = $this->doList();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        
        $this->data['url'] = url("admin/news");
        $this->data['title'] = "Services";
        return view('services.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doList() {
      // sort and order
      // $result['sort'] = Request::input('s') ?: 'created_at';
      // $result['order'] = Request::input('o') ?: 'desc';
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 10;
      // dd(Request::all());
      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = Services::where('title', 'LIKE', '%' . $search . '%')->orderBy($result['sort'], $result['order'])->paginate($per);

      } else {
        $count = Services::where('title', 'LIKE', '%' . $search . '%')->orderBy($result['sort'], $result['order'])->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = Services::where('title', 'LIKE', '%' . $search . '%')->orderBy($result['sort'], $result['order'])->paginate($per);

      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->data['title'] = "Create News";
        return view('services.create', $this->data);
    }

    /**
     * Save model
     *
     */
    public function save() {    

        $input = Request::all();
        
        // assume this is a new row
        $new = true;

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = Services::where('status', 1)->find(array_get($input, 'id'));

            if(!$row) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }

            // this is an existing row
            $new = false;
        }

        $rules = array(
            'title' => 'required',
            'icon' => 'required',
            'excerpt' => 'required',
            'body' => 'required',
        );

        // field name overrides
        $names = array(
            'title' => 'news title',
            'body' => 'content',
        );

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if ($new) {
            $row = new Services;
        }

        $row->title     = array_get($input, 'title');
        $row->body      = array_get($input, 'body');
        $row->icon      = array_get($input, 'icon');
        $row->excerpt      = array_get($input, 'excerpt');
        $row->status    = 1;

        // save model
        $row->save();

        $row->slug           = str_slug(array_get($input, 'title') . ' ' . $row->id, '-');
        // save model
        $row->save();

        // return
        return Response::json(['body' => 'News successfully added.']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if (is_null(Services::find($id))) {
            return abort(404);
        } else {
            $this->data['services'] = Services::find($id);

            $this->data['title'] = $this->data['services']->title;

            return view('services.edit', $this->data);
        }
    }

    /**
     * Delete the user
     *
     */
    public function delete() {
        if (Request::input('id')) {
          $row = Services::find(Request::input('id'));

          // check if user exists
          if(!is_null($row)) {
            Services::destroy(Request::input('id'));

            // return
            return Response::json(['body' => 'Service has been deleted.']);
          } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
          }
        } else {
          // not found
          return Response::json(['error' => ["The requested item was not found in the database."]]);
        }
    }
 }
