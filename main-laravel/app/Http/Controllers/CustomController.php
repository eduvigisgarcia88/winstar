<?php

namespace App\Http\Controllers;

use Auth;
use Crypt;
use Input;
use Request;
use Validator;
use Response;
use Hash;
use Redirect;
use App\User;
use App\UserType;
use App\About;
use App\Contact;
use App\Header;
use App\Http\Requests;
use Session;
use Paginator;
use Image;

class CustomController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth');
    }

/**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // // get row set
        $result = $this->doList();

        // // assign vars to template
        // $this->data['services'] = $result['services'];
        $this->data['about'] = $result['about'];
        $this->data['contact'] = $result['contact'];
        // $this->data['header'] = $result['header'];

        // $contact_id = Contact::first();
        // $row = Contact::first();
        // $this->data['contact_id'] = $contact_id->id;

        // $this->data['url'] = url("admin/service");
        $this->data['title'] = "Custom Pages";
        return view('custom.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doList() {

        // $services = Services::orderBy('created_at', 'asc')->paginate(99);
        $about = About::orderBy('created_at', 'asc')->get();
        $contact = Contact::first();
        // $header = Header::orderBy('created_at', 'asc')->paginate(99);

        // return response (format accordingly)
        if(Request::ajax()) {
            // $result['services'] = $services->toArray();
            $result['about'] = $about->toArray();
            $result['contact'] = $contact->toArray();
            // $result['header'] = $header->toArray();
            return Response::json($result);
        } else {
            // $result['services'] = $services;
            $result['about'] = $about;
            $result['contact'] = $contact;
            // $result['header'] = $header;
            return $result;
        }
    }

    public function editAbout($id) {

        if (is_null(About::find($id))) {
            return abort(404);
        } else {
            $this->data['about'] = About::find($id);

            $this->data['title'] = 'Editing About Us';

            return view('custom.edit-about-us', $this->data);
        }
    }

    /**
     * Save model
     *
     */
    public function saveAbout() {    

        $input = Request::all();
        

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = About::where('status', 1)->find(array_get($input, 'id'));

            if(!$row) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
        } else {
            return Response::json(['error' => "The requested item was not found in the database."]);
        }

        $rules = array(
            'title' => 'required',
            'icon' => 'required',
            'excerpt' => 'required',
            'body' => 'required',
        );

        // field name overrides
        $names = array(
            'title' => 'news title',
            'body' => 'content',
        );

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        $row->title     = array_get($input, 'title');
        $row->body      = array_get($input, 'body');
        $row->icon      = array_get($input, 'icon');
        $row->excerpt      = array_get($input, 'excerpt');
        $row->status    = 1;

        // save model
        $row->save();

        // return
        return Response::json(['body' => 'Saved.']);
    }

    public function editContact() {

        if (is_null(Contact::first())) {
            return abort(404);
        } else {
            $this->data['contact'] = Contact::first();

            $this->data['title'] = 'Editing Contact Us';

            return view('custom.edit-contact-us', $this->data);
        }
    }


    /**
     * Save model
     *
     */
    public function saveContact() {    

        $input = Input::all();

        $new = true;
        
        $row = Contact::first();

        // check if an ID is passed
        if($row) {
        	$new = false;
        }

        $rules = array(
            'body' => 'required',
        );

        // field name overrides
        $names = array(
            'body' => 'content',
        );

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        
        if ($new) {
        	$row = new Contact;
        }

        $row->body     = array_get($input, 'body');

        $row->status    = 1;

        // save model
        $row->save();

        // return
        return Response::json(['body' => 'Saved.']);
    }

}