<?php

namespace App\Http\Controllers;

use Auth;
use Crypt;
use Input;
use Request;
use Validator;
use Response;
use Hash;
use Image;
use Paginator;
use Session;
use Redirect;
use App\User;
use App\Banner;
use App\UserType;
use App\News;
use App\Client;
use App\Services;
use App\About;
use App\Contact;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\NewsRequest;

class HomeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->data['services'] = Services::orderBy('created_at', 'asc')->get();
        $this->data['about'] = About::orderBy('created_at', 'asc')->get();
        $this->data['contact'] = Contact::first();
        $this->data['clients'] = Client::orderBy('created_at', 'asc')->get();
        $this->data['banners'] = Banner::where('status', 1)->orderBy('created_at', 'asc')->get();
        return view('frontend.home', $this->data);
    }

        public function viewContactus()
    {
        $this->data['services'] = Services::orderBy('created_at', 'asc')->get();
        $this->data['about'] = About::orderBy('created_at', 'asc')->get();
        $this->data['contact'] = Contact::first();
        $this->data['clients'] = Client::orderBy('created_at', 'asc')->get();
        return view('frontend.contact-us', $this->data);
    }
            public function viewNews()
    {
        // get row set
        $result = $this->doList();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        
        $this->data['url'] = url("admin/news");
        $this->data['title'] = "News";
        return view('frontend.news', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doList() {
        $this->data['contact'] = Contact::first();
        $this->data['news'] = News::orderBy('created_at', 'asc')->get();
        $this->data['clients'] = Client::orderBy('created_at', 'asc')->get();

      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 5;
      if (!Auth::check()) {

          // return response (format accordingly)
          if(Request::ajax()) {
              return Response::json(['relogin' => "Relogin"]);
          }
      }

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = News::where('title', 'LIKE', '%' . $search . '%')->orderBy($result['sort'], $result['order'])->paginate($per);

      } else {
        $count = News::where('title', 'LIKE', '%' . $search . '%')->orderBy($result['sort'], $result['order'])->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = News::where('title', 'LIKE', '%' . $search . '%')->orderBy($result['sort'], $result['order'])->paginate($per);

      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }

    public function viewAbout($slug)
    {  
       if (is_null(About::where('slug', '=', $slug)->first())) {
            return abort(404);
        }  
        $this->data['about'] = About::where('slug', '=', $slug)->first();
        $this->data['contact'] = Contact::first();
        $this->data['clients'] = Client::orderBy('created_at', 'asc')->get();
        $this->data['news'] = News::where('slug', '=', $slug)->first();
        return view('frontend.custom-page', $this->data);
    }
    public function viewServices($slug)
    {     
      if (is_null(Services::where('slug', '=', $slug)->first())) {
            return abort(404);
        } 
        $this->data['contact'] = Contact::first();
        $this->data['clients'] = Client::orderBy('created_at', 'asc')->get();
       
        $this->data['services'] = Services::where('slug', '=', $slug)->first();
        return view('frontend.custom-page', $this->data);
    }
     public function viewNewsreadmore($slug)
    {     
      if (is_null(News::where('slug', '=', $slug)->first())) {
            return abort(404);
        } 
        $this->data['about'] = About::where('slug', '=', $slug)->first();
        $this->data['contact'] = Contact::first();
        $this->data['clients'] = Client::orderBy('created_at', 'asc')->get();
        $this->data['news'] = News::where('slug', '=', $slug)->first();
        return view('frontend.custom-page', $this->data);
    }

}