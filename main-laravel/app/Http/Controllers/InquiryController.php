<?php

namespace App\Http\Controllers;

use Auth;
use Crypt;
use Input;
use Request;
use Validator;
use Response;
use Hash;
use Redirect;
use Paginator;
use Mail;
use Carbon\Carbon;
use App\User;
use App\UserType;
use App\Inquiry;
use App\Contact;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\InquiryRequest;

class InquiryController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth', ['except' => 'save']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get row set
        $result = $this->doList();

        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];

        // // get row set
        // $pending = $this->doListPending();

        // // get row set
        // $replied = $this->doListReplied();

        // // assign vars to template
        // $this->data['pending'] = $pending['rows'];
        // $this->data['pending_pages'] = $pending['pages'];

        // // assign vars to template
        // $this->data['replied'] = $replied['rows'];
        // $this->data['replied_pages'] = $replied['pages'];

        // $this->data['status'] = $result['status'];
        $this->data['title'] = "Inquiries";

        return view('inquiry.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doList() {
      $result['sort'] = Request::input('sort') ?: 'updated_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status') ?: 'Pending';
      $per = Request::input('per') ?: 10;
      // dd(Request::all());
      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = Inquiry::where(function($query) use ($search, $status) {
                        $query->where('name', 'LIKE', '%' . $search . '%')
                              ->where('status', 'LIKE', '%' . $status . '%');
                        })
                        ->orderBy($result['sort'], $result['order'])
                        ->paginate($per);
      } else {
        $count = Inquiry::where(function($query) use ($search, $status) {
                        $query->where('name', 'LIKE', '%' . $search . '%')
                              ->where('status', 'LIKE', '%' . $status . '%');
                        })
                        ->orderBy($result['sort'], $result['order'])
                        ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = Inquiry::where(function($query) use ($search, $status) {
                        $query->where('name', 'LIKE', '%' . $search . '%')
                              ->where('status', 'LIKE', '%' . $status . '%');
                        })
                        ->orderBy($result['sort'], $result['order'])
                        ->paginate($per);
      }
      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }

    // /**
    //  * Build the list
    //  *
    //  */
    // public function doListPending() {
    //     // sort and order
    //     $result['sort'] = Request::input('s') ?: 'created_at';
    //     $result['order'] = Request::input('o') ?: 'desc';

    //     $rows = Inquiry::where('status', '=', 'Pending')->orderBy($result['sort'], $result['order'])->paginate(1);

    //     // return response (format accordingly)
    //     if(Request::ajax()) {
    //         $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
    //         $result['rows'] = $rows->toArray();
    //         return Response::json($result);
    //     } else {
    //         $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
    //         $result['rows'] = $rows;
    //         return $result;
    //     }
    // }

    // /**
    //  * Build the list
    //  *
    //  */
    // public function doListReplied() {
    //     // sort and order
    //     $result['sort'] = Request::input('s') ?: 'created_at';
    //     $result['order'] = Request::input('o') ?: 'desc';

    //     $rows = Inquiry::where('status', '=', 'Replied')->orderBy($result['sort'], $result['order'])->paginate(1);

    //     // return response (format accordingly)
    //     if(Request::ajax()) {
    //         $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
    //         $result['rows'] = $rows->toArray();
    //         return Response::json($result);
    //     } else {
    //         $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
    //         $result['rows'] = $rows;
    //         return $result;
    //     }
    // }

    /**
     * Save model
     *
     */
    public function save() {    
        // dd(Request::all());
        $input = Request::all();

        $rules = array(
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'body' => 'required',
        );

        // field name overrides
        $names = array(
            'phone' => 'phone no.',
        );

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        $row = new Inquiry;

        $row->name      = array_get($input, 'name');
        $row->email     = array_get($input, 'email');
        $row->phone     = array_get($input, 'phone');
        $row->body      = array_get($input, 'body');
        $row->status    = 'Pending';
        // save model
        $row->save();
        
        $row->tracking_no = 'WLC-INQ-' . sprintf("%09d", $row->id);
        
        $row->save();

        $inquiry = Inquiry::findOrFail($row->id);
        $contact = Contact::first();

        Mail::send('emails.inquire', ['inquiry' => $inquiry, 'contact' => $contact], function($message) use ($inquiry) {
            $message->to($inquiry->email, $inquiry->name)->subject('Inquiry Received (Tracking No: ' . $inquiry->tracking_no . ')');
        });

        $mail = [];

        $get_user_mail = User::first();

        $mail[0] = $get_user_mail->email;
        $mail[1] = $get_user_mail->email_inq;

        $emails = explode(",", $get_user_mail->inquiries);

        $ctr = 2;
        foreach ($emails as $key => $value) {
          $mail[$ctr] = $value;
          $ctr += 1;
        }

        foreach ($mail as $mkey => $mvalue) {
          Mail::send('emails.inquire-system', ['inquiry' => $inquiry, 'row' => $row], function($message) use ($inquiry, $mvalue) {
            $message->to($mvalue, 'Winstar Loans and Credits')->subject('Customer Inquiry Received');
          });
        }

        // return
        return Response::json(['body' => 'Inquiry successfully submitted.']);

    }


    /**
     * Retrieve user information
     *
     */
    public function view() {
        $id = Request::input('id');

        // prevent non-admin from viewing deactivated row
        $row = Inquiry::find($id);

        if($row) {
            return Response::json($row);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Delete the inquiry
     *
     */
    public function delete() {
        $row = Inquiry::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            Inquiry::destroy(Request::input('id'));

            // return
            return Response::json(['body' => 'Inquiry has been deleted.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Delete the inquiry
     *
     */
    public function replied() {
      if (Request::input('id')) {
        $row = Inquiry::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            $row->status = "Replied";
            $row->save();
            
            $inquiry = Inquiry::findOrFail($row->id);
            $contact = Contact::first();

            Mail::send('emails.reply', ['inquiry' => $inquiry, 'contact' => $contact], function($message) use ($inquiry) {
                $message->to($inquiry->email, $inquiry->name)->subject('Inquiry Replied (Tracking No: ' . $inquiry->tracking_no . ')');
            });

            $mail = [];

            $get_user_mail = User::first();

            $mail[0] = $get_user_mail->email;
            $mail[1] = $get_user_mail->email_inq;

            $emails = explode(",", $get_user_mail->inquiries);

            $ctr = 2;
            foreach ($emails as $key => $value) {
              $mail[$ctr] = $value;
              $ctr += 1;
            }

            foreach ($mail as $mkey => $mvalue) {
              Mail::send('emails.reply-system', ['inquiry' => $inquiry, 'row' => $row], function($message) use ($inquiry, $mvalue) {
                $message->to($mvalue, 'Winstar Loans and Credits')->subject('Replied Inquiry Details');
              });
            }

            // return
            return Response::json(['body' => 'Inquiry has been set to replied.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
      } else {
            // not found
            return Response::json(['error' => ["The requested item was not found in the database."]]);
      }
    }

    /**
     * Finish the inquiry
     *
     */
    public function finish() {
        $row = Inquiry::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            $row->status = "Finished";
            $row->save();

            $inquiry = Inquiry::findOrFail($row->id);
            $contact = Contact::first();

            Mail::send('emails.finish', ['inquiry' => $inquiry, 'contact' => $contact], function($message) use ($inquiry, $contact) {
                $message->to($inquiry->email, $inquiry->name)->subject('Client Booking Finished');
            });

            $user = User::select('email')->get();

            foreach ($user as $row) {
                Mail::send('emails.finish-system', ['inquiry' => $inquiry, 'row' => $row], function($message) use ($inquiry, $row) {
                    $message->to($row->email, $row->name)->subject('Booking Details Finished');
                });
            }

            // return
            return Response::json(['body' => 'Inquiry has been set to finished.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Cancel the inquiry
     *
     */
    public function cancel() {
        $row = Inquiry::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            $row->status = "Cancelled";
            $row->save();

            $inquiry = Inquiry::findOrFail($row->id);
            $contact = Contact::first();

            Mail::send('emails.cancel', ['inquiry' => $inquiry, 'contact' => $contact], function($message) use ($inquiry, $contact) {
                $message->to($inquiry->email, $inquiry->name)->subject('Client Booking Cancelled');
            });

            $user = User::select('email')->get();

            foreach ($user as $row) {
                Mail::send('emails.cancel-system', ['inquiry' => $inquiry, 'row' => $row], function($message) use ($inquiry, $row) {
                    $message->to($row->email, $row->name)->subject('Client Booking Cancelled');
                });
            }

            // return
            return Response::json(['body' => 'Inquiry has been cancelled.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Cancel the inquiry
     *
     */
    public function book() {
        $row = Inquiry::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {

            $rules = array(
                'date_sched' => 'required'
            );

            // field name overrides
            $names = array(
                'date_sched' => 'date',
            );

            // do validation
            $validator = Validator::make(Input::all(), $rules);
            $validator->setAttributeNames($names); 

            // return errors
            if($validator->fails()) {
                return Response::json(['error' => array_unique($validator->errors()->all())]);
            }

            $row->date_sched = Carbon::createFromFormat('m/d/Y h:i A', Request::input('date_sched'));
            $row->status = "Scheduled";
            $row->save();

            $inquiry = Inquiry::findOrFail($row->id);
            $contact = Contact::first();

            Mail::send('emails.book', ['inquiry' => $inquiry, 'contact' => $contact], function($message) use ($inquiry, $contact) {
                $message->to($inquiry->email, $inquiry->name)->subject('Client Booking Scheduled');
            });

            $user = User::select('email')->get();

            foreach ($user as $row) {
                Mail::send('emails.book-system', ['inquiry' => $inquiry, 'row' => $row], function($message) use ($inquiry, $row) {
                    $message->to($row->email, $row->name)->subject('Booking Details Scheduled');
                });
            }

            // return
            return Response::json(['body' => 'Inquiry has been scheduled.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

}
