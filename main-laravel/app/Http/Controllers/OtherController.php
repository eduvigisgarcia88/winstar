<?php

namespace App\Http\Controllers;

use Auth;
use Crypt;
use Input;
use Request;
use Validator;
use Response;
use Hash;
use Redirect;
use App\User;
use App\UserType;
use App\Services;
use App\About;
use App\Contact;
use App\Header;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ServiceRequest;
use App\Http\Requests\AboutRequest;
use App\Http\Requests\ContactRequest;
use App\Http\Requests\HeaderRequest;
use Session;
use Paginator;
use Image;

class OtherController extends Controller
{
    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get row set
        $result = $this->doList();

        // assign vars to template
        $this->data['services'] = $result['services'];
        $this->data['about'] = $result['about'];
        $this->data['contact'] = $result['contact'];
        $this->data['header'] = $result['header'];

        $contact_id = Contact::first();
        $row = Contact::first();
        $this->data['contact_id'] = $contact_id->id;

        $this->data['url'] = url("admin/service");
        $this->data['title'] = "Services";
        $this->data['refresh_route'] = url('admin/ads/refresh');
        return view('other.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doList() {

        $services = Services::orderBy('created_at', 'asc')->paginate(99);
        $about = About::orderBy('created_at', 'asc')->paginate(99);
        $contact = Contact::orderBy('created_at', 'asc')->paginate(99);
        $header = Header::orderBy('created_at', 'asc')->paginate(99);

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['services'] = $services->toArray();
            $result['about'] = $about->toArray();
            $result['contact'] = $contact->toArray();
            $result['header'] = $header->toArray();
            return Response::json($result);
        } else {
            $result['services'] = $services;
            $result['about'] = $about;
            $result['contact'] = $contact;
            $result['header'] = $header;
            return $result;
        }
    }

    /**
     * Show the form for editing the specified service.
     *
     * @param  int  $id
     * @return Response
     */
    public function editService($id)
    {
        if (is_null(Services::find($id))) {
            return abort(404);
        } else {
            $this->data['service'] = Services::find($id);

            $this->data['title'] = 'Editing Services';

            return view('other.edit-service', $this->data);
        }
    }

    /**
     * Update the specified service in storage.
     *
     * @param  ServiceRequest  $request
     * @param  int  $id
     * @return Response
     */
    public function updateService(ServiceRequest $request, $id)
    {
        $input = Input::all();

        $row = Services::find($id);
        $row->icon         = array_get($input, 'icon');
        $row->title_eng     = array_get($input, 'title_eng');
        $row->desc_eng      = array_get($input, 'desc_eng');
        $row->title_arabic  = array_get($input, 'title_arabic');
        $row->desc_arabic   = array_get($input, 'desc_arabic');

        // save model
        $row->save();

        $url = url('/');

        return redirect('config/other')
                ->with('notice', array(
                        'msg' => 'Service successfully updated.',
                        'url' => $url,
                        'type' => 'success'
                    ));
    }

    /**
     * Show the form for editing the specified service.
     *
     * @param  int  $id
     * @return Response
     */
    public function editAbout($id)
    {
        if (is_null(About::find($id))) {
            return abort(404);
        } else {
            $this->data['about'] = About::find($id);

            $this->data['title'] = 'Editing About Us';

            return view('other.edit-about', $this->data);
        }
    }

    /**
     * Update the specified service in storage.
     *
     * @param  ServiceRequest  $request
     * @param  int  $id
     * @return Response
     */
    public function updateAbout(AboutRequest $request, $id)
    {
        $input = Input::all();

        $row = About::find($id);
        $row->desc_eng      = array_get($input, 'desc_eng');
        $row->desc_arabic   = array_get($input, 'desc_arabic');

        // save model
        $row->save();

        $url = url('/');

        return redirect('config/other')
                ->with('notice', array(
                        'msg' => 'Description successfully updated.',
                        'url' => $url,
                        'type' => 'success'
                    ));
    }

    /**
     * Show the form for editing the specified service.
     *
     * @param  int  $id
     * @return Response
     */
    public function editContact($id)
    {
        if (is_null(Contact::find($id))) {
            return abort(404);
        } else {
            $this->data['contact'] = Contact::find($id);

            $this->data['title'] = "Editing Contact Information";

            return view('other.edit-contact', $this->data);
        }
    }

    /**
     * Update the specified service in storage.
     *
     * @param  ServiceRequest  $request
     * @param  int  $id
     * @return Response
     */
    public function updateContact(ContactRequest $request, $id)
    {
        $input = Input::all();

        $row = Contact::find($id);
        $row->address_eng      = array_get($input, 'address_eng');
        $row->address_arabic   = array_get($input, 'address_arabic');
        $row->phone            = array_get($input, 'phone');
        $row->email            = array_get($input, 'email');
        $row->clinic_eng       = array_get($input, 'clinic_eng');
        $row->clinic_arabic    = array_get($input, 'clinic_arabic');
        $row->facebook_link    = array_get($input, 'facebook_link');
        $row->twitter_link     = array_get($input, 'twitter_link');

        // save model
        $row->save();

        $url = url('/');

        return redirect('config/other')
                ->with('notice', array(
                        'msg' => 'Contact Information successfully updated.',
                        'url' => $url,
                        'type' => 'success'
                    ));
    }

    /**
     * Show the form for editing the specified service.
     *
     * @param  int  $id
     * @return Response
     */
    public function editHeader($id)
    {
        if (is_null(Header::find($id))) {
            return abort(404);
        } else {
            $this->data['header'] = Header::find($id);

            $this->data['title'] = "Editing Header";

            return view('other.edit-header', $this->data);
        }
    }

    /**
     * Update the specified service in storage.
     *
     * @param  ServiceRequest  $request
     * @param  int  $id
     * @return Response
     */
    public function updateHeader(HeaderRequest $request, $id)
    {
        $input = Input::all();

        $row = Header::find($id);
        $row->title_eng     = array_get($input, 'title_eng');
        $row->desc_eng      = array_get($input, 'desc_eng');
        $row->title_arabic  = array_get($input, 'title_arabic');
        $row->desc_arabic   = array_get($input, 'desc_arabic');

        // save model
        $row->save();

        $url = url('/');

        return redirect('config/other')
                ->with('notice', array(
                        'msg' => 'Header successfully updated.',
                        'url' => $url,
                        'type' => 'success'
                    ));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
