<?php

namespace App\Http\Controllers;

use Auth;
use Crypt;
use Input;
use Request;
use Validator;
use Response;
use Hash;
use Image;
use Redirect;
use Mail;
use Paginator;
use Carbon\Carbon;
use App\Client;
use App\User;
use App\UserType;
use App\Inquiry;
use App\Contact;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\InquiryRequest;

class ClientController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get row set
        $result = $this->doList();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];

        $this->data['title'] = "Clients";
        
        return view('clients.list', $this->data);
    }

    public function doList() {
    	$result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 10;
      // dd(Request::all());
      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = Client::where('title', 'LIKE', '%' . $search . '%')->orderBy($result['sort'], $result['order'])->paginate($per);

      } else {
        $count = Client::where('title', 'LIKE', '%' . $search . '%')->orderBy($result['sort'], $result['order'])->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = Client::where('title', 'LIKE', '%' . $search . '%')->orderBy($result['sort'], $result['order'])->paginate($per);

      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }

    public function edit()
    {
        $id = Request::input('id');
        if ($id) {
	        $row = Client::find($id);

	        if($row) {
	            return Response::json($row);
	        } else {
	            return Response::json(['error' => "Invalid row specified"]);
	        }
        } else {
					return Response::json(['error' => "Invalid row specified"]);
        }
    }

    /**
     * Save model
     *
     */
    public function save() {    

        // assume this is a new row
        $new = true;

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = Client::where('status', 1)->find(array_get($input, 'id'));

            if(!$row) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }

            // this is an existing row
            $new = false;
        }

        $rules = array(
        		'photo' => $new ? 'required' : '',
            'title' => 'required',
            'url' => 'required',
        );

        // field name overrides
        $names = array(
            'title' => 'title',
            'url' => 'url',
        );

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if ($new) {
            $row = new Client;
        }

        $row->title     = array_get($input, 'title');
        $row->url      = array_get($input, 'url');
        $row->status    = 1;

        // save model
        $row->save();

        if (Request::hasFile('photo')) {
          // Save the photo
          Image::make(Request::file('photo'))->fit(104, 40)->save('uploads/thumbnails/clients/' . $row->id . '.png');
          Image::make(Request::file('photo'))->fit(240, 92)->save('uploads/clients/' . $row->id . '.png');
          $row->photo = $row->id . '.png';
        }

        // save model
        $row->save();

        // return
        if ($new) {
          return Response::json(['body' => 'Client successfully added.']);
        } else {
          return Response::json(['body' => 'Client successfully updated.']);
        }
    }


    /**
     * Delete the user
     *
     */
    public function delete() {
        if (Request::input('id')) {
          $row = Client::find(Request::input('id'));

          // check if user exists
          if(!is_null($row)) {
            Client::destroy(Request::input('id'));

            // return
            return Response::json(['body' => 'Client has been deleted.']);
          } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
          }
        } else {
          // not found
          return Response::json(['error' => ["The requested item was not found in the database."]]);
        }
    }
}