<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Inquiry;
use App\Promo;
use App\Profile;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services;
use App\Application;

class BackendController extends Controller
{
    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->data['title'] = "Dashboard";
        $this->data['title_arabic'] = "لوحة القيادة";

        $this->data['inquiries'] = Inquiry::take(5)->orderBy('created_at', 'desc')->get();
        $this->data['applications'] = Application::take(5)->orderBy('created_at', 'desc')->get();
        $this->data['news'] = News::take(5)->orderBy('updated_at', 'desc')->get();
        $this->data['services'] = Services::take(5)->orderBy('created_at', 'desc')->get();

        return view('dashboard.home', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
