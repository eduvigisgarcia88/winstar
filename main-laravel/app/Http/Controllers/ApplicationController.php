<?php

namespace App\Http\Controllers;

use Auth;
use Crypt;
use Input;
use Request;
use Validator;
use Response;
use Hash;
use Redirect;
use Paginator;
use Mail;
use Carbon\Carbon;
use App\User;
use App\UserType;
use App\Application;
use App\Contact;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ApplicationRequest;

class ApplicationController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth', ['except' => 'save']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get row set
        $result = $this->doList();

        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];

        // // get row set
        // $pending = $this->doListPending();

        // // get row set
        // $replied = $this->doListReplied();

        // // assign vars to template
        // $this->data['pending'] = $pending['rows'];
        // $this->data['pending_pages'] = $pending['pages'];

        // // assign vars to template
        // $this->data['replied'] = $replied['rows'];
        // $this->data['replied_pages'] = $replied['pages'];

        // $this->data['status'] = $result['status'];
        $this->data['title'] = "Applications";

        return view('application.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doList() {
      $result['sort'] = Request::input('sort') ?: 'updated_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status') ?: 'Pending';
      $per = Request::input('per') ?: 10;
      // dd(Request::all());
      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = Application::where(function($query) use ($search, $status) {
                        $query->where('name', 'LIKE', '%' . $search . '%')
                              ->where('status', 'LIKE', '%' . $status . '%');
                        })
                        ->orderBy($result['sort'], $result['order'])
                        ->paginate($per);
      } else {
        $count = Application::where(function($query) use ($search, $status) {
                        $query->where('name', 'LIKE', '%' . $search . '%')
                              ->where('status', 'LIKE', '%' . $status . '%');
                        })
                        ->orderBy($result['sort'], $result['order'])
                        ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = Application::where(function($query) use ($search, $status) {
                        $query->where('name', 'LIKE', '%' . $search . '%')
                              ->where('status', 'LIKE', '%' . $status . '%');
                        })
                        ->orderBy($result['sort'], $result['order'])
                        ->paginate($per);
      }
      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }

    // /**
    //  * Build the list
    //  *
    //  */
    // public function doListPending() {
    //     // sort and order
    //     $result['sort'] = Request::input('s') ?: 'created_at';
    //     $result['order'] = Request::input('o') ?: 'desc';

    //     $rows = Application::where('status', '=', 'Pending')->orderBy($result['sort'], $result['order'])->paginate(1);

    //     // return response (format accordingly)
    //     if(Request::ajax()) {
    //         $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
    //         $result['rows'] = $rows->toArray();
    //         return Response::json($result);
    //     } else {
    //         $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
    //         $result['rows'] = $rows;
    //         return $result;
    //     }
    // }

    // /**
    //  * Build the list
    //  *
    //  */
    // public function doListReplied() {
    //     // sort and order
    //     $result['sort'] = Request::input('s') ?: 'created_at';
    //     $result['order'] = Request::input('o') ?: 'desc';

    //     $rows = Application::where('status', '=', 'Replied')->orderBy($result['sort'], $result['order'])->paginate(1);

    //     // return response (format accordingly)
    //     if(Request::ajax()) {
    //         $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
    //         $result['rows'] = $rows->toArray();
    //         return Response::json($result);
    //     } else {
    //         $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
    //         $result['rows'] = $rows;
    //         return $result;
    //     }
    // }

    /**
     * Save model
     *
     */
    public function save() {
        $input = Request::all();

        // dd(array_get($input, 'pref_sched'));
        $rules = array(
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            // 'body' => 'required',
            'others' => array_get($input, 'others_loan') ? 'required' : '',
            'dob' => 'required|date',
            'address' => 'required',
            'pref_sched' => 'required|date',
        );

        // field name overrides
        $names = array(
            'phone' => 'phone no.',
            'dob' => 'date of birth',
            'pref_sched' => 'preferred schedule',

        );

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if (!array_get($input, 'commercial_loan') && !array_get($input, 'consumer_loan') && !array_get($input, 'personal_loan') && !array_get($input, 'salary_loan') && !array_get($input, 'car_loan') && !array_get($input, 'others_loan')) {
          return Response::json(['error' => ['Select type of loan.']]);
        }

        // dd($input);

        $row = new Application;

        $row->name      = array_get($input, 'name');
        $row->email     = array_get($input, 'email');
        $row->phone     = array_get($input, 'phone');
        $row->commercial_loan      = array_get($input, 'commercial_loan');
        $row->consumer_loan      = array_get($input, 'consumer_loan');
        $row->personal_loan      = array_get($input, 'personal_loan');
        $row->salary_loan      = array_get($input, 'salary_loan');
        $row->car_loan      = array_get($input, 'car_loan');
        
        if (array_get($input, 'others_loan' )) {
          $row->others      = array_get($input, 'others');
        }

        $row->dob      = Carbon::createFromFormat('m/d/Y', array_get($input, 'dob'));
        $row->address      = array_get($input, 'address');
        $row->pref_sched      = Carbon::createFromFormat('m/d/Y h:i A', array_get($input, 'pref_sched'));
        $row->status    = 'Pending';
        // save model
        $row->save();


        $row->tracking_no = 'WLC-APP-' . sprintf("%09d", $row->id);

        $row->save();

        $inquiry = Application::findOrFail($row->id);
        $contact = Contact::first();

        Mail::send('emails.apply', ['inquiry' => $inquiry, 'contact' => $contact], function($message) use ($inquiry) {
            $message->to($inquiry->email, $inquiry->name)->subject('Application Received (Tracking No: ' . $inquiry->tracking_no . ')');
        });

        $mail = [];

        $get_user_mail = User::first();

        $mail[0] = $get_user_mail->email;
        $mail[1] = $get_user_mail->email_app;

        $emails = explode(",", $get_user_mail->applications);

        $ctr = 2;
        foreach ($emails as $key => $value) {
          $mail[$ctr] = $value;
          $ctr += 1;
        }

        foreach ($mail as $mkey => $mvalue) {
          Mail::send('emails.apply-system', ['inquiry' => $inquiry, 'row' => $row], function($message) use ($inquiry, $mvalue) {
            $message->to($mvalue, 'Winstar Loans and Credits')->subject('Application Details Received');
          });
        }

        // return
        return Response::json(['body' => 'Application successfully submitted.']);

    }


    /**
     * Retrieve user information
     *
     */
    public function view() {
        $id = Request::input('id');

        // prevent non-admin from viewing deactivated row
        $row = Application::find($id);

        if($row) {
            return Response::json($row);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Delete the inquiry
     *
     */
    public function delete() {
        $row = Application::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            Application::destroy(Request::input('id'));

            // return
            return Response::json(['body' => 'Application has been deleted.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Finish the inquiry
     *
     */
    public function finish() {
        $row = Application::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            $row->status = "Finished";
            $row->save();

            $inquiry = Application::findOrFail($row->id);
            $contact = Contact::first();

            Mail::send('emails.finish', ['inquiry' => $inquiry, 'contact' => $contact], function($message) use ($inquiry) {
                $message->to($inquiry->email, $inquiry->name)->subject('Application Finished (Tracking No: ' . $inquiry->tracking_no . ')');
            });

            $mail = [];

            $get_user_mail = User::first();

            $mail[0] = $get_user_mail->email;
            $mail[1] = $get_user_mail->email_app;

            $emails = explode(",", $get_user_mail->applications);

            $ctr = 2;
            foreach ($emails as $key => $value) {
              $mail[$ctr] = $value;
              $ctr += 1;
            }

            foreach ($mail as $mkey => $mvalue) {
              Mail::send('emails.finish-system', ['inquiry' => $inquiry, 'row' => $row], function($message) use ($inquiry, $mvalue) {
                $message->to($mvalue, 'Winstar Loans and Credits')->subject('Customer Application Finished');
              });
            }

            // return
            return Response::json(['body' => 'Application has been set to finished.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Cancel the inquiry
     *
     */
    public function cancel() {
        $row = Application::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            $row->status = "Cancelled";
            $row->save();

            $inquiry = Application::findOrFail($row->id);
            $contact = Contact::first();

            Mail::send('emails.cancel', ['inquiry' => $inquiry, 'contact' => $contact], function($message) use ($inquiry) {
                $message->to($inquiry->email, $inquiry->name)->subject('Application Cancelled (Tracking No: ' . $inquiry->tracking_no . ')');
            });

            $mail = [];

            $get_user_mail = User::first();

            $mail[0] = $get_user_mail->email;
            $mail[1] = $get_user_mail->email_app;

            $emails = explode(",", $get_user_mail->applications);

            $ctr = 2;
            foreach ($emails as $key => $value) {
              $mail[$ctr] = $value;
              $ctr += 1;
            }

            foreach ($mail as $mkey => $mvalue) {
              Mail::send('emails.cancel-system', ['inquiry' => $inquiry, 'row' => $row], function($message) use ($inquiry, $mvalue) {
                $message->to($mvalue, 'Winstar Loans and Credits')->subject('Customer Application Cancelled');
              });
            }

            // return
            return Response::json(['body' => 'Application has been cancelled.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

    /**
     * Cancel the inquiry
     *
     */
    public function schedule() {
        $row = Application::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {

            $rules = array(
                'date_sched' => 'required'
            );

            // field name overrides
            $names = array(
                'date_sched' => 'date',
            );

            // do validation
            $validator = Validator::make(Input::all(), $rules);
            $validator->setAttributeNames($names); 

            // return errors
            if($validator->fails()) {
                return Response::json(['error' => array_unique($validator->errors()->all())]);
            }

            $row->date_sched = Carbon::createFromFormat('m/d/Y h:i A', Request::input('date_sched'));
            $row->status = "Scheduled";
            $row->save();

            $inquiry = Application::findOrFail($row->id);
            $contact = Contact::first();

            Mail::send('emails.schedule', ['inquiry' => $inquiry, 'contact' => $contact], function($message) use ($inquiry) {
                $message->to($inquiry->email, $inquiry->name)->subject('Scheduled Application (Tracking No: ' . $inquiry->tracking_no . ')');
            });

            $mail = [];

            $get_user_mail = User::first();

            $mail[0] = $get_user_mail->email;
            $mail[1] = $get_user_mail->email_app;

            $emails = explode(",", $get_user_mail->applications);

            $ctr = 2;
            foreach ($emails as $key => $value) {
              $mail[$ctr] = $value;
              $ctr += 1;
            }

            foreach ($mail as $mkey => $mvalue) {
              Mail::send('emails.schedule-system', ['inquiry' => $inquiry, 'row' => $row], function($message) use ($inquiry, $mvalue) {
                $message->to($mvalue, 'Winstar Loans and Credits')->subject('Application Details Scheduled');
              });
            }

            // return
            return Response::json(['body' => 'Application has been scheduled.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

}
