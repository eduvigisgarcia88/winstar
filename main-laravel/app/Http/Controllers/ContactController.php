<?php

namespace App\Http\Controllers;

use Auth;
use Crypt;
use Input;
use Request;
use Validator;
use Response;
use Hash;
use Redirect;
use App\User;
use App\UserType;
use App\About;
use App\Contact;
use App\Header;
use App\Http\Requests;
use Session;
use Paginator;
use Image;

class ContactController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth');
    }

/**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get row set
        $result = $this->doList();

        // assign vars to template
        $this->data['contact'] = $result['contact'];
        $this->data['title'] = "Contact Information";
        return view('contact.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doList() {

        $contact = Contact::first();

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['contact'] = $contact->toArray();
            return Response::json($result);
        } else {
            $result['contact'] = $contact;
            return $result;
        }
    }


    public function edit() {

        if (is_null(Contact::first())) {
            return abort(404);
        } else {
            $this->data['contact'] = Contact::first();

            $this->data['title'] = 'Editing Contact Us';

            return view('contact.edit', $this->data);
        }
    }


    /**
     * Save model
     *
     */
    public function save() {    

        $input = Input::all();

        $new = true;
        
        $row = Contact::first();

        // check if an ID is passed
        if($row) {
            $new = false;
        }

        $rules = array(
            'address' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'office_hours' => 'required',
            'facebook_link' => 'required',
            'twitter_link' => 'required',
        );

        // field name overrides
        $names = array(
            'twitter_link' => 'twitter link',
        );

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        
        if ($new) {
            $row = new Contact;
        }

        $row->address     = array_get($input, 'address');
        $row->phone     = array_get($input, 'phone');
        $row->email     = array_get($input, 'email');
        $row->office_hours     = array_get($input, 'office_hours');
        $row->facebook_link     = array_get($input, 'facebook_link');
        $row->twitter_link     = array_get($input, 'twitter_link');

        $row->status    = 1;

        // save model
        $row->save();

        // return
        return Response::json(['body' => 'Saved.']);
    }


}
