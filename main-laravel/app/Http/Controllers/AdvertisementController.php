<?php

namespace App\Http\Controllers;


use App\Advertisement;
use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\Category;
use App\Http\Controllers\Controller;
use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Image;

class AdvertisementController extends Controller
{
   public function index(){

    $result = $this->doList();
      
      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['refresh_route'] = url('admin/ads/refresh');
      $this->data['title'] = "Advertisement Management";
      return View::make('admin.ads-management.front-end-users.list', $this->data);
   }

    public function edit(){
     $row = Advertisement::find(Request::input('id'));


     if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }


   }
   public function doList() {
        
      
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 5;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = Advertisement::select('advertisements.*')
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('status', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
        //dd($rows);
      } else {
        $count = Advertisement::where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                  })
                                //->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = Advertisement::select('advertisements.*')
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

    }

public function save(){

        $new = true;
        $input = Input::all();
        
        // $file  = Request::hasFile('photo');
        $file  = Request::file('image');
        dd($file);
        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = Advertisement::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [
  
            'title' => 'required|min:2|max:100',
        ];
        // field name overrides
        $names = [
            'title' => 'category name',
        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        // create model if new
        if($new) {
       $row = new Advertisement;
      
        $row->category   = array_get($input, 'category');
        
        $row->title   = array_get($input, 'title');
        $row->body    = array_get($input, 'body');
        $row->user_id = Auth::user()->id;
        $row->save();
        
         $file               = array_get($input, 'photo');
        $destinationPath    = 'uploads/ads';
        $extension          = $file->getClientOriginalExtension();
        $fileName           = $row->id . '.' . $extension;
        $upload_success     = $file->move($destinationPath, $fileName);

        if ($upload_success) {
            // create thumbnail
            $img = Image::make('uploads/ads/' . $fileName);
            $img->fit(125);
            $img->save('uploads/ads/thumbnail/' . $fileName);

            $photo          = Advertisement::find($row->id);
            $photo->photo   = $row->id . '.' . $extension;
            $photo->save();
        }

        $url = url('news/view/' . $row->id);

        // return error
        return redirect()->action('AdvetisementController@index')
                       ->with('notice', array(
                            'msg' => 'News successfully added.',
                            'url' => $url,
                            'type' => 'success'
                       ));

      
   
       
        } else {
        //$row->image   = $fileName;
        $row->category   = array_get($input, 'category');
        $row->title   = array_get($input, 'title');
        $row->body    = array_get($input, 'body');
        $row->user_id = Auth::user()->id;
        $row->save();
        }   
      $row->save();
      return Response::json(['body' => 'Ad created successfully.']);
}


    public function doSave() {
    	$this->data['title'] = "Ads Management";
    	$new = true;
      $input = Input::all();
    	
        // getting all of the post data
  $file = array('image' => array_get($input, 'image'));
  // setting up rules
  $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
  // doing the validation, passing post data, rules and the messages
  $validator = Validator::make($file, $rules);
  if ($validator->fails()) {
    // send back to the page with the input data and errors
    return Redirect::to('admin/ads')->withInput()->withErrors($validator);
  }
  else {
    // checking file is valid.
    $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
    if ($extension=='jpg' OR $extension=='png') {
      $destinationPath = 'uploads'; // upload path
      $fileName = rand(11111,99999).'.'.$extension; // renameing image
      Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
      // sending back with message
      Session::flash('success', 'Upload successfully');

      	$row = new Advertisement;
		$row->image   = $fileName;
		$row->category   = array_get($input, 'category');
        $row->title   = array_get($input, 'title');
        $row->body    = array_get($input, 'body');
        $row->user_id = Auth::user()->id;
        $row->save();
        
      return Redirect::to('admin/ads');
    }
    else {
      // sending back with error message.
      Session::flash('error', 'uploaded file is not valid');
      return Redirect::to('admin/ads');
    }
  }
    $this->data['ads'] = Advertisement::join('users', 'advertisements.user_id', '=', 'users.id')
   						   ->select('users.*','advertisements.*')->get();
                             
    return View::make('admin.ads-management.front-end-users.list', $this->data);
    }

    public function viewAds (){

      $id = Request::input('id');
      $row = Advertisement::with('user')->find($id);

        if($row) {
            return Response::json($row);
        } else {
            return Response::json(['error' => "Invalid row specified"]);
        }
    }

public function editor ()
{
   return View::make('editor.index');
}

public function sub2editor ()
{
   return View::make('editor.sub1.sub2.index');
}

public function formeditor ()
{
   return View::make('admin.ads-management.front-end-users.form');
}

}