<?php

namespace App\Http\Controllers;


use App\Advertisement;
use Auth;
use DB;
use View;
use App\Loan;
use App\LoanAmortization;
use App\Usertypes;
use App\Category;
use App\Http\Controllers\Controller;
use Hash;
use Validator;
use Carbon\Carbon;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Excel;
use Image;

class LoanController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth', ['except' => ['view', 'all']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get row set
        $result = $this->doList();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        
        $this->data['url'] = url("admin/news");
        $this->data['title'] = "Loan Amortization";
        return view('loans.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doList() {
      // sort and order
      // $result['sort'] = Request::input('s') ?: 'created_at';
      // $result['order'] = Request::input('o') ?: 'desc';
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 10;
      // dd(Request::all());
      if (!Auth::check()) {

          // return response (format accordingly)
          if(Request::ajax()) {
              return Response::json(['relogin' => "Relogin"]);
          }
      }

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = Loan::with('getLoanAmortization')->where(function($query) use ($search) {
					        		$query->where('name', 'LIKE', '%' . $search . '%')
                            ->orWhere('account_no', 'LIKE', '%' . $search . '%');
					        })
                  ->where('status', 1)
        					->orderBy($result['sort'], $result['order'])->paginate($per);

      } else {
        $count = Loan::where(function($query) use ($search) {
                      $query->where('name', 'LIKE', '%' . $search . '%')
                            ->orWhere('account_no', 'LIKE', '%' . $search . '%');
                  })
                  ->where('status', 1)
                  ->orderBy($result['sort'], $result['order'])->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = Loan::with('getLoanAmortization')->where(function($query) use ($search) {
                      $query->where('name', 'LIKE', '%' . $search . '%')
                            ->orWhere('account_no', 'LIKE', '%' . $search . '%');
                  })
                  ->where('status', 1)
                  ->orderBy($result['sort'], $result['order'])->paginate($per);

      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }


    /**
     * Save model
     *
     */
    public function save() {    

        // assume this is a new row
        $new = true;

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = Loan::where('status', 1)->find(array_get($input, 'id'));

            if(!$row) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }

            // this is an existing row
            $new = false;
        }

        $rules = array(
            'account_no' => 'required|unique:loans,account_no' . (!$new ? ',' . $row->id : ''),
            'name' => 'required',
        );

        // field name overrides
        $names = array(
            'account_no' => 'account no',
        );

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if ($new) {
            $row = new Loan;
        }

        $row->account_no     = array_get($input, 'account_no');
        $row->name     = array_get($input, 'name');
        // save model
        $row->save();

        // return
        if ($new) {
        	return Response::json(['body' => 'Loan successfully added.']);
        } else {
        	return Response::json(['body' => 'Loan successfully updated.']);
        }
    }

    public function edit()
    {
        $id = Request::input('id');
        if ($id) {
	        $row = Loan::find($id);

	        if($row) {
	            return Response::json($row);
	        } else {
	            return Response::json(['error' => "Invalid row specified"]);
	        }
        } else {
					return Response::json(['error' => "Invalid row specified"]);
        }
    }

    public function view() {

   		$get_amortization = LoanAmortization::where(function($query) {
   								$query->where('loan_id', '=', Request::input('id'))
   											->where('status', 1);
   							})->get();

      $rows = '<div id="loanExpand" class="type-table"><table class="table table-striped">' .
              '<thead>' .
              '<th class="tbheader"> TITLE</th>
              <th class="tbheader"> LOAN AMOUNT</th>
              <th class="tbheader"> INTEREST RATE</th>
              <th class="tbheader"> TERM OF LOAN</th>
              <th></th>
              </thead><tbody>';

     foreach ($get_amortization as $key => $field) {
     		$rows .= '<tr data-id="' . $field->id . '">' .
     							'<td>' . $field->title . '</td>' . 
     							'<td>Php ' . number_format($field->loan_amount, 2) . '</td>' . 
     							'<td>' . number_format($field->interest_rate, 2) . ' %</td>' . 
     							'<td>' . $field->term_of_loan . '</td>' . 
     							'<td class="text-right">' .
     								' <a data-toggle="tooltip" title="View" data-id="' . $field->id . '"  data-loan_id="' . Request::input('id') . '" class="btn btn-info borderZero btn-xs btn-view-amortization"><i class="fa fa-eye"></i></a>' .
     								' <a data-toggle="tooltip" title="Edit" class="btn btn-primary borderZero btn-xs btn-edit-amortization"><i class="fa fa-pencil"></i></a>' .
     								' <a data-toggle="tooltip" title="Delete" data-id="' . $field->id . '" class="btn btn-danger borderZero btn-xs btn-delete-amortization"><i class="fa fa-trash"></i></a>' .
     							'</td>' . 
     							'</tr>';
     }

			$rows .= '</tbody></table></div>';

      if (Request::input('id')) {
        return Response::json($rows);
      } else {
        return Response::json(['error' => "The requested item was not found in the database."]);
      }

    }

    /**
     * Save model
     *
     */
    public function saveAmortization() {    

        // assume this is a new row
        $new = true;

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = LoanAmortization::where('status', 1)->find(array_get($input, 'id'));

            if(!$row) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }

            // this is an existing row
            $new = false;
        }

        $rules = array(
        	'loan_amount' => 'required|numeric',
        	'interest_rate' => 'required|numeric',
        	'term_of_loan' => 'required|integer',
        	'date_acc' => 'required|date',
        );

        // field name overrides
        $names = array(
            'date_acc' => 'first accrual date',
        );

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

	      if(!array_get($input, 'loan_id')) {
	        return Response::json(['error' => ["The requested item was not found in the database."]]);
	      }

	      $get_loan = Loan::find(array_get($input, 'loan_id'));

	      if (!$get_loan) {
	        return Response::json(['error' => ["The requested item was not found in the database."]]);
	      }

        $check_unique_title = LoanAmortization::where(function ($query) use ($input) {
        												$query->where('loan_id', '=', array_get($input, 'loan_id'))
        															->where('title', '=', array_get($input, 'title'));
        											})->first();

	      if ($check_unique_title) {

	      	if (!$new && array_get($input, 'title') == $check_unique_title->title) {

	      	} else {
	        	return Response::json(['error' => ["The title has already been taken."]]);
	      	}

	      }

        if ($new) {
            $row = new LoanAmortization;
        }

        $row->loan_id     = array_get($input, 'loan_id');
        $row->loan_amount     = array_get($input, 'loan_amount');
        $row->interest_rate     = array_get($input, 'interest_rate');
        $row->term_of_loan     = array_get($input, 'term_of_loan');
        $row->title     = array_get($input, 'title');
        $row->date_acc      = Carbon::createFromFormat('m/d/Y', array_get($input, 'date_acc'));

        // save model
        $row->save();

        // return
        if ($new) {
        	return Response::json(['body' => 'Amortization successfully added.']);
        } else {
        	return Response::json(['body' => 'Amortization successfully updated.']);
        }
    }

    public function editAmortization()
    {
        $id = Request::input('id');
        if ($id) {
	        $row = LoanAmortization::find($id);

	        if($row) {
	            return Response::json($row);
	        } else {
	            return Response::json(['error' => "Invalid row specified"]);
	        }
        } else {
					return Response::json(['error' => "Invalid row specified"]);
        }
    }

    /**
     * Delete the user
     *
     */
    public function delete() {
        if (Request::input('id')) {
          $row = Loan::find(Request::input('id'));

          // check if user exists
          if(!is_null($row)) {
            $row->status = 2;
            $row->save();

            // return
            return Response::json(['body' => 'Loan has been deleted.']);
          } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
          }
        } else {
          // not found
          return Response::json(['error' => ["The requested item was not found in the database."]]);
        }
    }

    /**
     * Delete the user
     *
     */
    public function deleteAmortization() {
        if (Request::input('id')) {
          $row = LoanAmortization::find(Request::input('id'));

          // check if user exists
          if(!is_null($row)) {
            $row->status = 2;
            $row->save();

            // return
            return Response::json(['body' => 'Amortization has been deleted.']);
          } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
          }
        } else {
          // not found
          return Response::json(['error' => ["The requested item was not found in the database."]]);
        }
    }

    public function preview() {

    	// assume this is a new row
      $new = true;

      $input = Input::all();

      // check if an ID is passed
      if(array_get($input, 'id')) {

          // get the user info
          $row = LoanAmortization::where('status', 1)->find(array_get($input, 'id'));

          if(!$row) {
              return Response::json(['error' => "The requested item was not found in the database."]);
          }

          // this is an existing row
          $new = false;
      }

      if(!array_get($input, 'loan_id')) {
        return Response::json(['error' => ["The requested item was not found in the database."]]);
      }

      $get_loan = Loan::find(array_get($input, 'loan_id'));

      if (!$get_loan) {
        return Response::json(['error' => ["The requested item was not found in the database."]]);
      }

      $rules = array(
      	'loan_amount' => 'required|numeric',
      	'interest_rate' => 'required|numeric',
      	'term_of_loan' => 'required|integer',
      	'date_acc' => 'required|date',
      );

      // field name overrides
      $names = array(
          'date_acc' => 'first accrual date',
      );

      // do validation
      $validator = Validator::make(Input::all(), $rules);
      $validator->setAttributeNames($names); 

      // return errors
      if($validator->fails()) {
          return Response::json(['error' => array_unique($validator->errors()->all())]);
      }

      $check_unique_title = LoanAmortization::where(function ($query) use ($input) {
      												$query->where('loan_id', '=', array_get($input, 'loan_id'))
      															->where('title', '=', array_get($input, 'title'));
      											})->first();

      if ($check_unique_title) {

      	if (!$new && array_get($input, 'title') == $check_unique_title->title) {

      	} else {
        	return Response::json(['error' => ["The title has already been taken."]]);
      	}

      }

      $loan_amount = array_get($input, 'loan_amount');
      $interest_rate = array_get($input, 'interest_rate');
      $term_of_loan = array_get($input, 'term_of_loan');
      $title = array_get($input, 'title');
      $date_acc = Carbon::createFromFormat('m/d/Y', array_get($input, 'date_acc'))->format('l, F d, Y');
      $date_mat = Carbon::createFromFormat('m/d/Y', array_get($input, 'date_acc'))->addMonth($term_of_loan - 1)->format('l, F d, Y');

      $interest = $loan_amount * ($interest_rate / 100);
      $total_interest = $interest * $term_of_loan;

      $total_payment = $total_interest + $loan_amount;

      $row = '<div class="table-responsive"><table class="table">' .
							'<tbody>' .
								'<thead>' .
									'<th colspan="4"><center>LOAN AMORTIZATION SCHEDULE</center></th>' .
								'</thead>' .
								'<tr>' .
									'<td><strong>Account No.</strong></td>' .
									'<td>' . $get_loan->account_no . '</td>' .
									'<td><strong>SUMMARY</strong></td>' .
									'<td></td>' .
								'</tr>' .
								'<tr>' .
									'<td><strong>Account Name</strong></td>' .
									'<td>' . $get_loan->name . '</td>' .
									'<td><strong>TOTAL PAYMENT</strong></td>' .
									'<td>PHP ' . number_format($total_payment, 2) . '</td>' .
								'</tr>' .
								'<tr>' .
									'<td><strong>Loan Amount</strong></td>' .
									'<td>PHP ' . number_format($loan_amount, 2) . '</td>' .
									'<td><strong>TOTAL INTEREST</strong></td>' .
									'<td>PHP ' . number_format($total_interest, 2) . '</td>' .
								'</tr>' .
								'<tr>' .
									'<td><strong>Interest Rate</td>' .
									'<td>' . number_format($interest_rate, 2) . '%</td>' .
									'<td></td>' .
									'<td></td>' .
								'</tr>' .
								'<tr>' .
									'<td><strong>Term of Loan</strong></td>' .
									'<td>' . $term_of_loan . '</td>' .
									'<td></td>' .
									'<td></td>' .
								'</tr>' .
								'<tr>' .
									'<td><strong>First Accrual Date</strong></td>' .
									'<td>' . $date_acc . '</td>' .
									'<td></td>' .
									'<td></td>' .
								'</tr>' .
								'<tr>' .
									'<td><strong>Maturity Date</strong></td>' .
									'<td>' . $date_mat . '</td>' .
									'<td></td>' .
									'<td></td>' .
								'</tr>' .
								'<tr>' .
									'<td><strong>Payment Frequency</strong></td>' .
									'<td>Monthly</td>' .
									'<td></td>' .
									'<td></td>' .
								'</tr>' .
							'</tbody>' .
						'</table>' .
						'<table class="table table-responsive">' .
							'<thead>' .
								'<tr>' .
									'<th>Due Date</th>' .
									'<th>Principal</th>' .
									'<th>Interest</th>' .
									'<th>Principal + Interest</th>' .
									'<th>Total Amount Due</th>' .
									'<th>Running Balance</th>' .
								'</tr>' .
							'</thead>' .
							'<tbody>';

							$principal = $loan_amount / $term_of_loan;
							$total_amount_due = $principal + $interest;
							$running_balance = 0;

							for ($i=0; $i < $term_of_loan; $i++) { 
								$running_balance = $loan_amount - ($principal * $i);
								$principal_interest = $running_balance + ($interest * ($term_of_loan - $i)); 

								$row .= '<tr>' .
								'<td>' . Carbon::createFromFormat('m/d/Y', array_get($input, 'date_acc'))->addMonth($i)->format('d-M-y') . '</td>' .
								'<td>PHP ' . number_format($principal, 2) . '</td>' .
								'<td>PHP ' . number_format($interest, 2) . '</td>' .
								'<td>PHP ' . number_format($principal_interest, 2) . '</td>' .
								'<td>PHP ' . number_format($total_amount_due, 2) . '</td>' .
								'<td>PHP ' . number_format($running_balance, 2) . '</td>' .
								'</tr>';
							}

							$row .=	'<tr>' .
									'<td><strong>Total:</strong></td>' .
									'<td>PHP ' . number_format($loan_amount, 2) . '</td>' .
									'<td>PHP ' . number_format($total_interest, 2) . '</td>' .
									'<td></td>' .
									'<td>PHP ' . number_format($total_payment, 2) . '</td>' .
									'<td></td>' .
								'</tr>' .
							'</tbody>' .
						'</table></div>';

      return Response::json($row);
    }

    public function viewAmortization() {

      $input = Input::all();

      if(!array_get($input, 'id')) {
        return Response::json(['error' => "The requested item was not found in the database."]);
      }

      $get_amortization = LoanAmortization::where(function ($query) use ($input){
														      	$query->where('status', 1)
														      				->where('id', '=', array_get($input, 'id'));
														      			})->first();

      if(!$get_amortization) {
        return Response::json(['error' => "The requested item was not found in the database."]);
      }

      if(!array_get($input, 'loan_id')) {
        return Response::json(['error' => ["The requested item was not found in the database."]]);
      }

      $get_loan = Loan::find(array_get($input, 'loan_id'));

      if (!$get_loan) {
        return Response::json(['error' => ["The requested item was not found in the database."]]);
      }

      $loan_amount = $get_amortization->loan_amount;
      $interest_rate = $get_amortization->interest_rate;
      $term_of_loan = $get_amortization->term_of_loan;
      $title = $get_amortization->title;
      $date_acc = Carbon::createFromFormat('Y-m-d', $get_amortization->date_acc)->format('l, F d, Y');
      $date_mat = Carbon::createFromFormat('Y-m-d', $get_amortization->date_acc)->addMonth($term_of_loan - 1)->format('l, F d, Y');

      $interest = $loan_amount * ($interest_rate / 100);
      $total_interest = $interest * $term_of_loan;

      $total_payment = $total_interest + $loan_amount;

      $row = '<div class="table-responsive"><table class="table">' .
							'<tbody>' .
								'<thead>' .
									'<th colspan="4"><center>LOAN AMORTIZATION SCHEDULE</center></th>' .
								'</thead>' .
								'<tr>' .
									'<td><strong>Account No.</strong></td>' .
									'<td>' . $get_loan->account_no . '</td>' .
									'<td><strong>SUMMARY</strong></td>' .
									'<td></td>' .
								'</tr>' .
								'<tr>' .
									'<td><strong>Account Name</strong></td>' .
									'<td>' . $get_loan->name . '</td>' .
									'<td><strong>TOTAL PAYMENT</strong></td>' .
									'<td>PHP ' . number_format($total_payment, 2) . '</td>' .
								'</tr>' .
								'<tr>' .
									'<td><strong>Loan Amount</strong></td>' .
									'<td>PHP ' . number_format($loan_amount, 2) . '</td>' .
									'<td><strong>TOTAL INTEREST</strong></td>' .
									'<td>PHP ' . number_format($total_interest, 2) . '</td>' .
								'</tr>' .
								'<tr>' .
									'<td><strong>Interest Rate</td>' .
									'<td>' . number_format($interest_rate, 2) . '%</td>' .
									'<td></td>' .
									'<td></td>' .
								'</tr>' .
								'<tr>' .
									'<td><strong>Term of Loan</strong></td>' .
									'<td>' . $term_of_loan . '</td>' .
									'<td></td>' .
									'<td></td>' .
								'</tr>' .
								'<tr>' .
									'<td><strong>First Accrual Date</strong></td>' .
									'<td>' . $date_acc . '</td>' .
									'<td></td>' .
									'<td></td>' .
								'</tr>' .
								'<tr>' .
									'<td><strong>Maturity Date</strong></td>' .
									'<td>' . $date_mat . '</td>' .
									'<td></td>' .
									'<td></td>' .
								'</tr>' .
								'<tr>' .
									'<td><strong>Payment Frequency</strong></td>' .
									'<td>Monthly</td>' .
									'<td></td>' .
									'<td></td>' .
								'</tr>' .
							'</tbody>' .
						'</table>' .
						'<table class="table table-responsive">' .
							'<thead>' .
								'<tr>' .
									'<th>Due Date</th>' .
									'<th>Principal</th>' .
									'<th>Interest</th>' .
									'<th>Principal + Interest</th>' .
									'<th>Total Amount Due</th>' .
									'<th>Running Balance</th>' .
								'</tr>' .
							'</thead>' .
							'<tbody>';

							$principal = $loan_amount / $term_of_loan;
							$total_amount_due = $principal + $interest;
							$running_balance = 0;

							for ($i=0; $i < $term_of_loan; $i++) { 
								$running_balance = $loan_amount - ($principal * $i);
								$principal_interest = $running_balance + ($interest * ($term_of_loan - $i)); 

								$row .= '<tr>' .
								'<td>' . Carbon::createFromFormat('Y-m-d', $get_amortization->date_acc)->addMonth($i)->format('d-M-y') . '</td>' .
								'<td>PHP ' . number_format($principal, 2) . '</td>' .
								'<td>PHP ' . number_format($interest, 2) . '</td>' .
								'<td>PHP ' . number_format($principal_interest, 2) . '</td>' .
								'<td>PHP ' . number_format($total_amount_due, 2) . '</td>' .
								'<td>PHP ' . number_format($running_balance, 2) . '</td>' .
								'</tr>';
							}

							$row .=	'<tr>' .
									'<td><strong>Total:</strong></td>' .
									'<td>PHP ' . number_format($loan_amount, 2) . '</td>' .
									'<td>PHP ' . number_format($total_interest, 2) . '</td>' .
									'<td></td>' .
									'<td>PHP ' . number_format($total_payment, 2) . '</td>' .
									'<td></td>' .
								'</tr>' .
							'</tbody>' .
						'</table></div>';

      return Response::json($row);

    }

    public function download() {
    
    $check_loan = null;
   	$title_loan = "Invalid";
   	if (!Request::input('id')) {
   		$title_loan = "Invalid";
   	} else {
	    $check_loan = Loan::find(Request::input('id'));
	   	if ($check_loan) {
	   		$title_loan = $check_loan->account_no;
	   	}
   	}

    Excel::create($title_loan, function($excel) use ($check_loan) {
    	$row = LoanAmortization::where(function($query) {
    														$query->where('loan_id', '=', Request::input('id'))
    																	->where('status', 1);
    														})->get();


    	if (!Request::input('id') || !$check_loan) {

		    $excel->sheet('Error', function($sheet) {

		      $sheet->appendRow(array(
		          'Invalid data.'
		      ));

		    });
    	} else if (count($row) < 1 && Request::input('id')) {

		    $excel->sheet($check_loan->account_no, function($sheet) {

		      $sheet->appendRow(array(
		          'No Amortization Detected.'
		      ));

		    });
    	} else {

    		foreach ($row as $key => $field) {
			    $excel->sheet($field->title, function($sheet) use ($field, $check_loan) {

						$sheet->mergeCells('A1:F1');

						$sheet->cell('A1', function($cells) {
							// Set font
							$cells->setFont(array(
							    'family'     => 'Myraid Pro',
							    'size'       => '28',
							    'bold'       =>  true,
							));

	 						$cells->setAlignment('center');
	 						$cells->setValignment('middle');

						});

			      $loan_amount = $field->loan_amount;
			      $interest_rate = $field->interest_rate;
			      $term_of_loan = $field->term_of_loan;
			      $title = $field->title;
			      $date_acc = Carbon::createFromFormat('Y-m-d', $field->date_acc)->format('l, F d, Y');
			      $date_mat = Carbon::createFromFormat('Y-m-d', $field->date_acc)->addMonth($term_of_loan - 1)->format('l, F d, Y');

			      $interest = $loan_amount * ($interest_rate / 100);
			      $total_interest = $interest * $term_of_loan;

			      $total_payment = $total_interest + $loan_amount;
			      
			      $row_cell = 0;

			      $sheet->appendRow(array(
			          'LOAN AMORTIZATION SCHEDULE'
			      ));
			      $row_cell += 1;

			      $sheet->appendRow(array(
			          'Account No.', $check_loan->account_no, 'SUMMARY'
			      ));
			      $row_cell += 1;

			      $sheet->appendRow(array(
			          'Account Name', $check_loan->name, 'TOTAL PAYMENT', 'Php ' . number_format($total_payment, 2)
			      ));
			      $row_cell += 1;

			      $sheet->appendRow(array(
			          'Loan Amount', 'Php ' . number_format($loan_amount, 2), 'TOTAL PAYMENT', 'Php ' . number_format($total_interest, 2)
			      ));
			      $row_cell += 1;

			      $sheet->appendRow(array(
			          'Interest Rate', number_format($interest_rate, 2) . '%'
			      ));
			      $row_cell += 1;

			      $sheet->appendRow(array(
			          'Term of Loan', $term_of_loan
			      ));
			      $row_cell += 1;

			      $sheet->appendRow(array(
			          'First Accrual Date', $date_acc
			      ));
			      $row_cell += 1;

			      $sheet->appendRow(array(
			          'Maturity Date', $date_mat
			      ));
			      $row_cell += 1;

			      $sheet->appendRow(array(
			          'Payment Frequency', 'Monthly'
			      ));
			      $row_cell += 1;

			      $sheet->appendRow(array(
			          'Due Date', 'Principal', 'Interest', 'Principal+Interest', 'Total Amount Due', 'Running Balance'
			      ));
			      $row_cell += 1;

						$sheet->cell('C2:D9', function($cells) {
							$cells->setBackground('#c4d79b');
						});

						$sheet->cell('B2:B9', function($cells) {
							$cells->setBackground('#d8e4bc');
						});

						$sheet->cell('A2:A9', function($cells) {
							$cells->setBackground('#c4d79b');
						});


						$principal = $loan_amount / $term_of_loan;
						$total_amount_due = $principal + $interest;
						$running_balance = 0;

						for ($i=0; $i < $term_of_loan; $i++) { 
							$running_balance = $loan_amount - ($principal * $i);
							$principal_interest = $running_balance + ($interest * ($term_of_loan - $i)); 

				      $sheet->appendRow(array(
				          Carbon::createFromFormat('Y-m-d', $field->date_acc)->addMonth($i)->format('d-M-y'),
				          'Php ' . number_format($principal, 2),
				          'Php ' . number_format($interest, 2),
				          'Php ' . number_format($principal_interest, 2),
				          'Php ' . number_format($total_amount_due, 2),
				          'Php ' . number_format($running_balance, 2)
				      ));
			      	$row_cell += 1;
						}

			      $sheet->appendRow(array(
			          ''
			      ));
			      $row_cell += 1;

			      $sheet->appendRow(array(
			          'Total:',
			          'Php ' . number_format($loan_amount, 2) ,
			          'Php ' . number_format($total_interest, 2),
			          '',
			          'Php ' . number_format($total_payment, 2)
			      ));
			      $row_cell += 1;

						$sheet->row($row_cell, function($row_excel) {
											$row_excel->setFont(array(
													    'family'     => 'Myraid Pro',
													    'bold'       =>  true,
													));
						});

						$sheet->setSize(array(
						    'A1' => array(
						        'width'     => 25,
						        'height'     => 35,
						    ),
						    'B1' => array(
						        'width'     => 25,
						        'height'     => 35,
						    ),
						    'C1' => array(
						        'width'     => 25,
						        'height'     => 35,
						    ),
						    'D1' => array(
						        'width'     => 25,
						        'height'     => 35,
						    ),
						    'E1' => array(
						        'width'     => 25,
						        'height'     => 35,
						    ),
						    'F1' => array(
						        'width'     => 25,
						        'height'     => 35,
						    ),
						));

						$sheet->cell('A10', function($cells) {
							// Set font
							$cells->setFont(array(
							    'family'     => 'Myraid Pro',
							    'bold'       =>  true,
							));

	 						$cells->setAlignment('center');
	 						$cells->setValignment('middle');
						});

						$sheet->cell('B10', function($cells) {
							// Set font
							$cells->setFont(array(
							    'family'     => 'Myraid Pro',
							    'bold'       =>  true,
							));

	 						$cells->setAlignment('center');
	 						$cells->setValignment('middle');
						});

						$sheet->cell('C10', function($cells) {
							// Set font
							$cells->setFont(array(
							    'family'     => 'Myraid Pro',
							    'bold'       =>  true,
							));

	 						$cells->setAlignment('center');
	 						$cells->setValignment('middle');
						});

						$sheet->cell('D10', function($cells) {
							// Set font
							$cells->setFont(array(
							    'family'     => 'Myraid Pro',
							    'bold'       =>  true,
							));

	 						$cells->setAlignment('center');
	 						$cells->setValignment('middle');
						});

						$sheet->cell('E10', function($cells) {
							// Set font
							$cells->setFont(array(
							    'family'     => 'Myraid Pro',
							    'bold'       =>  true,
							));

	 						$cells->setAlignment('center');
	 						$cells->setValignment('middle');
						});

						$sheet->cell('F10', function($cells) {
							// Set font
							$cells->setFont(array(
							    'family'     => 'Myraid Pro',
							    'bold'       =>  true,
							));

	 						$cells->setAlignment('center');
	 						$cells->setValignment('middle');
						});

			    });
	    	}
    	}

    })->download('xls');

  }

}