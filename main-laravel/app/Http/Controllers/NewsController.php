<?php

namespace App\Http\Controllers;

use Auth;
use Crypt;
use Input;
use Request;
use Validator;
use Response;
use Hash;
use Image;
use Paginator;
use Session;
use Redirect;
use App\User;
use App\UserType;
use App\News;
use App\Contact;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\NewsRequest;

class NewsController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get row set
        $result = $this->doList();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        
        $this->data['url'] = url("admin/news");
        $this->data['title'] = "News";
        return view('news.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doList() {
      // sort and order
      // $result['sort'] = Request::input('s') ?: 'created_at';
      // $result['order'] = Request::input('o') ?: 'desc';
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 10;
      // dd(Request::all());
      if (!Auth::check()) {

          // return response (format accordingly)
          if(Request::ajax()) {
              return Response::json(['relogin' => "Relogin"]);
          }
      }

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = News::where('title', 'LIKE', '%' . $search . '%')->orderBy($result['sort'], $result['order'])->paginate($per);

      } else {
        $count = News::where('title', 'LIKE', '%' . $search . '%')->orderBy($result['sort'], $result['order'])->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = News::where('title', 'LIKE', '%' . $search . '%')->orderBy($result['sort'], $result['order'])->paginate($per);

      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function redirectIndex()
    {
        // get row set
        $result = $this->doRedirectList();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];

        $this->data['url'] = url("admin/news");
        $this->data['notice'] = Session::has('notice');
        $this->data['title'] = "News";

        return Response::json($this->data);
        // return view('news.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doRedirectList() {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'created_at';
        $result['order'] = Request::input('o') ?: 'desc';

        $rows = News::orderBy($result['sort'], $result['order'])->paginate(10);

        $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
        $result['rows'] = $rows;
        return $result;
    }
    /**
     * Save model
     *
     */
    public function save() {    

        $input = Request::all();
        
        // assume this is a new row
        $new = true;

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = News::where('status', 1)->find(array_get($input, 'id'));

            if(!$row) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }

            // this is an existing row
            $new = false;
        }

        $rules = array(
            'title' => 'required',
            'body' => 'required',
        );

        // field name overrides
        $names = array(
            'title' => 'news title',
            'body' => 'body',
        );

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if ($new) {
            $row = new News;
        }

        $row->title     = array_get($input, 'title');
        $row->body      = array_get($input, 'body');
        $row->status    = 1;

        // save model
        $row->save();

        $row->slug           = str_slug(array_get($input, 'title'));
        // save model
        $row->save();

        // return
        return Response::json(['body' => 'News successfully added.']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->data['title'] = "Create News";
        return view('news.create', $this->data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function view()
    {   
        $id = Request::input('id');
        if ($id) {
            $row = News::find($id);
            if ($row) {
                return Response::json($row);
            }
        }

        return Response::json(['error' => "The requested item was not found in the database."]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function all()
    {
        // get row set
        $result = $this->doList();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];

        $this->data['contact'] = Contact::first();

        $this->data['title_arabic'] = 'أخبار';
        $this->data['url'] = url("admin/news");
        $this->data['title'] = "News";

        return view('news.all', $this->data);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if (is_null(News::find($id))) {
            return abort(404);
        } else {
            $this->data['news'] = News::find($id);

            $this->data['title'] = $this->data['news']->title;

            return view('news.edit', $this->data);
        }
    }

    /**
     * Delete the user
     *
     */
    public function delete() {
        if (Request::input('id')) {
          $row = News::find(Request::input('id'));

          // check if user exists
          if(!is_null($row)) {
            News::destroy(Request::input('id'));

            // return
            return Response::json(['body' => 'News has been deleted.']);
          } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
          }
        } else {
          // not found
          return Response::json(['error' => ["The requested item was not found in the database."]]);
        }
    }

}
