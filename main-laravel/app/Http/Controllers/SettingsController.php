<?php

namespace App\Http\Controllers;

use Auth;
use Crypt;
use Input;
use Request;
use Validator;
use Response;
use Hash;
use Image;
use Paginator;
use Session;
use Redirect;
use App\User;

class SettingsController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {

        $this->data['settings'] = User::find(Auth::user()->id);

        $this->data['title'] = "Settings";

        return view('settings.list', $this->data);
    }

    public function save() {

        // dd(Request::all());

        $input = Input::all();

        // get the user info
        $row = User::where('status', 1)->find(Auth::user()->id);

        if(!$row) {
            return Response::json(['error' => "The requested item was not found in the database."]);
        }

        $rules = [
            'username' => 'required|min:2|max:100',
            'password' => 'min:6|confirmed',
            'email' => 'required|email|unique:users,email,' . $row->id,
            'email_inq' => 'required|email|unique:users,email_inq,' . $row->id,
            'email_app' => 'required|email|unique:users,email_app,' . $row->id,
        ];

        // field name overrides
        $names = [
            'email_inq' => 'main recipient (inquiries)',
            'email_app' => 'main recipient (applications)',
        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if (Request::input('inquiries')) {
            $inquiries = explode(",", Request::input('inquiries'));

            foreach ($inquiries as $key => $value) {
                if(!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                    return Response::json(['error' => ["The sub recipient (inquiries) emails must be a valid email address."]]);
                }
            }
        }

        if (Request::input('applications')) {
            $applications = explode(",", Request::input('applications'));

            foreach ($applications as $key => $value) {
                if(!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                    return Response::json(['error' => ["The sub recipient (applications) emails must be a valid email address."]]);
                }
            }
        }

        $row->username      = array_get($input, 'username');
        $row->email     = array_get($input, 'email');
        $row->email_inq    = array_get($input, 'email_inq');
        $row->email_app  = array_get($input, 'email_app');
        $row->inquiries  = array_get($input, 'inquiries');
        $row->applications  = array_get($input, 'applications');

        // do not change password if old user and field is empty
        if(!empty(array_get($input, 'password'))) {
            $row->password = Hash::make(array_get($input, 'password'));
        }
        // save model
        $row->save();

        // return
        return Response::json(['body' => 'Updated.']);


    }

}
