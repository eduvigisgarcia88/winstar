<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Password;
use Redirect;
use Input;
use Lang;
use Request;
use View;
use Hash;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\BackendController;
use Validator;
use App\Banner;
use Image;
use Response;
class BannerController extends Controller
{

	public function index(){
		$result = $this->doList();
		$this->data['count_banners'] = Banner::count('id');
		$this->data['rows'] = $result['rows'];
		$this->data['title'] = "Banner Management";
		return view('banner.list', $this->data);
	}
	  public function edit()
    {
        $id = Request::input('id');
        if ($id) {
	        $row = Banner::find($id);

	        if($row) {
	            return Response::json($row);
	        } else {
	            return Response::json(['error' => "Invalid row specified"]);
	        }
        } else {
				return Response::json(['error' => "Invalid row specified"]);
        }
    }

     public function doList() {
      $rows = Banner::take(3)->get();

      // return response (format accordingly)
      if(Request::ajax()) {
          //$result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          //$result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }
     public function delete() {
        if (Request::input('id')) {
          $row = Banner::find(Request::input('id'));

          // check if user exists
          if(!is_null($row)) {
           Banner::destroy(Request::input('id'));

            // return
            return Response::json(['body' => 'Banner has been deleted.']);
          } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
          }
        } else {
          // not found
          return Response::json(['error' => ["The requested item was not found in the database."]]);
        }
    }
    public function disable() {
        if (Request::input('id')) {
          $row = Banner::find(Request::input('id'));
          // check if user exists
          if(!is_null($row)) {
           	$row->status = "2";
           	$row->save();

            // return
            return Response::json(['body' => 'Banner has been disabled.']);
          } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
          }
        } else {
          // not found
          return Response::json(['error' => ["The requested item was not found in the database."]]);
        }
    }

    public function enable() {
        if (Request::input('id')) {
          $row = Banner::find(Request::input('id'));
          // check if user exists
          if(!is_null($row)) {
            $row->status = "1";
            $row->save();

            // return
            return Response::json(['body' => 'Banner has been enabled.']);
          } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
          }
        } else {
          // not found
          return Response::json(['error' => ["The requested item was not found in the database."]]);
        }
    }
	public function save(){
		  $new = true;

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = Banner::where('status', 1)->find(array_get($input, 'id'));

            if(!$row) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }

            // this is an existing row
            $new = false;
        }

        $rules = array(
            'title' => $new ? 'required' : '',
            'url' => $new ? 'required' : '',
        );

        // field name overrides
        $names = array(
            'title' => 'Catch Phrase',
            'url' => 'Image Url',
        );

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if ($new) {
            $row = new Banner;
        }

        $row->title     = array_get($input, 'title');
        $row->url     = array_get($input, 'url');
        // save model
        $row->save();


        // return
        if ($new) {
          return Response::json(['body' => 'Banner successfully added.']);
        } else {
          return Response::json(['body' => 'Banner successfully updated.']);
        }
	}
}