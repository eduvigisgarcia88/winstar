<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    protected $table = 'promos';

    protected $fillable = [
    	'photo',
    	'title_eng',
    	'desc_eng',
    	'title_arabic',
    	'desc_arabic',
    	'status',
    ];
}
