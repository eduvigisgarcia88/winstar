  
//tab navigation
$(document).ready(function(){
    $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });
   
});


$.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
    options.async = true;
});

//$(".btn-file").html("<i class='fa fa-folder'></i>");
// tooltip
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
// show dialog modal
function dialog(title, body, action, id) {
	$("#dialog-title").html(title);
	$("#dialog-body").html(body);
	$("#dialog-confirm").data('url', action)
						.data('id', id);

	// show confirmation
    $("#modal-dialog").modal('show');
}

// display status message
function status(title, body, type, box) {
	if(box) {
		var html = '<' + 'div class="alert alert-dismissible ' + type + '" role="alert">' +
				   '<' + 'button type="button" class="close" data-dismiss="alert"><' + 'span aria-hidden="true">&times;</' + 'span><' + 'span class="sr-only">Close</' + 'span></' + 'button>' +
				   (title ? '<strong>' + title + '</strong> ' : '') + body +
				   '</div>';

		$(box).removeClass('hide').html(html);
	} else {
		switch(type) {
			case 'alert-success':
				toastr.success(body);
				break;
			case 'alert-danger':
				toastr.error(body);
				break;
		}
	}
}

// get url parameter
function urlData(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');

    for(var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if(sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

$(document).ready(function() {

	$(".table").addClass("tablesorter");

	// do action after confirmation
	$("#dialog-confirm").click(function() {
		var url = $(this).data('url');
		var id = $(this).data('id');
      	token = $("#token").val();

		$.post(url, { id: id, _token: token }, function(response) {
			$("#modal-dialog").modal('hide');

			if(response.error) {
				status("Error", response.error, 'alert-danger');
			} else {
				status(response.title ? response.title : false, response.body, 'alert-success', response.box ? response.box : false);
				refresh();
			}
		}, 'json');
	});

	// reset form notice
	$("#modal-form").on('show.bs.modal', function(e) {
		$("#form-notice").html("");
	});

	// submit form
    $("#modal-form").find("form").submit(function(e) {
   
  		var form = $("#modal-form").find("form");
  		var loading = $("#load-form");

  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			//data: form.serialize(),
			data: new FormData($("#modal-save_form")[0]),
			dataType: "json",	
			async: true,
	  		processData: false,
	  		contentType: false,

			success: function(response) {

                if (response.unauthorized) {
                window.location.href = '{{ url("login")}}';
                }

				if(response.error) {
					var errors = '';

					$.each(response.error, function(index, value) {
						errors +=  value;
					});

					status('', errors, 'alert-danger', '#form-notice');
				} else {
					$("#modal-form").modal('hide');
					if (form.parent().hasClass('ajax-submit')) {
						status(response.title, response.body, 'alert-success');
					}
					
					$("#alert").addClass("hide");
					$("#alert-delete").removeClass("hide");
					// refresh the list
					
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });
   
 $("#modal-disable").find("form").submit(function(e) {
   
        var form = $("#modal-disable").find("form");
        var loading = $("#load-form");

        // stop form from submitting
        e.preventDefault();

        loading.removeClass('hide');
        console.log(form.serialize());
        // push form data
        $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-save_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,

            success: function(response) {

                if (response.unauthorized) {
                window.location.href = '{{ url("login")}}';
                }

                if(response.error) {
                    var errors = '';

                    $.each(response.error, function(index, value) {
                        errors +=  value;
                    });

                    status('', errors, 'alert-danger', '#form-notice');
                } else {
                    $("#modal-disable").modal('hide');
                    if (form.parent().hasClass('ajax-submit')) {
                        status(response.title, response.body, 'alert-success');
                    }
                    
                    $("#alert").addClass("hide");
                    $("#alert-delete").removeClass("hide");
                    // refresh the list
                    
                    refresh();
                }

                loading.addClass('hide');

            }
        });
    });
  $("#modal-enable").find("form").submit(function(e) {
   
        var form = $("#modal-enable").find("form");
        var loading = $("#load-form");

        // stop form from submitting
        e.preventDefault();

        loading.removeClass('hide');
        console.log(form.serialize());
        // push form data
        $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-save_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,

            success: function(response) {

                if (response.unauthorized) {
                window.location.href = '{{ url("login")}}';
                }

                if(response.error) {
                    var errors = '';

                    $.each(response.error, function(index, value) {
                        errors +=  value;
                    });

                    status('', errors, 'alert-danger', '#form-notice');
                } else {
                    $("#modal-enable").modal('hide');
                    if (form.parent().hasClass('ajax-submit')) {
                        status(response.title, response.body, 'alert-success');
                    }
                    
                    $("#alert").addClass("hide");
                    $("#alert-delete").removeClass("hide");
                    // refresh the list
                    
                    refresh();
                }

                loading.addClass('hide');

            }
        });
    });
	// reset form notice
	$("#modal-client").on('show.bs.modal', function(e) {
		$("#client-notice").html("");
	});

	// submit form
    $("#modal-client").find("form").submit(function(e) {
   
  		var form = $("#modal-client").find("form");
  		var loading = $("#load-client");

  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			//data: form.serialize(),
			data: new FormData($("#modal-save_client")[0]),
			dataType: "json",	
			async: true,
      		processData: false,
      		contentType: false,
			success: function(response) {

                if (response.unauthorized) {
                window.location.href = '{{ url("login")}}';
                }

				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#client-notice');
				} else {
					$("#modal-client").modal('hide');
					status(response.title, response.body, 'alert-success');
					
					// refresh the list
					
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });
  // submit form
    $("#modal-banner").find("form").submit(function(e) {
   
        var form = $("#modal-banner").find("form");
        var loading = $("#load-banner");

        // stop form from submitting
        e.preventDefault();

        loading.removeClass('hide');
        console.log(form.serialize());
        // push form data
        $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-save_banner")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
            success: function(response) {

                if (response.unauthorized) {
                window.location.href = '{{ url("login")}}';
                }

                if(response.error) {
                    var errors = '<ul>';

                    $.each(response.error, function(index, value) {
                        errors += '<li>' + value + '</li>';
                    });

                    errors += '</ul>';

                    status('Please correct the following:', errors, 'alert-danger', '#banner-notice');
                } else {
                    $("#modal-banner").modal('hide');
                    status(response.title, response.body, 'alert-success');
                    
                    // refresh the list
                    
                    refresh();
                }

                loading.addClass('hide');

            }
        });
    });


    // reset form notice
    $("#modal-replied").on('show.bs.modal', function(e) {
        $("#replied-notice").html("");
    });

    // submit form
    $("#modal-replied").find("form").submit(function(e) {
   
        var form = $("#modal-replied").find("form");
        var loading = $("#load-replied");
        
        // stop form from submitting
        e.preventDefault();

        loading.removeClass('hide');
        
        // push form data
        $.ajax({
            type: "post",
            url: form.attr('action'),
            data: form.serialize(),
            dataType: "json",
            success: function(response) {

                if (response.unauthorized) {
                window.location.href = '{{ url("login")}}';
                }

                if(response.error) {
                    var errors = '';

                    $.each(response.error, function(index, value) {
                        errors += value;
                    });

                    status('', errors, 'alert-danger', '#replied-notice');
                } else {
                    $("#modal-replied").modal('hide');
                    status(response.title, response.body, 'alert-success');

                    refresh();
                }

                loading.addClass('hide');
            }
        });
    });


    // reset form notice
    $("#modal-scheduled").on('show.bs.modal', function(e) {
        $("#scheduled-notice").html("");
    });

    // submit form
    $("#modal-scheduled").find("form").submit(function(e) {
   
        var form = $("#modal-scheduled").find("form");
        var loading = $("#load-scheduled");
        
        // stop form from submitting
        e.preventDefault();

        loading.removeClass('hide');
        
        // push form data
        $.ajax({
            type: "post",
            url: form.attr('action'),
            data: form.serialize(),
            dataType: "json",
            success: function(response) {

                if (response.unauthorized) {
                window.location.href = '{{ url("login")}}';
                }

                if(response.error) {
                    var errors = '';

                    $.each(response.error, function(index, value) {
                        errors += value;
                    });

                    status('', errors, 'alert-danger', '#scheduled-notice');
                } else {
                    $("#modal-scheduled").modal('hide');
                    status(response.title, response.body, 'alert-success');

                    refresh();
                }

                loading.addClass('hide');
            }
        });
    });


    // reset form notice
    $("#modal-cancelled").on('show.bs.modal', function(e) {
        $("#cancelled-notice").html("");
    });

    // submit form
    $("#modal-cancelled").find("form").submit(function(e) {
   
        var form = $("#modal-cancelled").find("form");
        var loading = $("#load-cancelled");
        
        // stop form from submitting
        e.preventDefault();

        loading.removeClass('hide');
        
        // push form data
        $.ajax({
            type: "post",
            url: form.attr('action'),
            data: form.serialize(),
            dataType: "json",
            success: function(response) {

                if (response.unauthorized) {
                window.location.href = '{{ url("login")}}';
                }
                if(response.error) {
                    var errors = '';

                    $.each(response.error, function(index, value) {
                        errors += value;
                    });

                    status('', errors, 'alert-danger', '#cancelled-notice');
                } else {
                    $("#modal-cancelled").modal('hide');
                    status(response.title, response.body, 'alert-success');

                    refresh();
                }

                loading.addClass('hide');
            }
        });
    });

    // reset form notice
    $("#modal-finished").on('show.bs.modal', function(e) {
        $("#finished-notice").html("");
    });

    // submit form
    $("#modal-finished").find("form").submit(function(e) {
   
        var form = $("#modal-finished").find("form");
        var loading = $("#load-finished");
        
        // stop form from submitting
        e.preventDefault();

        loading.removeClass('hide');
        
        // push form data
        $.ajax({
            type: "post",
            url: form.attr('action'),
            data: form.serialize(),
            dataType: "json",
            success: function(response) {

                if (response.unauthorized) {
                window.location.href = '{{ url("login")}}';
                }
                if(response.error) {
                    var errors = '';

                    $.each(response.error, function(index, value) {
                        errors += value;
                    });

                    status('', errors, 'alert-danger', '#finished-notice');
                } else {
                    $("#modal-finished").modal('hide');
                    status(response.title, response.body, 'alert-success');

                    refresh();
                }

                loading.addClass('hide');
            }
        });
    });

    // reset form notice
    $("#modal-loan").on('show.bs.modal', function(e) {
        $("#loan-notice").html("");
    });

    // submit form
    $("#modal-loan").find("form").submit(function(e) {
   
        var form = $("#modal-loan").find("form");
        var loading = $("#load-loan");
        
        // stop form from submitting
        e.preventDefault();

        loading.removeClass('hide');
        
        // push form data
        $.ajax({
            type: "post",
            url: form.attr('action'),
            data: form.serialize(),
            dataType: "json",
            success: function(response) {

                if (response.unauthorized) {
                window.location.href = '{{ url("login")}}';
                }
                if(response.error) {
                    var errors = '<ul>';

                    $.each(response.error, function(index, value) {
                        errors += '<li>' + value + '</li>';
                    });

                    errors += '</ul>';

                    status('Please correct the following:', errors, 'alert-danger', '#loan-notice');
                } else {
                    $("#modal-loan").modal('hide');
                    status(response.title, response.body, 'alert-success');

                    refresh();
                }

                loading.addClass('hide');
            }
        });
    });

    // reset form notice
    $("#modal-amortization").on('show.bs.modal', function(e) {
        $("#amortization-notice").html("");
        $("#preview-notice").html("");
        $("#amortization-preview").html("");
    });

    // submit form
    $("#modal-amortization").find("form").submit(function(e) {
   
        var form = $("#modal-amortization").find("form");
        var loading = $("#load-amortization");
        
        // stop form from submitting
        e.preventDefault();

        loading.removeClass('hide');
        
        // push form data
        $.ajax({
            type: "post",
            url: form.attr('action'),
            data: form.serialize(),
            dataType: "json",
            success: function(response) {

                if (response.unauthorized) {
                window.location.href = '{{ url("login")}}';
                }
                if(response.error) {
                    var errors = '<ul>';

                    $.each(response.error, function(index, value) {
                        errors += '<li>' + value + '</li>';
                    });

                    errors += '</ul>';

                    status('Please correct the following:', errors, 'alert-danger', '#amortization-notice');
                } else {
                    $("#modal-amortization").modal('hide');
                    status(response.title, response.body, 'alert-success');

                    refresh();
                }

                loading.addClass('hide');
            }
        });
    });

    // reset form notice
    $("#modal-delete-amortization").on('show.bs.modal', function(e) {
        $("#delete-amortization-notice").html("");
    });

    // submit form
    $("#modal-delete-amortization").find("form").submit(function(e) {
   
        var form = $("#modal-delete-amortization").find("form");
        var loading = $("#load-delete-amortization");
        
        // stop form from submitting
        e.preventDefault();

        loading.removeClass('hide');
        
        // push form data
        $.ajax({
            type: "post",
            url: form.attr('action'),
            data: form.serialize(),
            dataType: "json",
            success: function(response) {

                if (response.unauthorized) {
                window.location.href = '{{ url("login")}}';
                }
                if(response.error) {
                    var errors = '<ul>';

                    $.each(response.error, function(index, value) {
                        errors += '<li>' + value + '</li>';
                    });

                    errors += '</ul>';

                    status('Please correct the following:', errors, 'alert-danger', '#delete-amortization-notice');
                } else {
                    $("#modal-delete-amortization").modal('hide');
                    status(response.title, response.body, 'alert-success');

                    refresh();
                }

                loading.addClass('hide');
            }
        });
    });

	// submit form
    $("#modal-profile").find("form").submit(function(e) {
  		var form = $("#modal-profile").find("form");
  		var loading = $("#load-profile");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			//data: form.serialize(),
			data: new FormData($("#modal-save_profile")[0]),
			dataType: "json",
			async:true,
      		processData: false,
      		contentType: false,
			success: function(response) {
                
                if (response.unauthorized) {
                window.location.href = '{{ url("login")}}';
                }
				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#profile-notice');
				} else {
					$("#modal-profile").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list 
					// error on  refresh
					refresh_profile();

				}

				loading.addClass('hide');

			}
  		});
    });

});
