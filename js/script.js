$(function() {
    toastr.options.positionClass = 'toast-bottom-right';
    toastr.options.closeButton = true;
    toastr.options.showMethod = 'slideDown';
});

    $("#featured-carousel div .item:first-child").addClass('active');

// Touchscreen swipe through carousel (slider)
$(document).ready(function() {  
   if ($.mobile) {
        //must target by ID, so these need to be used in your content
        $('#myCarousel, #myCarousel2, #agnosia-bootstrap-carousel').swiperight(function() {  
            $(this).carousel('prev');  
        });  
        $('#myCarousel, #myCarousel2, #agnosia-bootstrap-carousel').swipeleft(function() {  
            $(this).carousel('next');  
        });  
    }
});  

// Smooth scroll to anchor within the page
$(document).ready(function() {
    $('.smoothscroll').click(function() {
      var target = $(this.hash);
      var offset = $('body').css('padding-top');
      if (offset) offset = offset.replace('px','');
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: ( target.offset().top - offset )
        }, 1000);
        return false;
      }
});
});

if ($(window).scrollTop() <= 0) {
    $('#navbar-main').removeClass('opaque');
    $('#main-logo').removeClass('opaque-logo');
}

$(window).scroll(function() {
    
    // check if window scroll for more than 430px. May vary
    // as per the height of your main banner.

    if($(this).scrollTop() > 5) { 
        $('#navbar-main').addClass('opaque'); // adding the opaque class
        $('#main-logo').addClass('opaque-logo'); // adding the opaque class
    } else {
        $('#navbar-main').removeClass('opaque'); // removing the opaque class
        $('#main-logo').removeClass('opaque-logo'); // removing the opaque class
    }

});

var divs= $('section');
var nav = $('.navbar');
var nav_height = nav.outerHeight();

$(window).on('scroll', function () {
  var cur_pos = $(this).scrollTop();
  
  divs.each(function() {
    var top = $(this).offset().top - nav_height - 1,
        bottom = top + $(this).outerHeight();
    
    if (cur_pos >= top && cur_pos <= bottom) {
      nav.find('a').removeClass('active');
      
      $(this).addClass('active');

      nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
    }
   if($(window).scrollTop() == 0) {
     $(".btn-top").fadeOut();
   } else {
     $(".btn-top").fadeIn();
   }
  });
});

$(function(){ 

  var $window = $(window);
  var scrollTime = 1.2;
  var scrollDistance = 400;

  $window.on("mousewheel DOMMouseScroll", function(event){

    event.preventDefault(); 

    var delta = event.originalEvent.wheelDelta/80 || -event.originalEvent.detail/3;
    var scrollTop = $window.scrollTop();
    var finalScroll = scrollTop - parseInt(delta*scrollDistance);

    TweenMax.to($window, scrollTime, {
      scrollTo : { y: finalScroll, autoKill:true },
        ease: Power1.easeOut,
        overwrite: 5              
      });

  });
});

// Perfect Parallax
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    $(".jumbotron").attr('style', 'background: ' + $('#parallax-bg').css('background-image') + ' no-repeat center center;');
    $('#parallax-bg').addClass('hide');
} else {
    if ($(window).scrollTop() > 0) {
        $('#parallax-bg').addClass('hide');
    } else {
        $('#parallax-bg').css('height', $('.jumbotron').height() + 'px');
    }
    $(".jumbotron").css('background', 'none');
    if ($(window).width() > 767) {
        var jumboHeight = $('.jumbotron').outerHeight();
        function parallax(){
            var scrolled = $(window).scrollTop();
            $('#parallax-bg').css('height', (jumboHeight-scrolled) + 'px');
        }
        $(window).scroll(function(e){
            if ($(window).width() <= 767) {
                $('#parallax-bg').addClass('hide');
            } else {
                parallax();
                $('#parallax-bg').removeClass('hide');
            }
        });
    } else {
        $(".jumbotron").attr('style', 'background: ' + $('#parallax-bg').css('background-image') + ' no-repeat center center;');
        $('#parallax-bg').addClass('hide');
    }
}

$(window).resize(function() {
  if ($(window).width() <= 767) {
      $(".jumbotron").attr('style', 'background: ' + $('#parallax-bg').css('background-image') + ' no-repeat center center;');
      $('#parallax-bg').addClass('hide');
  } else {
      if ($(window).scrollTop() > $('.jumbotron').height() ) {
          $('#parallax-bg').addClass('hide');
      } else {
          $('#parallax-bg').css('height', $('.jumbotron').height() - $(window).scrollTop() + 'px');
      }
      $(".jumbotron").css('background', 'none');
      $('#parallax-bg').removeClass('hide');
      var jumboHeight = $('.jumbotron').outerHeight();
      function parallax(){
          var scrolled = $(window).scrollTop();
          $('#parallax-bg').css('height', (jumboHeight-scrolled) + 'px');
      }
      $(window).scroll(function(e){
          parallax();
      });
  }
});